PANEL = {}

PANEL.Index = 0
PANEL.IsSelected = false

---Initialize the panel upon creation
function PANEL:Init()
    -- Panel variables
    self.Data = nil         -- The data used to populate this element
    self.DataChanged = false    -- A flag indicating that the data was changed
    self.UseSmall = false   -- When updating this element, use a smaller size
    self.IsSmall = false    -- When the element was updated, was it updated with a small size
    self.SearchableKeys = { -- Keys that are searchable in `self.Data` by the search bar filter
        "TextMain",
        "TextSub"
    }

    -- Flags
    self.VisibleIcon = false    -- When the element was last updated, was the icon visible
    self.VisiblePanelBottom = false -- When the element was last updated, was the bottom panel visible

    -- Give the panel some initial properties
    self:SetHeight(64)

    -- Create the icon over on the left
    local Icon = vgui.Create("SpawnIcon", self)
    self.Icon = Icon
    Icon.PaintOver = function(Panel, W, H) end  -- Stop the glow from rendering when hovering over the icon
    Icon:SetEnabled(false)  -- Stop the thing from acting as a button when it gets clicked
    Icon:SetSize(64,64)
    Icon:Hide()     -- Hide the icon
    Icon:Dock(LEFT)

    -- Create the top and bottom panes
    local PanelTop = vgui.Create("DPanel", self)
    self.PanelTop = PanelTop
    PanelTop:SetHeight(32)
    PanelTop:Dock(TOP)
    PanelTop:SetPaintBackground(false)

    local PanelBottom
    PanelBottom = vgui.Create("DPanel", self)
    self.PanelBottom = PanelBottom
    PanelBottom:SetHeight(32)
    PanelBottom:Dock(BOTTOM)
    PanelBottom:SetPaintBackground(false)

    -- Create the main text field
    local TextMain = vgui.Create("DLabel", PanelTop)
    self.TextMain = TextMain
    TextMain:SetFont("SpawnMenuLarge")
    TextMain:DockMargin(5,0,0,0)
    TextMain:Dock(LEFT)

    -- Create the sub text field
    local TextSub = vgui.Create("DLabel", PanelBottom)
    self.TextSub = TextSub
    TextSub:SetFont("SpawnMenuSmall")
    TextSub:SetWrap(true)
    TextSub:SetAutoStretchVertical(true)
    TextSub:DockMargin(5,0,0,2)
    TextSub:Dock(BOTTOM)

    self.Initialized = true
end

---Called after the panel is resized, with the new sizes passed in
---@param W number #The new width of the panel
---@param H number #The new height of the panel
function PANEL:OnSizeChanged(W, H)
    if not self.Initialized then return end

    local UseSmall = W < 256
    local SmallToLarge = self.IsSmall and not UseSmall
    local LargeToSmall = (not self.IsSmall) and UseSmall

    if SmallToLarge then
        self:SetHeight(64)
        self.PanelTop:SetTooltip()  -- Nil out the tooltip displayed on the top panel
        self.PanelBottom:Show() -- Display the bottom panel
        self.Icon:SetSize(64,64) -- Make the icon big
        self.TextMain:SetFont("SpawnMenuLarge") -- Main text uses big font
    elseif LargeToSmall then
        self:SetHeight(32)
        self.PanelTop:SetTooltip((self.Data or {}).TextSub)  -- Set the top panel's tooltip to the sub text
        self.PanelBottom:Hide() -- Hide the bottom panel
        self.Icon:SetSize(32,32) -- Make the icon small
        self.TextMain:SetFont("SpawnMenuMedium") -- Main text uses smaller font
    end

    self.IsSmall = UseSmall
end

---Called when the panel's layout needs to change
---@param W number #The current width of the panel
---@param H number #The current height of the panel
function PANEL:PerformLayout(W, H)
    if not self.Initialized then return end

    if self.DataChanged then
        local Data = self.Data or {}

        -- Update the icon
        if Data.ModelPath then
            self.Icon:SetModel(Data.ModelPath, Data.ModelSkinID or 0)
            if not self.VisibleIcon then
                self.Icon:Show()
                self.VisibleIcon = true
            end
        else
            if self.VisibleIcon then
                self.Icon:Hide()
                self.VisibleIcon = false
            end
        end

        -- Update the main text
        self.TextMain:SetText(Data.TextMain)
        self.TextMain:SizeToContents()
        self.Icon:SetTooltip(Data.TextMain)

        -- Update the sub text
        self.TextSub:SetText(Data.TextSub)

        self.Datachanged = false
    end
end

---Set the index of the element representing its position in the parent searchpanel
---@param Index integer #The index of the element
function PANEL:SetIndex(Index)
    self.Index = Index
end

---Get the index of the element in the parent searchpanel's data set
---@return integer Index #The index of the element
function PANEL:GetIndex()
    return self.Index
end

---Set/Reset the state of the element's selection. Used to tell the panel how to draw itself.
---@param Selected boolean #The selection state
function PANEL:SetSelected(Selected)
    self.IsSelected = Selected
end

---Set the data to be displayed by this element and update it
---@param Data table #The data to be displayed
function PANEL:SetData(Data)
    self.Data = Data
    self.Data.SearchableKeys = self.SearchableKeys
    self.DataChanged = true
    self:InvalidateLayout()
end

---Returns the element panel's data table
---@return table Data
function PANEL:GetData()
    return self.Data
end

vgui.Register("SearchElement_Base", PANEL, "Panel")