AddCSLuaFile()

if not CLIENT then return end

local MenuContentPath = "%s/clientcontent/html/menu_%s.txt"

-- Panel based blur function by Chessnut from NutScript
local blurmat = Material("pp/blurscreen")
local function BackgroundBlur(panel, layers, density, alpha)
    -- Its a scientifically proven fact that blur improves a script
    local x, y = panel:LocalToScreen(0, 0)
    surface.SetDrawColor(255, 255, 255, alpha)
    surface.SetMaterial(blurmat)

    for i = 1, 3 do
        blurmat:SetFloat("$blur", (i / layers) * density)
        blurmat:Recompute()

        render.UpdateScreenEffectTexture()
        surface.DrawTexturedRect(-x, -y, ScrW(), ScrH())
    end
end

local PANEL = {}

AccessorFunc(PANEL, "m_bBackgroundBlurred", "BackgroundBlurred", FORCE_BOOL)

function PANEL:Init()
    self:Dock(FILL)
    self:SetKeyboardInputEnabled(true)
	self:SetMouseInputEnabled(true)
    self:SetBackgroundColor(Color(70, 70, 70, 110))

    if BRANCH ~= "x86-64" and BRANCH ~= "chromium" then
        chat.AddText(Color(255, 50, 50, 255), "You must be on either the x86-64 or chromium Garry's Mod versions to use this menu!")
        self:Remove()
        return
    end

    self.HTML = vgui.Create("DHTML", self)
    self.HTML:Dock(FILL)
    self.HTML:SetScrollbars(false)
    self.HTML:SetAllowLua(true)
    self.HTML:SetKeyboardInputEnabled(true)
    self.HTML:SetMouseInputEnabled(true)
    self.HTML:RequestFocus()

    self.HTML:AddFunction("spacecombat", "CloseMenu", function()
        self:Remove()
    end)

    self:MakePopup()
    self:SetPopupStayAtBack(true)
    self:SetBackgroundBlurred(true)
end

function PANEL:LoadMenu(MenuName)
    if not IsValid(self.HTML) then
        return
    end

    local Path = Format(MenuContentPath, SC.DataFolder, MenuName)
    if not file.Exists(Path, "DATA") then
        SC.Error(Format("Menu %s does not exist!", MenuName), 5)
        return
    end

    hook.Run("SC.InitHTMLMenu", self.HTML, MenuName)

    self.HTML:OpenURL(Format("asset://garrysmod/data/%s", Path))
end

function PANEL:Paint()
    if self:GetBackgroundBlurred() then
        BackgroundBlur(self, 7, 15, 150)
    end
end

vgui.Register("DHTMLMenu", PANEL, "DPanel")