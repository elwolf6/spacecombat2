AddCSLuaFile()

local ok, err = pcall(require, "socket.core")
if not ok then
	print("bad socket")
	if err~="Module not found!" then ErrorNoHalt(err) end
	return
end

ok, err = pcall(require, "vscode-debuggee")
if not ok then
	print("bad debuggee")
	if err~="Module not found!" then ErrorNoHalt(err) end
	return
end

local OldSocket

function StartDebugger()
	local json = {
		encode = util.TableToJSON,
		decode = util.JSONToTable
	}

	if OldSocket ~= nil then
		OldSocket:close()
		OldSocket = nil
	end

	local DebuggerPort
	if SERVER then
		DebuggerPort = 56789
	else
		DebuggerPort = 56799
	end

	local Config = {
		controllerPort = DebuggerPort,
		connectTimeout = 0.1,
		redirectPrint = false
	}

	local startResult, breakerType, sock = debuggee.start(json, Config)
	print('debuggee start ->', startResult, breakerType)

	OldSocket = sock

	if startResult then
		timer.Create("TickDebugger", 0.1, 0, function()
			debuggee.poll()
		end)
	end
end

function StopDebugger()
	if OldSocket ~= nil then
		debug.sethook()
		OldSocket:close()
		OldSocket = nil
		timer.Remove("TickDebugger")
	end
end

function RestartDebugger()
	StopDebugger()

	timer.Simple(5, StartDebugger)
end

--hook.Add("InitPostEntity", "InitDebugger", StartDebugger)
hook.Remove("OnReloaded", "ReloadDebugger")
hook.Add("OnReloaded", "ReloadDebugger", RestartDebugger)