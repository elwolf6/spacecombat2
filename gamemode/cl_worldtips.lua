surface.CreateFont( "GModWorldtip",
{
	font		= "Helvetica",
	size		= SC2ScreenScale(20),
	weight		= 700
})

local cl_drawworldtooltips = CreateConVar( "cl_drawworldtooltips", "1", { FCVAR_ARCHIVE } )
local WorldTip = nil

--
-- Adds a hint to the queue
--
function AddWorldTip( ent, text, unused1, unused2, unused3 )

	if unused3 then
		ent = unused3
	end

	--invalid worldtip
	if not IsValid(ent) then
		return
	end

	WorldTip = {}

	WorldTip.dietime 	= SysTime() + 0.05
	WorldTip.text 		= text
	WorldTip.ent 		= ent

end

local edgesize = 18

 -- makes sure the overlay doesn't go out of the screen & provides several useful sizes and positions for the DrawBody function
local function GetWorldTipPositions( w, h, w_body, h_body, w_footer, h_footer, WorldTip )
	local pos, spos

	if WorldTip.manualpaint then
		pos = {x=0,y=0}
	else
		pos = LocalPlayer():GetEyeTrace().HitPos
		spos = LocalPlayer():GetShootPos()
		if pos == spos then -- if the position is right in your face, get a better position
			pos = spos + LocalPlayer():GetAimVector() * 5
		end
		pos = pos:ToScreen()

		pos.x = math.Round(pos.x)
		pos.y = math.Round(pos.y)
	end

	w = math.min( w, ScrW() - 64 )
	h = math.min( h, ScrH() - 64 )

	local maxx = pos.x - 32
	local maxy = pos.y - 32

	local minx = maxx - w
	local miny = maxy - h

	if minx < 32 then
		maxx = 32 + w
		minx = 32
	end

	if miny < 32 then
		maxy = 32 + h
		miny = 32
	end

	local centerx = (maxx+minx)/2
	local centery = (maxy+miny)/2

	return {	min = {x = minx,y = miny},
				max = {x = maxx,y = maxy},
				center = {x = centerx, y = centery},
				size = {w = w, h = h},
				bodysize = {w = w_body, h = h_body },
				footersize = {w = w_footer, h = h_footer},
				edgesize = edgesize
			}
end

local function DrawWorldTipOutline( pos )
	draw.NoTexture()
	surface.SetDrawColor(Color(25,25,25,200))

	local poly = {
					{x = pos.min.x + edgesize, 	y = pos.min.y,				u = 0, v = 0 },
					{x = pos.max.x, 			y = pos.min.y,				u = 0, v = 0 },
					{x = pos.max.x, 			y = pos.max.y - edgesize,	u = 0, v = 0 },
					{x = pos.max.x - edgesize, 	y = pos.max.y,				u = 0, v = 0 },
					{x = pos.min.x, 			y = pos.max.y,				u = 0, v = 0 },
					{x = pos.min.x, 			y = pos.min.y + edgesize,	u = 0, v = 0 },
				}

	render.CullMode(MATERIAL_CULLMODE_CCW)
	surface.DrawPoly( poly )

	surface.SetDrawColor(Color(0,0,0,255))

	for i=1,#poly-1 do
		surface.DrawLine( poly[i].x, poly[i].y, poly[i+1].x, poly[i+1].y )
	end
	surface.DrawLine( poly[#poly].x, poly[#poly].y, poly[1].x, poly[1].y )
end

local function GetWorldTipBodySize( WorldTip )
	if not WorldTip.text then return 0,0 end
	return surface.GetTextSize( WorldTip.text )
end

local function DrawWorldTipBody( text, pos )
	draw.DrawText( text, "GModWorldtip", pos.min.x + pos.size.w/2, pos.min.y + edgesize/2, Color(255,255,255,255), TEXT_ALIGN_CENTER )
end

local function DrawWorldTip(WorldTip)
	surface.SetFont( "GModWorldtip" )

	local self = WorldTip.ent

	local txt = WorldTip.text or ""
	local class = (self.PrintName ~= "" and self.PrintName or self:GetClass()) .. " [" .. self:EntIndex() .. "]"

	local w_body, 	h_body = 0, 0
	if self.GetWorldTipBodySize then w_body, h_body = self:GetWorldTipBodySize( WorldTip ) else w_body, h_body = GetWorldTipBodySize( WorldTip ) end
	local w_class, 	h_class = surface.GetTextSize( class )

	local w_total = w_body
	local h_total = h_body

	local w_footer, h_footer = 0, 0

	w_footer = math.max(w_total,w_class + 8)
	h_footer = h_class + edgesize + 8

	w_total = w_footer
	h_total = h_total + h_footer

	if h_body == 0 then h_total = h_total - h_body - edgesize end

	local pos = GetWorldTipPositions(	w_total + edgesize*2,h_total + edgesize,
										w_body,h_body,
										w_footer,h_footer, WorldTip )

	DrawWorldTipOutline( pos )

	local offset = pos.min.y
	if h_body > 0 then
		if self.DrawWorldTipBody then self:DrawWorldTipBody( WorldTip, pos ) else DrawWorldTipBody( txt, pos ) end
		offset = offset + h_body + edgesize

		surface.SetDrawColor( Color(0,0,0,255) )
		surface.DrawLine( pos.min.x, offset, pos.max.x, offset )
	end

	draw.DrawText( class, "GModWorldtip", pos.center.x, offset + 16, Color(255,255,255,255), TEXT_ALIGN_CENTER )
end

function GM:PaintWorldTip( ent ) -- manually paint the world tip for this entity
	if IsValid( ent ) then
		DrawWorldTip({
			text = ent:GetOverlayText(),
			ent = ent,
			manualpaint = true,
		})
	end
end

function GM:PaintWorldTips()
	if not cl_drawworldtooltips:GetBool() then return end

	if WorldTip and IsValid(WorldTip.ent) and WorldTip.dietime > SysTime() then
		DrawWorldTip(WorldTip)
	end
end