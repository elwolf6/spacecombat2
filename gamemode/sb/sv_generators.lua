local GM = GM

hook.Remove("SC.Config.Register", "SC.LoadGeneratorConfig")
hook.Add("SC.Config.Register", "SC.LoadGeneratorConfig", function()
    local Config = GM.Config

    Config:Register({
        File = "generators",
        Section = "General",
        Key = "EnableVolumeBasedMultipliers",
        Default = false
    })

    Config:Register({
        File = "generators",
        Section = "ClassMultipliers",
        Key = "Drone",
        Default = 1
    })

    Config:Register({
        File = "generators",
        Section = "ClassMultipliers",
        Key = "Fighter",
        Default = 2
    })

    Config:Register({
        File = "generators",
        Section = "ClassMultipliers",
        Key = "Frigate",
        Default = 4
    })

    Config:Register({
        File = "generators",
        Section = "ClassMultipliers",
        Key = "Cruiser",
        Default = 6
    })

    Config:Register({
        File = "generators",
        Section = "ClassMultipliers",
        Key = "Battlecruiser",
        Default = 8
    })

    Config:Register({
        File = "generators",
        Section = "ClassMultipliers",
        Key = "Battleship",
        Default = 10
    })

    Config:Register({
        File = "generators",
        Section = "ClassMultipliers",
        Key = "Dreadnaught",
        Default = 12
    })

    Config:Register({
        File = "generators",
        Section = "ClassMultipliers",
        Key = "Titan",
        Default = 16
    })
end)