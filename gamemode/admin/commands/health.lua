AddCSLuaFile()

local Admin = SC.Administration

hook.Add("RegisterSC2Commands", "RegisterHealthCommands", function()
    -- Register any permissions we need for these commands
    Admin.RegisterPermission("PlayerHealthControl", "The ability to change the amount of health a player has.")

    -- Register commands
    Admin.RegisterCommand("Kill", "Immediately kill someone, probably because they were naughty.",
        -- Permissions
        {
            "PlayerHealthControl"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            },
            {
                Name = "Silent",
                Type = "optional_bool"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target

            if IsValid(Target) then
                if Arguments.Silent then
                    Target:KillSilent()
                else
                    Target:Kill()
                end
            end
        end)

    Admin.RegisterCommand("SetHealth", "Change someone's health to a value of your choosing.",
        -- Permissions
        {
            "PlayerHealthControl"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            },
            {
                Name = "Health",
                Type = "number"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target

            if IsValid(Target) then
                Target:SetHealth(math.Clamp(Arguments.Health, 1, Target:GetMaxHealth()))
            end
        end)

    Admin.RegisterCommand("God", "Disable or enable all damage for a player.",
        -- Permissions
        {
            "PlayerHealthControl"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target

            if IsValid(Target) then
                if Target:HasGodMode() then
                    Target:GodDisable()
                else
                    Target:GodEnable()
                end
            end
        end)
end)