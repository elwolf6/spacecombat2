require("leakdetector")

AddCSLuaFile()

local Admin = SC.Administration

hook.Add("RegisterSC2Commands", "RegisterDebugCommands", function()
    -- Register any permissions we need for these commands
    Admin.RegisterPermission("Debug", "The ability to run gamemode debug commands.")

    -- Register commands
    Admin.RegisterCommand("memorysnapshot", "Create a memory snapshot",
        -- Permissions
        {
            "Debug"
        },

        -- Arguments
        {
            {
                Name = "Name",
                Type = "string"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local Name = Arguments.Name

            collectgarbage("collect")
            leakdetector.DumpMemorySnapshot(SC.DataFolder.."/temp/memory/", Name, -1)
        end)

    local RunningTest = false
    Admin.RegisterCommand("memoryleaktest", "Create a memory snapshot",
        -- Permissions
        {
            "Debug"
        },

        -- Arguments
        {
            {
                Name = "Time",
                Type = "number"
            }
        },

        -- Callback
        function(Executor, Arguments)
            if RunningTest then
                if IsValid(Executor) then
                    Executor:ChatPrint("A leak test is already running!")
                end

                return
            end

            RunningTest = true

            local Time = Arguments.Time

            collectgarbage("collect")

            if IsValid(Executor) then
                Executor:ChatPrint(string.format("Beginning leak test for %s seconds, current memory usage: %sKB", sc_ds(Time), tostring(collectgarbage("count"))))
            end

            local MemoryFolder = SC.DataFolder.."/temp/memory/"

            file.CreateDir(MemoryFolder)

            leakdetector.DumpMemorySnapshot(MemoryFolder, "leaktest-before", -1)

            timer.Simple(Time, function()
                RunningTest = false

                collectgarbage("collect")
                leakdetector.DumpMemorySnapshot(MemoryFolder, "leaktest-after", -1)
                leakdetector.DumpMemorySnapshotComparedFile(MemoryFolder, "leaktest-compared", -1, MemoryFolder.."LuaMemRefInfo-All-[leaktest-before].txt", MemoryFolder.."LuaMemRefInfo-All-[leaktest-after].txt")

                -- Clean up one more time before printing to ensure an accurate count
                collectgarbage("collect")

                if IsValid(Executor) then
                    Executor:ChatPrint(string.format("Finished leak test for %s seconds, current memory usage: %sKB", sc_ds(Time), tostring(collectgarbage("count"))))
                end
            end)
        end)
end)