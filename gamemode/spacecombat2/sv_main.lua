include("sv_config.lua")

--[[******************************************************************************
	Values you can tweak below this line.
******************************************************************************]]--

--Convars
CreateConVar("sc_FastCoreCalculations", "0", {FCVAR_NOTIFY, FCVAR_ARCHIVE}, "Should cores use faster calculations or accurate calculations? Set to 1 for Fast, 0 for Accurate.")

--####	Basic Health Stuff	####--
local MaxHealth = 1000000	-- Max Health Allowed
local MinHealth = 100		-- Min Health Allowed
--This will only apply to individual props, not Cores

local ETSC_M 	= 1.00 --Multiplier for repair modual HP boost
local ETSC_CM 	= 1.00 --Multiplier for modual Cap use

--This applies to props as well as cores
local BaseHullRes = {
	EM		= 0.0,
	EXP		= 0.0,
	KIN		= 0.0,
	THERM	= 0.0
}

--Shield and Armor resistances only apply to cores
local BaseArmorRes = {
	EM		= 0.60,
	EXP		= 0.25,
	KIN		= 0.35,
	THERM	= 0.55
}

local BaseShieldRes = {
	EM		= 0.25,
	EXP		= 0.50,
	KIN		= 0.40,
	THERM	= 0.35
}

--Sounds used for shield hits
--MP3 in GMod memory leaks but WAV dosn't :(
SC.ShieldSounds = {
	Sound("spacecombat2/shieldhit/shieldhit_01.wav"),
	Sound("spacecombat2/shieldhit/shieldhit_02.wav"),
	Sound("spacecombat2/shieldhit/shieldhit_03.wav"),
	Sound("spacecombat2/shieldhit/shieldhit_04.wav")
}

SC.ArmorSounds = {}
SC.ArmorSounds.KIN = {
	"ship_weapons/sc_armor_hit_kin_01.wav"
}
SC.ArmorSounds.EM = {
	"ship_weapons/sc_armor_hit_em_01.wav"
}
SC.ArmorSounds.EXP = {
	"ship_weapons/sc_armor_hit_exp_01.wav"
}
SC.ArmorSounds.THERM = {
	"ship_weapons/sc_armor_hit_therm_01.wav"
}

SC.SelfDestructSounds = {
	"ambient/alarms/amb_container_alarm_01.wav",
	"alarm/sga_midway_alarm.wav",
	"alarm/sga_midway_selfdestruct.wav"
}

SC.SelfDestructFinalSounds = {
	"ambient/machines/usetoilet_thank.wav",
	"vo/citadel/br_youfool.wav",
	"vo/citadel/br_ohshit.wav",
	"vo/citadel/br_laugh01.wav",
	"vo/k_lab/kl_fiddlesticks.wav",
	"vo/k_lab/kl_getoutrun03.wav",
	"vo/npc/male01/gethellout.wav",
	"vo/npc/female01/gethellout.wav",
	"vo/npc/male01/hacks01.wav"
}

--Called when the core starts dying, add stuff that needs to stop working in here
SC.CoreDeath_KillClasses = {
	--"gmod_wire_expression2", --Until I make E2s themselves get gimped by core death. -Steeveeo
	--"gyropod_advanced"
}

--####	Health Multipliers Based on Material	####--
local MULTI = {}
MULTI[MAT_CONCRETE]		= 1.50
MULTI[MAT_METAL] 		= 3.00
MULTI[MAT_DIRT] 		= 0.20
MULTI[MAT_VENT] 		= 1.50
MULTI[MAT_GRATE] 		= 1.50
MULTI[MAT_TILE] 		= 0.50
MULTI[MAT_SLOSH] 		= 0.10
MULTI[MAT_WOOD] 		= 0.25
MULTI[MAT_COMPUTER] 	= 1.00
MULTI[MAT_GLASS] 		= 0.10
MULTI[MAT_FLESH] 		= 0.25
MULTI[MAT_BLOODYFLESH]	= 0.25
MULTI[MAT_CLIP] 		= 0.01
MULTI[MAT_ANTLION]		= 0.50
MULTI[MAT_ALIENFLESH]	= 0.50
MULTI[MAT_FOLIAGE]		= 0.10
MULTI[MAT_SAND]			= 0.15
MULTI[MAT_PLASTIC]		= 0.75

--[[******************************************************************************
	No More Tweaking!!! (unless you know what you're doing)
******************************************************************************]]--

--Networking Stuff
util.AddNetworkString("ship_core_start_selfdestruct")
util.AddNetworkString("ship_core_stop_selfdestruct")

for k,v in pairs(SC.ShieldSounds) do
	util.PrecacheSound(v)
end

for k,v in pairs(SC.SelfDestructSounds) do
	util.PrecacheSound(v)
end

for k,v in pairs(SC.SelfDestructFinalSounds) do
	util.PrecacheSound(v)
end

--####	Global functions to return some values so other stuff can read ####--
function SC.MaxHealth()	-- Get the MaxHealth
	return MaxHealth
end

function SC.MinHealth()	-- Get the MinHealth
	return MinHealth
end

function SC.ETSCM()
	return ETSC_M
end

function SC.ETSCCM()
	return ETSC_CM
end

function SC.BaseHullResists()
    return BaseHullRes
end

function SC.BaseArmorResists()
    return BaseArmorRes
end

function SC.BaseShieldResists()
    return BaseShieldRes
end

function SC.GetMatType(ent)	--Bad way to find material type.. but appears to be the only way there is :(
	local min = ent:OBBMins() + ent:GetPos()
	--Msg("TR Min: "..tostring(min).."\n")
	local max = ent:OBBMaxs() + ent:GetPos()
	--Msg("TR Max: "..tostring(max).."\n")

	local tr = {
		start = min,
		endpos = max,
	}
	tr = util.TraceLine( tr )
	return tr.MatType
end

function SC.GetEntVolume(ent)
	local min = ent:OBBMins()
	local max = ent:OBBMaxs()
	local dif = max - min
	local volume = dif.x * dif.y * dif.z

	return volume
end

function SC.GetHealthMultiplier(ent)
	local mat = SC.GetMatType(ent)
	local multi = 0.5

	if mat then
		multi = MULTI[mat] or 0.5
	end

	return multi
end

function SC.CalcHealth(ent)
    -- If the entity isn't valid, or doesn't have a valid physics objects, then it shouldn't have health.
    if not IsValid(ent) then return end
    if not IsValid(ent:GetPhysicsObject()) then return end

    -- Additionally, if the entity isn't solid or never moves then it probably shouldn't have health either.
    if not ent:IsSolid() or ent:GetMoveType() == MOVETYPE_NONE then return end

    local Volume = SC.GetEntVolume(ent)
	local Multiplier = SC.GetHealthMultiplier(ent)
	local Health = math.Round((Volume^0.55)*Multiplier)

	ent.SC_Health = math.Clamp(Health, MinHealth, MaxHealth)
	ent.SC_MaxHealth = math.Clamp(Health, MinHealth, MaxHealth)

    return Health
end

function SC.Spawned( ent )
	SC.CalcHealth(ent)

	local OldFunc = ent:GetTable().OnTakeDamage
	function ent:OnTakeDamage(dmginfo) --Make sure the ent has an OnTakeDamage function so we can use that hook on it
        local Return = SC.EntityTakeDamage(self, dmginfo)
		if not Return then
		    Return = OldFunc(self, dmginfo)
        end

        return Return
	end
end
if not SC.Filter then hook.Add( "OnEntityCreated", "SC.Spawned", SC.Spawned) end

--[[******************************************************************************
	Things to deal damage
******************************************************************************]]--

function SC.EntityTakeDamage(ent, dmginfo)
    if not IsValid(ent) then return false end
    if ent:IsWorld() then return false end

    if ent.SC_Immune then
        --Scale the damage so the entity doesn't take normal game damage
        dmginfo:ScaleDamage(0)
        return dmginfo
    end

	if ent:IsPlayer() or ent:IsNPC() then return end --Nothing in here worries about players, and just causes a stack overflow with the fix below. --Steeveeo

	if not ent:GetProtector() and not ent.SC_Immune and not ent.SC_Health then
		SC.CalcHealth(ent)

        if not ent.SC_Health then
            SC.Error(Format("Failed to calculate health for entity %s", tostring(ent)), 5)
            return
        end
	end

	local inflictor = dmginfo:GetInflictor()
	local attacker = dmginfo:GetAttacker()
	local amount = dmginfo:GetDamage()

    if not ent.SC_Immune then
		-- Remove player-walking-on-shit damage
		if dmginfo:IsDamageType(DMG_CRUSH) and dmginfo:GetInflictor():IsPlayer() then
			return
		end

        -- Translate source damage types to Space Combat damage types
		local damage
		if dmginfo:IsExplosionDamage() then
		    damage = {EXP = amount}
        elseif dmginfo:IsDamageType(DMG_SHOCK) or dmginfo:IsDamageType(DMG_ENERGYBEAM) or dmginfo:IsDamageType(DMG_DISSOLVE) then
            damage = {EM = amount}
        elseif dmginfo:IsDamageType(DMG_BURN) or dmginfo:IsDamageType(DMG_SLOWBURN) or dmginfo:IsDamageType(DMG_PLASMA) or dmginfo:IsDamageType(DMG_ACID) then
            damage = {THERM = amount}
		else
			damage = {KIN = amount}
		end

		-- Deploy death, passes a valid-ish HitData table to prevent errors
		ent:ApplyDamage(damage, attacker, inflictor, {
            Entity = ent,
            Fraction = 1,
            FractionLeftSolid = 1,
            Hit = true,
            HitBox = -1,
            HitGroup = HITGROUP_GENERIC,
            HitNoDraw = false,
            HitNonWorld = not ent:IsWorld(),
            HitNormal = (dmginfo:GetDamagePosition() - ent:GetPos()):GetNormalized(),
            HitPos = dmginfo:GetDamagePosition(),
            HitSky = ent:IsWorld(),
            HitTexture = "",
            HitWorld = ent:IsWorld(),
            MatType = SC.GetMatType(ent),
            Normal = (dmginfo:GetDamagePosition() - ent:GetPos()):GetNormalized(),
            PhysicsBone = 0,
            StartPos = Vector(0, 0, 0),
            SurfaceProps = 1,
            StartSolid = false,
            AllSolid = false
        })
    end

    --Scale the damage so the entity doesn't take normal game damage
    dmginfo:ScaleDamage(0)

    return dmginfo
end

hook.Add("EntityTakeDamage", "SC.EntityTakeDamage", SC.EntityTakeDamage)

--####	Kill A Destroyed Entity  ####--
function SC.KillEnt(ent)
	if IsValid(ent) and not ent:IsPlayer() then
		ent:Fire("sethealth", "0", 0)
		ent:Fire("kill", "", 1)
	end
end

function SC.TotalDamage(damage)
	if not damage then return 0	end

	local ret = 0
	for i,k in pairs(damage) do
		ret = ret + k
	end

	return ret
end

local EntityMeta = FindMetaTable("Entity")
function EntityMeta:IsProtected()
    -- WTF are you doing here..?
    if not IsValid(self) then return end

    return IsValid(self:GetProtector())
end

function EntityMeta:SetProtector(NewProtector)
    -- WTF are you doing here..?
    if not IsValid(self) then return end

    self.SC_ProtectorEntity = NewProtector

    -- This is a dirty hack required because the scripted entity table doesn't override the entity meta table
    local SEntTable = scripted_ents.Get(self:GetClass())
    if SEntTable and SEntTable.SetProtector then
        return SEntTable.SetProtector(self, NewProtector)
    end
end

function EntityMeta:GetProtector()
    -- WTF are you doing here..?
    if not IsValid(self) then return end

    -- This is a dirty hack required because the scripted entity table doesn't override the entity meta table
    local SEntTable = scripted_ents.Get(self:GetClass())
    if SEntTable and SEntTable.GetProtector then
        return SEntTable.GetProtector(self)
    end

    return self.SC_ProtectorEntity
end

function EntityMeta:ApplyDamage(Damage, Attacker, Inflictor, HitData)
    -- WTF are you doing here..?
    if not IsValid(self) then return false end

    -- If brutality is enabled, fuck shit up
    if SC.BrutalityMode and SC.TotalDamage(Damage) > 1 then
        Damage = {
            BRUTALITY = math.random(1, 10000000000)
        }
    end

    -- Check if something is preventing us from dealing damage to the entity
    local ShouldDealDamage = hook.Run("SC.ShouldApplyDamage", self, Damage, Attacker, Inflictor, HitData)

    -- Hooks return nil by default, so we can't just check "not ShouldDealDamage"
    if ShouldDealDamage == false then
        return false
    end

    -- This is a dirty hack required because the scripted entity table doesn't override the entity meta table
    local SEntTable = scripted_ents.Get(self:GetClass())
    if SEntTable and SEntTable.ApplyDamage then
        return SEntTable.ApplyDamage(self, Damage, Attacker, Inflictor, HitData)
    end

    --Handle players in vehicles
	if self:IsVehicle() and IsValid(self:GetDriver()) then
        -- TODO: Replace this with a better check, this shouldn't really depend on the protector entity being a core.
        -- Probably won't be an issue unless we add chairs to planetary bases though.
        local core = self:GetProtector()
        if not IsValid(core) or (core:GetShieldAmount() == 0 and core:GetArmorAmount() == 0) then
            self:GetDriver():ApplyDamage(Damage, Attacker, Inflictor, HitData)
            return true
        end

        return false
    end

    -- Player Damage Logic
    if self:IsPlayer() then
        return player_manager.RunClass(self, "ApplyDamage", Damage, Attacker, Inflictor, HitData)
    end

	-- NPC Damage Logic
	if self:IsNPC() then
		--An npc is not going to stand up to very much fire from anti-ship weaponry,
		--but perhaps we should allow them to take one or two hits?
		local Damage2 = SC.TotalDamage(Damage)
		Damage2 = Damage2 * 0.1

		self:TakeDamage(Damage2, Attacker, Inflictor)

		return true
	end

	--Properly ignore protected things
	if self.SC_Immune then return false end

	--Is the entity protected?
	if self:IsProtected() then
		--Pass the Damage through
		self:GetProtector():ApplyDamage(Damage, Attacker, Inflictor, HitData)

    --If it isn't then do the normal Damage method
	else

		--Add up that Damage!
		local Damage2 = SC.TotalDamage(Damage)

		--If its capable of being Damaged, not a player, not a weapon self, and not a vehicle, then KICK ITS ASS!
		if not self.SC_Health then
			if not self:IsVehicle() and not self:IsWeapon() then
				self:TakeDamage(Damage2, Attacker, Inflictor)
			end

			return true
		end

		--Can we tank it? If not lets explode!
		if self.SC_Health > Damage2 then
			self.SC_Health = (self.SC_Health - Damage2)
		elseif self.SC_Health < Damage2 then
			SC.KillEnt(self)
		end
	end

    return true
end

--####  Explode ####--
function SC.Explode(Position, Radius, Falloff, Velocity, Damage, Attacker, Inflictor, Filter)
	-- First let's make sure we have a valid environment, because we need one for this to work properly
	local ExplosionEnvironment = GAMEMODE:GetAtmosphereAtPoint(Position)

	if not ExplosionEnvironment then
		SC.Error("Unable to find environment for explosion at position "..tostring(Position).."! What happened!?", 5)
		return
	end

	local RadiusSqr = Radius * Radius
	local FalloffSqr = Falloff * Falloff

	-- Find out what we might hit
	local PotentialHits = ents.FindInSphere(Position, Radius + Falloff)
	local CheckedHits = {}

	for Index, Ent in ipairs(PotentialHits) do
		-- If the entity has a core we want to check based on that entity instead
		local EntToCheck = IsValid(Ent:GetProtector()) and Ent:GetProtector() or Ent

		-- If the entity has been filtered, then don't bother with it
		if not CheckedHits[EntToCheck] and not (Filter and Filter[EntToCheck]) then
			-- Check if our environment matches that of the explosion, we shouldn't do damage if it doesn't
			if not EntToCheck.GetEnvironment and true or EntToCheck:GetEnvironment() == ExplosionEnvironment then
				-- Trace from the center of the explosion to the entity we're checking
				local function TestForFilter(TestEnt)
					-- Fast check, {Ent = Ent} style tables
					if IsValid(Filter[TestEnt]) then
						return false
					else
					-- Slow check, gets everything else
						for i,k in pairs(Filter) do
							if i == TestEnt or k == TestEnt then
								return false
							end
						end
					end

					return true
				end

				local Data = {
				    start = Position,
				    endpos = EntToCheck:GetPos(),
				    filter = Filter and TestForFilter or Inflictor
			    }

			    local Result = util.TraceLine(Data)
				if IsValid(Result.Entity) then
					-- Figure out what we hit, if it's not what we want then don't do anything
					local HitEnt = IsValid(Result.Entity:GetProtector()) and Result.Entity:GetProtector() or Result.Entity
					if HitEnt == EntToCheck then
						-- Finally, if we hit the entity we wanted then calculate our damage to that entity and apply it
						local DistanceSqr = Result.HitPos:DistToSqr(Result.StartPos)
						local DamageFalloff = 1 - (math.max(DistanceSqr - RadiusSqr, 0) / FalloffSqr)

						if DamageFalloff > 0 then
							local VelocityFalloff = 1
							local EntVelocity = HitEnt:GetVelocity()
							local VelocityLength = EntVelocity:Length()
							if VelocityLength > 100 then
								local dir = HitEnt:GetPos() - Position
								dir = dir / dir:Length()
								local movedir = EntVelocity / VelocityLength
								local dot = math.Clamp(dir:Dot(movedir), -1, 1)
								local vel = EntVelocity / Velocity
								VelocityFalloff = math.Remap((1 - math.Clamp(vel:Length(), 0, 1)) * dot, -1, 1, 0.25, 1.5)
							end

							local ActualDamage = {}
							for i,k in pairs(Damage) do
								ActualDamage[i] = k * DamageFalloff * VelocityFalloff
							end

							HitEnt:ApplyDamage(ActualDamage, Attacker, Inflictor, Result)
						end
					end
				end
			end
		end

		-- Make sure we don't check ships multiple times
		CheckedHits[EntToCheck] = true
	end
end