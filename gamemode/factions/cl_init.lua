AddCSLuaFile()

if not CLIENT then return end

local GM = GM
local team = team

function GM.PlayerCreationCompleted(Player, Name, Faction, Info, Success)
    hook.Run("SC.OnPlayerCreationCompleted", Player, Name, Faction, Info, Success)

    Player.CharacterCreationCompleted = true
end

net.Receive("SC.PlayerCreationCompleted", function()
    local Success = net.ReadBool()
    local Name = net.ReadString()
    local Faction = net.ReadString()
    local Info = net.ReadTable()

    GM.PlayerCreationCompleted(LocalPlayer(), Name, Faction, Info, Success)
end)

function GM.RequestPlayerCreation(Name, Faction, Info)
    -- Name and Info are ignored for now
    -- TODO: Hook up Name and Info when careers are added

    net.Start("SC.RequestPlayerCreation")
    net.WriteString(Name or "New Character")
    net.WriteString(Faction or "Civilian")
    net.WriteTable(Info or {})
    net.SendToServer()
end

hook.Add("SC.Content.PostContentMounted", "SC.StartPlayerCreation", function()
    if not LocalPlayer().CharacterCreationCompleted then
        GM.RequestPlayerCreation("", "Civilian", {})
    end
end)