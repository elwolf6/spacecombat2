local GM = GM
local LauncherUpgradeData = {}

GM.Launchers = GM.Launchers or {}
GM.Launchers.Upgrades = {}
GM.Launchers.Upgrades.LoadedUpgrades = LauncherUpgradeData

if SERVER then
    util.AddNetworkString("SC.LauncherUpgradeDataLoad")

    net.Receive("SC.LauncherUpgradeDataLoad", function(Length, Client)
        net.Start("SC.LauncherUpgradeDataLoad")
        net.WriteUInt(table.Count(LauncherUpgradeData), 16)
        for _,Data in pairs(LauncherUpgradeData) do
            Data:WriteCreationPacket()
        end
        net.Send(Client)
    end)
else
    net.Receive("SC.LauncherUpgradeDataLoad", function()
        local NumberOfComponents = net.ReadUInt(16)
        SC.Print("Got "..NumberOfComponents.." serialized Launcher Upgrades from the server!", 4)
        for I = 1, NumberOfComponents do
            local Data = GM.class.new("LauncherUpgradeData")
            Data:ReadCreationPacket()
            LauncherUpgradeData[Data:GetName()] = LauncherUpgradeData

            SC.Print("Loaded Upgrade Data "..Data:GetName(), 4)
        end


        SC.Print("Finished loading Launcher Upgrades", 4)
        hook.Run("SC.LauncherUpgradeDataLoaded")
    end)
end

function GM.Launchers.Upgrades.ReloadLauncherUpgradeData()
    -- Server loads all files from disk for launcher Upgrades
    if SERVER then
        -- Get all the files
        local Files = file.Find(SC.DataFolder.."/launchers/upgrades/*", "DATA")

        -- For each file load the data into a new launcher Upgrade class
        for _,File in pairs(Files) do
            local NewLauncherUpgradeData = GM.class.new("LauncherUpgradeData")
            if NewLauncherUpgradeData:LoadFromINI(File) then
                LauncherUpgradeData[NewLauncherUpgradeData:GetName()] = NewLauncherUpgradeData
            else
                SC.Error("Failed to load launcher upgrade from file "..File, 5)
            end
        end

        hook.Run("SC.LauncherUpgradeDataLoaded")

    -- Clients request data from the server for launcher Upgrades
    else
        -- Reset the existing data until we get the new stuff
        LauncherUpgradeData = {}
        GM.Launchers.Upgrades.LoadedUpgrades = LauncherUpgradeData

        -- Request new data from the server
        net.Start("SC.LauncherUpgradeDataLoad")
        net.SendToServer()
    end
end

hook.Remove("SC.ProjectileRecipesLoaded", "SC.LoadLauncherUpgradeData")
hook.Add("SC.ProjectileRecipesLoaded", "SC.LoadLauncherUpgradeData", function()
    GM.Launchers.Upgrades.ReloadLauncherUpgradeData()
end)

local C = GM.LCS.class({
    -- Name of the upgrade
    Name = "Upgrade",

    -- Description of the upgrade displayed in the launcher creation interface
    Description = "",

    -- What family of upgrades does this upgrade belong to
    -- Used to determine what upgrades can be placed in a slot
    Family = "Upgrade",

    -- A list of factions that can use the upgrade in launchers
    Factions = {},

    -- A list of resources required to produce a launcher with this upgrade
    ResourceCost = {},

    -- If the upgrade should be hidden from the launcher creation tool
    Hidden = false,

    -- The amount of mass added to the launcher by the upgrade
    Mass = 0,

    -- A table of data passed to the upgrade class when it is created
    ClassData = {},

    -- A list of options available for this upgrade in the launcher creation tool
    ToolOptions = {},
})

function C:init()
    -- Does nothing by default, add functionality here
end

function C:GetName()
    return self.Name
end

function C:GetDescription()
    return self.Description
end

function C:GetFamily()
    return self.Family
end

function C:IsHidden()
    return self.Hidden
end

function C:GetMass()
    return self.Mass
end

function C:GetFactions()
    return table.Copy(self.Factions)
end

function C:GetResourceCost()
    return table.Copy(self.ResourceCost)
end

function C:LoadFromINI(FileName)
    local Success, Data = GM.util.LoadINIFile(SC.DataFolder.."/launchers/upgrades/"..FileName)

    -- Load all the data into the class if we got any data back
    if Success then
        self:DeSerialize(Data)
        return true
    end

    return false
end

function C:ValidateINIData(Data)
    -- TODO: Use this function to make sure that an INI file has valid data
    -- If it returns false it should also return an error string to be printed to the console
    return true
end

function C:ValidateToolOptions(Options)
    -- Make sure the options are a table
    if not Options or not type(Options) == "table" then
        return false
    end

    -- Loop over all the options and make sure everything matches the tool options set in the data
    for Name, Value in pairs(Options) do
        -- If the option isn't supposed to be here then the data isn't valid
        local ToolOptions = self.ToolOptions[Name]
        if not ToolOptions then
            return false
        end

        -- Check to make sure the data is of the right type
        if ToolOptions.Type == "Number" then
            -- Make sure it's a number
            if type(Value) ~= "number" then
                return false
            end

            -- Make sure it's not too low
            if ToolOptions.Min and Value < ToolOptions.Min then
                return false
            end

            -- Make sure it's not too high
            if ToolOptions.Max and Value > ToolOptions.Max then
                return false
            end
        elseif ToolOptions.Type == "Color" then
            -- Make sure it's a color
            if type(Value) ~= "table" or not Value.r or not Value.g or not Value.b then
                return false
            end
        elseif ToolOptions.Type == "String" then
            -- Make sure it's a string
            if type(Value) ~= "string" then
                return false
            end
        elseif ToolOptions.Type == "Vector" then
            -- Make sure it's a vector
            if type(Value) ~= "Vector" then
                return false
            end
        elseif ToolOptions.Type == "Angle" then
            -- Make sure it's a angle
            if type(Value) ~= "Angle" then
                return false
            end
        end
    end

    return true
end

function C:GetUpgradeData(Options, Modifiers)
    -- If the options we got from the tool aren't valid them don't return anything
    if not self:ValidateToolOptions(Options) then
        return false, {}
    end

    -- This table has the basic data needed to create a upgrade
    local Data = {
        UpgradeClass = self.Class,
        Modifiers = table.Copy(Modifiers or {}),
        Mass = self.Mass
    }

    -- Add any special data needed by our upgrade class
    for Name, Value in pairs(self.ClassData) do
        Data[Name] = Value
    end

    -- Set the values of any configured data
    for Name, Info in pairs(self.ToolOptions) do
        Data[Name] = Options[Name] or Info.Default
    end

    return true, Data
end

function C:WriteCreationPacket()
    if not SERVER then return end
    net.WriteString(self.Name)
    net.WriteString(self.Description)
    net.WriteString(self.Class)
    net.WriteString(self.Family)
    net.WriteTable(self.Factions)
    net.WriteBool(self.Hidden)
    net.WriteFloat(self.Mass)
    net.WriteTable(self.ClassData)
    net.WriteTable(self.ToolOptions)
    net.WriteTable(self.ResourceCost)
end

function C:ReadCreationPacket()
    if not CLIENT then return end
    self.Name = net.ReadString()
    self.Description = net.ReadString()
    self.Class = net.ReadString()
    self.Family = net.ReadString()
    self.Factions = net.ReadTable()
    self.Hidden = net.ReadBool()
    self.Mass = net.ReadFloat()
    self.ClassData = net.ReadTable()
    self.ToolOptions = net.ReadTable()
    self.ResourceCost = net.ReadTable()
end

function C:Serialize()
    return {
        Configuration = {
            Name = self.Name,
            Description = self.Description,
            Class = self.Class,
            Family = self.Family,
            Factions = self.Factions,
            Hidden = self.Hidden,
            Mass = self.Mass,
        },

        ClassData = self.ClassData,
        ToolOptions = self.ToolOptions,
        ResourceCost = self.ResourceCost,
    }
end

function C:DeSerialize(Data)
    if Data.Configuration then
        self.Name = Data.Configuration.Name or "Upgrade"
        self.Description = Data.Configuration.Description or ""
        self.Class = Data.Configuration.Class or "LauncherUpgrade"
        self.Family = Data.Configuration.Family or "Upgrade"
        self.Factions = Data.Configuration.Factions or {}
        self.Hidden = Data.Configuration.Hidden or false
        self.Mass = Data.Configuration.Mass or 0
    end

    self.ClassData = Data.ClassData or {}
    self.ToolOptions = Data.ToolOptions or {}
    self.ResourceCost = Data.ResourceCost or {}
end

GM.class.registerClass("LauncherUpgradeData", C)