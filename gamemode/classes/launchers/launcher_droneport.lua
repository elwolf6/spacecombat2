local GM = GM

---@class Launcher_Droneport : Launcher
---@field LogisticsNode LogisticsNode #The logistics node assigned to the port
---@field DroneSize integer #The storage size of the drones created
---@field MaxDrones integer #The maximum number of drones that can be fielded by this port
---@field AvailableDrones integer #How many drones are available to be launched
---@field Drones table #A table of drones launched by this drone port
local C = GM.class.getClass("Launcher"):extends({
    LogisticsNode = nil,

    DroneSize = 1000,

    MaxDrones = 0,
    AvailableDrones = 0,
    Drones = {}
})

local BaseClass = C:getClass()

function C:IsProjectileRecipeCompatible(Recipe)
    return BaseClass.IsProjectileRecipeCompatible(self, Recipe)
end

function C:SetLauncherType(Type)
    local LauncherData = BaseClass.SetLauncherType(self, Type)
    if LauncherData then

    end
end

function C:init()
    -- TODO: Remove these when Brandon exposes them in the launcher data
    self.HasMuzzleFlash = false
    self.FireSound = "sound/weapons/physcannon/energy_bounce2.wav"

    if SERVER then
        --Set up the logistics node associated with this port
        self.LogisticsNode = GM.class.getClass("LogisticsNode"):new("DRONEPORT")
        self.LogisticsNode:SetParent(self)
        self.LogisticsNode:SetStorage(self:GetCore())

        --Drone status stuff
        self.MaxDrones = 10
        self.AvailableDrones = self.MaxDrones
    end

    BaseClass.init(self)
end

function C:CanFire()
    local CanFire = BaseClass.CanFire(self)
    return CanFire and (self.AvailableDrones > 0)
end

function C:SetOwner(NewOwner)
    self.LogisticsNode:SetOwner(NewOwner)
    BaseClass.SetOwner(self, NewOwner)
end

function C:SetCore(Core)
    self.LogisticsNode:SetStorage(Core)
    BaseClass.SetCore(self, Core)
end

function C:GetLogisticsNode()
    return self.LogisticsNode
end

--[[
========================
    Drone Management
========================
]]
function C:AddDrone(NewDrone)
    --Configure the drone's controller
    ---@type LogisticsDroneControlComponent
    local DroneControl = NewDrone:GetTargetingComponent()
    DroneControl:SetHome(self)
    DroneControl:SupplyEnergy(DroneControl:GetMaxEnergy())

    --Keep track of the drone
    self.Drones[NewDrone:GetID()] = NewDrone

    self.AvailableDrones = self.AvailableDrones - 1
end

--Called by the drone control component when the drone is removed
function C:RemoveDrone(Drone)
    self.Drones[Drone:GetID()] = nil

    self.AvailableDrones = self.AvailableDrones + 1
end

--[[
==========================
    Internal Callbacks
==========================
]]
function C:PreProjectileFired(Projectile)
    Projectile.ShouldSendNetUpdates = false

    BaseClass.PreProjectileFired(self, Projectile)
end

function C:PostProjectileFired(Projectile)
    self:AddDrone(Projectile)

    BaseClass.PostProjectileFired(self, Projectile)
end

function C:OnStartedFiring()
    -- Cancel any requests to return home to dock
    for DroneID, Drone in pairs(self.Drones) do
        Drone:GetTargetingComponent():SetReturnHomeToDock(false)
    end

    BaseClass.OnStartedFiring(self)
end

function C:OnStoppedFiring()
    -- Tell all of the drones to return home and dock
    for DroneID, Drone in pairs(self.Drones) do
        Drone:GetTargetingComponent():SetReturnHomeToDock(true)
    end

    BaseClass.OnStoppedFiring(self)
end

function C:OnRemoved()
    for DroneID, Drone in pairs(self.Drones) do
        Drone:Remove()
        self.Drones[DroneID] = nil
    end

    local Manager = GAMEMODE:GetLogisticsManager()
    Manager:UnregisterNode(self.LogisticsNode)

    BaseClass.OnRemoved(self)
end

--[[
====================================
    Entity Compatibility Methods
====================================
]]
function C:GetPos()
    return self:GetWorldPosition()
end

function C:GetProtector()
    return self:GetCore()
end

--[[
============================
    Base Class Overrides
============================
]]
function C:Serialize()
    local Data = BaseClass.Serialize(self)
    Data.LogisticsNode = self.LogisticsNode:Serialize()
    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)
    if Data.LogisticsNode then
        self.LogisticsNode:DeSerialize(Data.LogisticsNode)
    end
end

GM.class.registerClass("Launcher_Droneport", C)