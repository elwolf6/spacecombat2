--[[
/**********************************************
	Scalable Railgun Projectile Effect

	Author: Steeveeo
***********************************************/
--]]
local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({
	TrailLength = 1500,
    CurTrailLength = 0,
    Velocity = -15000
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("RailgunEffectComponent")

if CLIENT then
    Glow = GM.MaterialFromVMT(
	    "StaffGlow",
	    [["UnLitGeneric"
	    {
	        "$basetexture"		"sprites/light_glow01"
	        "$nocull" 1
	        "$additive" 1
	        "$vertexalpha" 1
	        "$vertexcolor" 1
	    }]]
    )

    Shaft = Material("effects/ar2ground2");
end

function C:init(Parent, NewColor)
	BaseClass.init(self, Parent)

    self:SetColor(NewColor or Color(144, 196, 255, 255))
end

function C:Think()
    local Parent = self:GetParent()
    self.CurTrailLength = math.Clamp(self.CurTrailLength + (-self.Velocity * FrameTime()), 0, self.TrailLength)
	local velocity = (Parent:GetAngles():Forward() + (VectorRand() * 0.15)):GetNormalized() + (Parent:GetAngles():Forward() * (self.Velocity * math.Rand(-0.1, 0.75)))
    local p = self:GetEmitter():Add("effects/flares/light-rays_001", Parent:GetPos())
	p:SetDieTime(math.Rand(0.5, 1))
	p:SetVelocity(velocity)
	p:SetAirResistance(300)
	p:SetStartAlpha(255)
	p:SetEndAlpha(255)
	p:SetStartSize(math.random(20, 30) * self:GetScale())
    p:SetEndSize(0)

    local col = self:GetColor()
    p:SetColor(col.r, col.g, col.b, col.a)
end

function C:SetScale(NewScale)
    BaseClass.SetScale(self, NewScale)
    self.TrailLength = NewScale * 1500
    self.Velocity = NewScale * -10000
end

function C:Draw()
    local Parent = self:GetParent()
	render.SetMaterial(Shaft)
    render.DrawBeam(Parent:GetPos(), Parent:GetPos() + (Parent:GetAngles():Forward() * -self.CurTrailLength), 7 * self:GetScale(), 1, 0, self:GetColor())
    render.SetMaterial(Glow)
	render.DrawSprite(Parent:GetPos(),45*self:GetScale(),45*self:GetScale(),self:GetColor())
	render.DrawSprite(Parent:GetPos(),45*self:GetScale()/2,45*self:GetScale()/2,Color(255,255,255,255))
end

function C:GetComponentClass()
    return "RailgunEffectComponent"
end

GM.class.registerClass("RailgunEffectComponent", C)