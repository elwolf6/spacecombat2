local C = GM.class.getClass("ProjectileComponent"):extends({
    Offset = Vector(0,0,0),
    OffsetAngles = Angle(0, 0, 0),
    TurretEntity = NULL
})

local BaseClass = C:getClass()

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("LocalizedMovementComponent")

-- This should always return the name of the class!
function C:GetComponentClass()
    return "LocalizedMovementComponent"
end

local InFireEvent = false
function C:OnProjectileEvent(Event, Info)
    if Event == "Initialized" then
        local Parent = self:GetParent()
        local LauncherEntity = Parent:GetLauncherEntity()
        if IsValid(LauncherEntity) then
            self.Offset, self.OffsetAngles = WorldToLocal(Parent:GetPos(), Parent:GetAngles(), LauncherEntity:GetPos(), LauncherEntity:GetAngles())
        else
            SC.Error("Using LocalizedMovementComponent without a LauncherEntity, wut!?", 5)
        end
        return
    elseif Event == "SetTurret" then
        if InFireEvent then return end
        self.TurretEntity = Info.Ent

        InFireEvent = true
        self:GetParent():FireNetworkedEvent("SetTurret", Info)
        self:GetParent():FireNetworkedEvent("UpdateLocalizedOffset", {O=self.Offset, A=self.OffsetAngles})
        InFireEvent = false
    elseif Event == "UpdateLocalizedOffset" then
        if CLIENT then
            self.Offset = Info.O
            self.OffsetAngles = Info.A
        end
    end

    BaseClass.OnProjectileEvent(self, Event, Info)
end

function C:Think()
    local Parent = self:GetParent()
    local LauncherEntity = Parent:GetLauncherEntity()
    if IsValid(self.TurretEntity) and IsValid(self.TurretEntity.PivotHolo) then
        local Pos, Ang = LocalToWorld(self.Offset, self.OffsetAngles, self.TurretEntity.PivotHolo:GetPos(), self.TurretEntity.PivotHolo:GetAngles())
        Parent:SetPos(Pos)
        Parent:SetAngles(Ang)
    elseif IsValid(LauncherEntity) then
        local Pos, Ang = LocalToWorld(self.Offset, self.OffsetAngles, LauncherEntity:GetPos(), LauncherEntity:GetAngles())
        Parent:SetPos(Pos)
        Parent:SetAngles(Ang)
    end
end

function C:ShouldThink()
    return true
end

function C:IsClientside()
    return true
end

-- This component doesn't need any network updates
function C:ReadNetworkUpdate()

end

function C:SendNetworkUpdate()

end


GM.class.registerClass("LocalizedMovementComponent", C)