local GM = GM

--Enums
local STATE_IDLE = 0
local STATE_MOVETODEMAND = 1
local STATE_MOVETOSUPPLY = 2
local STATE_RETURNHOME = 3
local STATE_EMERGENCYCHARGE = 4

---@class LogisticsDroneControlComponent : DroneControlComponent
---@field MaxTasks integer #The maximum number of tasks that the drone can hold in its courier's queue
---@field Task LogisticsTask #The current task being followed
---@field Courier LogisticsCourier #The courier object for the drone
---@field StorageSize integer #How much storage space the drone has
---@field Storage ResourceContainer[] #The resourcecontainer table of storage
---@field Range integer #How far the drone can travel before needing to visit a drone port to recharge
---@field ReachableNodes LogisticsNode[] #A table of nodes that can be reached by the drone
---@field NodeGraph table #The graph of nodes that the drone can use to pathfind
---@field Path table #The path of nodes to reach the target node
---@field PathOutdated boolean #A flag indicating that the current pathfinding path is out of date and needs to be re-solved
---@field PathfindingTarget LogisticsNode #The node that the solver should build a path to
---@field AStarSolver AStarSolver #The A* solver used to pathfind between drone ports
---@field State integer #The current state that the logistics state machine is in
---@field NextLogisticsThinkTime integer #The next time, in seconds, that the logistics state machine will think
---@field NextPathfindingTime integer #The next time, in seconds, that the pathfinding logic will fire off
---@field ReturnHomeTime integer #The time in seconds when the drone will return home
---@field ReturnHomeToDock boolean #If true, the drone will return home and "dock" itself with its home drone port (and get removed)

local C = GM.class.getClass("DroneControlComponent"):extends({
    MaxTasks = 1,
    Task = nil,
    Courier = nil,

    StorageSize = 1000,
    Storage = nil,

    Range = 5000,
    ReachableNodes = nil,
    NodeGraph = nil,
    Path = nil,
    PathOutdated = false,
    PathfindingTarget = nil,
    AStarSolver = nil,

    State = STATE_IDLE,
    NextLogisticsThinkTime = 0,
    NextPathfindingTime = 0,
    ReturnHomeTime = 0,
    ReturnHomeToDock = false
})

local BaseClass = C:getClass()
local ZeroVector = Vector(0, 0, 0)
local ZeroAngle = Angle(0, 0, 0)

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("LogisticsDroneControlComponent")

function C:init()
    BaseClass.init(self)

    if SERVER then
        self.NextLogisticsThinkTime = CurTime() + 1
        self.NectPathfindingTime = CurTime() + 1

        -- Set up the courier object
        self.Courier = GM.class.getClass("LogisticsCourier"):new()
        self.Courier:SetMaxTasks(1)
        self.Courier:SetIsAvailable(true)

        self.Courier.OnTaskAddedCallback = function(_, Task)
            if not self.Task then
                self:SetTask(Task)  -- Directly assign the task
                self.Courier:PullTask() -- Pull the task out of the courier's task queue
            end
        end

        self.Courier.OnGetPos = function(_)
            return self:GetParent():GetPos()
        end

        -- Set up the A* solver
        self.ReachableNodes = {}
        self.NodeGraph = {}
        self.Path = {}
        self.AStarSolver = GM.class.getClass("AStarSolver"):new()

        -- Set the heuristic score to use the distance between nodes
        self.AStarSolver.GetHeuristic = function(AStar, A, B)
            -- If the node can supply energy, divide the weight by 100
            if self.NodeGraph[B].CanSupplyEnergy then
                return self.NodeGraph[A].DistanceTo[B] * 0.01
            end

            return self.NodeGraph[A].DistanceTo[B]
        end

        -- Override the courier's override functions for node selection
        self.Courier.OnRequestAvailableNodes = function()
            return self.ReachableNodes
        end

        self.Courier.OnGetIsAvailable = function()
            local Available = (not self.ReturnHomeToDock) and (self:GetEnergy() > (self:GetMaxEnergy() * 0.25)) and (not self.Task)
            return Available
        end
    end
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "LogisticsDroneControlComponent"
end

function C:Serialize()
    local Data = BaseClass.Serialize(self)
    Data.MaxTasks = self.MaxTasks
    Data.StorageSize = self.StorageSize
    Data.Range = self.Range
    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)
    self.MaxTasks = Data.MaxTasks
    self.StorageSize = Data.StorageSize or 0
    self.Range = Data.Range or 0
end

-- Stop constructing the class at this point if on client
if CLIENT then
    GM.class.registerClass("LogisticsDroneControlComponent", C)
    return
end

--#region Home Return and Task Management

---Clear the current task and start the home return timer
function C:ReturnHome()
    self:SetTask(nil)
    self.ReturnHomeTime = CurTime() + 3 -- Wait 3 seconds before going home
end

---Set if the drone should stop receiving tasks and head home to dock
---@param DockHome boolean #If true, the drone will stop receiving tasks and head home as soon as it is done with the current task
function C:SetReturnHomeToDock(DockHome)
    self.ReturnHomeToDock = DockHome
end

---If the drone isn't scheduled to return home, find a new task and assign it to the drone
---@return boolean GotTask #True if the drone found a task
function C:CheckTasks()
    if not self.ReturnHomeToDock then
        local Task = self.Courier:PullTask()

        if Task then
            self:SetTask(Task)
            return true
        end
    end

    return false
end
--#endregion
--#region Behavior Callback Overrides

---Called every time the drone arrives at a target
function C:OnArrivedAtTarget()
    local CurrentTarget = self:GetTarget()

    if IsValid(CurrentTarget) then
        local EnergyAmount = math.min(self:GetMaxEnergy() - self:GetEnergy(), CurrentTarget:GetAvailableResource("Energy"))
        if EnergyAmount > 0 then
            CurrentTarget:ConsumeResource("Energy", EnergyAmount)
            self:SupplyEnergy(EnergyAmount)
        end
    end

    local State = self.State

    if State == STATE_MOVETOSUPPLY then
        local SupplyNode = self.Task.SupplyNode
        local DemandNode = self.Task.DemandNode

        if CurrentTarget:GetID() ~= SupplyNode:GetID() then
            self:TraversePath()
            return
        end
        --Arrived at supplier

        local PullSuccessful = self:PullFromSupply()
        if PullSuccessful then
            --Fly off to the demand node
            self:SolvePathToNode(DemandNode)
            self:TraversePath()
            self.State = STATE_MOVETODEMAND
        else
            --The logistics manager was a jerk and sent us to a node with no suppliable resource
            --Go home and sulk
            if not self:CheckTasks() then
                self:ReturnHome()
            end
        end

    elseif State == STATE_MOVETODEMAND then
        local SupplyNode = self.Task.SupplyNode
        local DemandNode = self.Task.DemandNode

        if CurrentTarget:GetID() ~= DemandNode:GetID() then
            self:TraversePath()
            return
        end

        --Arrived at demander

        local PushSuccessful = self:PushToDemand()

        if PushSuccessful then
            if not self:CheckTasks() then
                self:ReturnHome()
            end
        else
            --The demander somehow ended up with more of the resource than we were sent to supply to it
            if DemandNode ~= SupplyNode then
                --Create a new task for ourself to return the resource to the supplying node and return whatever we took
                self.Task.DemandNode = SupplyNode
                self:SolvePathToNode(DemandNode)
                self:TraversePath()
            else
                -- We already tried returning the resource and it didn't fit!
                -- Void the resource
                self:ConsumeResource(self.Task.ResourceName, self.Task.Amount)
                -- Get a new task, or return home
                if not self:CheckTasks() then
                    self:ReturnHome()
                end

            end
        end
    elseif State == STATE_RETURNHOME then
        if CurrentTarget:GetID() ~= self:GetHome():GetLogisticsNode():GetID() then
            self:TraversePath()
            return
        end
        --Arrived at home
        --This is handled in the OnArrivedAtHome callback
    elseif State == STATE_EMERGENCYCHARGE then
        if CurrentTarget:GetID() ~= self.PathfindingTarget:GetID() then
            self:TraversePath()
            return
        end

        self.State = STATE_IDLE
    end
end

---Called when the current target changes positions
---@param NewPos vector #The new position of the target
function C:OnTargetPositionChanged(NewPos)
    self.PathOutdated = true
    BaseClass.OnTargetPositionChanged(self, NewPos)
end

---Called when the drone arrives at its "home"
function C:OnArrivedAtHome()
    if self.ReturnHomeToDock then
        self:GetParent():Remove()
        return
    end

    if self.State == STATE_RETURNHOME then
        self.State = STATE_IDLE
    end

    BaseClass.OnArrivedAtHome(self)
end

---Called to set the drone's target to home
function C:SetTargetToHome()
    local HomeNode = self:GetHome():GetLogisticsNode()
    self.State = STATE_RETURNHOME
    if self.ReachableNodes[HomeNode:GetID()] then
        self:SolvePathToNode(HomeNode)
        self:TraversePath()
    else
        BaseClass.SetTargetToHome(self)
    end
end

--#endregion
--#region Member Variable Accessors

---Set the task for the drone to carry out
---@param Task LogisticsTask #The task to carry out
function C:SetTask(Task)
    self.Task = Task

    if not Task then
        self.State = STATE_IDLE
    else
        self.PathOutdated = true    -- Indicate that the pathfinding path needs to be rebuilt
        self:SolvePathToNode(Task.SupplyNode)
        self:TraversePath()
        self.State = STATE_MOVETOSUPPLY
    end
end

---Get the current task being fulfilled by the drone
---@return LogisticsTask
function C:GetTask()
    return self.Task
end

--#endregion
--#region Projectile Events
function C:OnProjectileEvent(Event, Info)
    if Event == "Removed" then
        self.Courier:Remove()

        if IsValid(self:GetHome()) then
            self:GetHome():RemoveDrone(self:GetParent())
        end
        return
    elseif Event == "Initialized" then
        local Parent = self:GetParent()
        self.Courier:SetOwner(Parent:GetOwner())
        self.Courier:SetParent(Parent)
        self.Courier:SetStorageSize(self.StorageSize)
        self.Courier:SetMaxTasks(self.MaxTasks)

        self:ResizeStorage()
    end

    BaseClass.OnProjectileEvent(self, Event, Info)
end

--#endregion
--#region Node Resource Manipulation

---Carry out the supply pulling side transaction of the current task
---@return boolean PullSuccessful
function C:PullFromSupply()
    local ResName = self.Task.ResourceName
    local ResAmount = self.Task.Amount
    local SupplyNode = self.Task.SupplyNode


    --Find out how much we can pull
    local SupplierAmount = SupplyNode:GetAmount(ResName)
    local TransferAmount = math.min(SupplierAmount, ResAmount)

    --Can't transfer anything. Return false.
    if not (TransferAmount > 0) then
        return false
    end

    --Pull the resource from the supplying node into our storage
    SupplyNode:CourierConsumeResource(ResName, TransferAmount) --Pull while updaing the outbound table
    self:SupplyResource(ResName, TransferAmount)

    return true
end

---Carry out the demand pushing side transaction of the current task
---@return boolean PushSiccessful
function C:PushToDemand()
    local ResName = self.Task.ResourceName
    local ResAmount = self.Task.Amount
    local DemandNode = self.Task.DemandNode

    --Find out how much we can push
    local DemanderAmount = math.max(DemandNode:GetMaxAmount(ResName) - DemandNode:GetAmount(ResName), 0)
    local TransferAmount = math.min(DemanderAmount, ResAmount)

    --Can't transfer anything. Return false.
    if not (TransferAmount > 0) then
        return false
    end

    --Push the resource from our storage into the demanding node
    DemandNode:CourierSupplyResource(ResName, TransferAmount)
    self:ConsumeResource(ResName, TransferAmount)

    return true
end

--#endregion
--#region Storage Management

---Resize the drone's storage volume, or create the storage if there is none yet
function C:ResizeStorage()
    if not self.Storage then
        self.Storage = {}
        self.Storage["Energy"] = GAMEMODE:NewStorage("Energy", self.StorageSize * 100, CONTAINERMODE_AUTOENLARGE)
        self.Storage["Cargo"] = GAMEMODE:NewStorage("Cargo", self.StorageSize, CONTAINERMODE_AUTOENLARGE)
        self.Storage["Ammo"] = GAMEMODE:NewStorage("Ammo", self.StorageSize, CONTAINERMODE_AUTOENLARGE)
        self.Storage["Liquid"] = GAMEMODE:NewStorage("Liquid", self.StorageSize, CONTAINERMODE_AUTOENLARGE)
        self.Storage["Gas"] = GAMEMODE:NewStorage("Gas", self.StorageSize, CONTAINERMODE_AUTOENLARGE)
    else
        self.Storage["Energy"]:Resize(self.StorageSize * 100, true)
        self.Storage["Cargo"]:Resize(self.StorageSize, true)
        self.Storage["Ammo"]:Resize(self.StorageSize, true)
        self.Storage["Liquid"]:Resize(self.StorageSize, true)
        self.Storage["Gas"]:Resize(self.StorageSize, true)
    end
end

---Gets the resourcecontainer of the specified type
---@param StorageType string
---@return ResourceContainer
function C:GetStorageOfType(StorageType)
    return self.Storage[StorageType]
end

---Supply a resource to the drone's storage
---@param ResourceName string
---@param ResourceAmount integer
---@return boolean SupplySuccessful
function C:SupplyResource(ResourceName, ResourceAmount)
    local Resource = GAMEMODE:GetResourceFromName(ResourceName)
    local ResourceType = Resource:GetType()
    return self:GetStorageOfType(ResourceType):SupplyResource(ResourceName, ResourceAmount)
end

---Consume a resource from the drone's storage
---@param ResourceName string
---@param ResourceAmount integer
---@return boolean ConsumptionSuccessful
function C:ConsumeResource(ResourceName, ResourceAmount)
    local Resource = GAMEMODE:GetResourceFromName(ResourceName)
    local ResourceType = Resource:GetType()
    return self:GetStorageOfType(ResourceType):ConsumeResource(ResourceName, ResourceAmount)
end
--#endregion
--#region Pathfinding

---Populate the ReachableNodes table with nodes that the drone can visit and the node graph
function C:BuildNodeData()
    local MyOwner = self:GetParent():GetOwner()
    local MaxDistance = self.Range
    local AvailableNodes = {}

    local NodeIDs = {}
    -- Build the list of nodes that the courier can visit
    for NodeID, Node in pairs(GM:GetLogisticsManager():GetNodes()) do
        if IsValid(Node) and Node:GetIsAvailable() and Node:GetOwner() == MyOwner then
            AvailableNodes[NodeID] = Node
            table.insert(NodeIDs, NodeID)
        end
    end

    -- TODO: Use a more optimal algorithm to construct the node graph

    -- Build the list of nodes that the courier can reach
    local function GetConnectedNodes(Pos, CheckedNodes)
        for NodeID, Node in pairs(AvailableNodes) do
            if not CheckedNodes[NodeID] then
                local NodePos = Node:GetPos()
                local Distance = Pos:Distance(NodePos)
                if Distance < MaxDistance then
                    CheckedNodes[NodeID] = Node
                    GetConnectedNodes(NodePos, CheckedNodes)
                end
            end
        end
    end

    local MyPos = self:GetParent():GetPos()

    self.ReachableNodes = {}
    local ReachableNodes = self.ReachableNodes
    GetConnectedNodes(MyPos, ReachableNodes)

    -- Build the node graph of reachable nodes
    self.NodeGraph = {}
    local NodeGraph = self.NodeGraph
    for NodeID, Node in pairs(ReachableNodes) do
        local ConnectedNodes = {}
        local NodeDistances = {}
        local NodePos = Node:GetPos()

        for OtherNodeID, OtherNode in pairs(ReachableNodes) do
            if NodeID ~= OtherNodeID then
                local Distance = NodePos:Distance(OtherNode:GetPos())
                if Distance < MaxDistance then
                    table.insert(ConnectedNodes, OtherNodeID)
                    NodeDistances[OtherNodeID] = Distance
                end
            end
        end

        NodeGraph[NodeID] = {
            ConnectedNodes = ConnectedNodes,
            DistanceTo = NodeDistances,
            CanSupplyEnergy = Node:GetAvailableResource("Energy") > 100
        }
    end
end

---Use the A* solver to build a path to a node
---@param TargetNode LogisticsNode #The node to pathfind to
function C:SolvePathToNode(TargetNode)
    self.PathfindingTarget = TargetNode
    local MyPos = self:GetParent():GetPos()
    local StartNodeID = 0
    local ClosestDistance = 9999999
    for NodeID, Node in pairs(self.ReachableNodes) do
        local Distance = MyPos:Distance(Node:GetPos())
        if Distance < ClosestDistance then
            StartNodeID = Node:GetID()
            ClosestDistance = Distance
        end
    end

    self.AStarSolver:Setup(StartNodeID, TargetNode:GetID(), self.NodeGraph)
    self.AStarSolver:Solve()
    self.Path = self.AStarSolver:GetPath()
    self.PathOutdated = false
end

---Pull the next node from the path
---@return LogisticsNode NextNode #The next node in the path
function C:PullNextTargetNode()
    local TargetID = table.remove(self.Path, 1)
    if not TargetID then
        SC.Error(string.format("[Logsitcs Drone %d]No more targets in path!", self.Courier:GetID()), 4)
        return self:GetHome():GetLogisticsNode()
    end

    local Node = GM:GetLogisticsManager():GetNodes()[TargetID]
    -- If the target is not valid, pull the next node in the path
    if not IsValid(Node) then
        return self:PullNextTargetNode()
    end

    return Node
end

---Pull a node from the path and set it as the target
function C:TraversePath()
    local Node = self:PullNextTargetNode()
    self:SetTarget(Node)
end

---Get the list of nodes that are rechable for this drone
---@return LogisticsNode[]
function C:GetReachableNodes()
    return self.ReachableNodes()
end

---Find the best node to recharge at and go there to recharge
function C:SetTargetToRecharge()
    local BestNode = nil
    local BestScore = math.huge
    for NodeID, Node in pairs(self.ReachableNodes) do
        -- TODO: Allow A* to take a list of end nodes and return a list of their scores
        if Node:GetAvailableResource("Energy") > 100 then
            self:SolvePathToNode(Node)
            local Score = self.AStarSolver:GetGScore()[NodeID]
            if Score < BestScore then
                BestNode = Node
                BestScore = Score
            end
        end
    end

    if IsValid(BestNode) then
        self.State = STATE_EMERGENCYCHARGE
        self:SolvePathToNode(BestNode)
        self:TraversePath()
    else
        self:SetTargetToHome()
    end
end


--#endregion

---Called whenever
function C:Think()
    if self.NextLogisticsThinkTime < CurTime() then
        self.NextLogisticsThinkTime = CurTime() + 1

        local EnergyPercent = self:GetEnergy() / self:GetMaxEnergy()
        local State = self.State

        if (State == STATE_IDLE) or (State == STATE_RETURNHOME) then
            self:CheckTasks()
        elseif State ~= STATE_RETURNHOME then
            -- Go home if the target becomes invalid
            if not IsValid(self:GetTarget()) then
                self:ReturnHome()
            end
        end

        if (State == STATE_IDLE) then
            if self.AtHome then
                -- Charge up if we're home and idle
                local HomeNode =  self:GetHome():GetLogisticsNode()
                local EnergyAmount = math.min(self:GetMaxEnergy() - self:GetEnergy(), HomeNode:GetAvailableResource("Energy"))
                if EnergyAmount > 0 then
                    HomeNode:ConsumeResource("Energy", EnergyAmount)
                    self:SupplyEnergy(EnergyAmount)
                end
            else
                -- Return home if we're idle for too long
                if self.ReturnHomeTime < CurTime() then
                    self:SetTargetToHome()
                end
            end
        end

        if State == STATE_MOVETOSUPPLY then
            local Task = self.Task
            local ResourceName = Task.ResourceName
            local SupplyNode = Task.SupplyNode
            local DemandNode = Task.DemandNode

            -- Stop what we're doing and recharge if the energy reserves are too low
            if EnergyPercent < 0.25 then
                SupplyNode:SubtractOutbound(ResourceName, Task.Amount)
                DemandNode:SubtractInbound(ResourceName, Task.Amount)
                self.Task = nil
                self.Courier:DiscardTasks()
                self:SetTargetToRecharge()
            end

            -- Return home if the supplying node has run out of the resource
            if SupplyNode:GetAmount(ResourceName) < 1 then
                self:ReturnHome()
            end

            -- Return home if the demand node can't fit the any more of the resource
            if (DemandNode:GetMaxAmount(ResourceName) - DemandNode:GetAmount(ResourceName)) < 1 then
                self:ReturnHome()
            end

            -- Rebuild the path if it is out of date
            if self.PathOutdated then
                self:SolvePathToNode(SupplyNode)
                self:TraversePath()
            end
        elseif State == STATE_MOVETODEMAND then
            local Task = self.Task
            local ResourceName = Task.ResourceName
            local SupplyNode = Task.SupplyNode
            local DemandNode = Task.DemandNode

            -- Stop what we're doing and go recharge if the energy reserves are too low
            if EnergyPercent < 0.25 then
                if DemandNode ~= SupplyNode then
                    DemandNode:SubtractInbound(ResourceName, Task.Amount)
                end
                self.Task = nil
                self.Courier:DiscardTasks()
                self:SetTargetToRecharge()
            end

            -- Return the resources we pulled back to the supplying node if the demanding node can't fit any
            if (DemandNode:GetMaxAmount(ResourceName) - DemandNode:GetAmount(ResourceName)) < 1 then
                -- If we aren't returning resources to the supply node, return to the supply node
                if DemandNode ~= SupplyNode then
                    -- Modify the current task to return to the supplying node and return whatever we took
                    self.Task.DemandNode = SupplyNode
                    self:SolvePathToNode(DemandNode)
                    self:TraversePath()
                else
                    -- Void the resource and go home
                    self:ConsumeResource(ResourceName, Task.Amount)
                    self:ReturnHome()
                end
            end

            if self.PathOutdated then
                self:SolvePathToNode(DemandNode)
                self:TraversePath()
            end
        end
    end

    if self.NextPathfindingTime < CurTime() then
        self.NextPathfindingTime = CurTime() + 5

        self:BuildNodeData()
    end

    BaseClass.Think(self)
end

GM.class.registerClass("LogisticsDroneControlComponent", C)

