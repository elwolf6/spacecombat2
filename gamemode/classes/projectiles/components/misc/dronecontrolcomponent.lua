local GM = GM

---@alias DroneTarget entity|LogisticsNode|Launcher_Droneport

---@class DroneControlComponent : ProjectileComponent
---@field Home Launcher_Droneport #The drone's home droneport launcher
---@field Target DroneTarget #The target that the drone is trying to move towards
---@field Arrived boolean #A flag indicating that the drone has arrived at its target
---@field NextTargetPosUpdateTime integer #The time, in seconds, when the drone will send the next target position update to the client
---@field LastTargetPos vector #The position that the target was in the last time the update was sent to the client
---@field TargetPos vector #The current target position
---@field TargetVelocity vector #The velocity of the target
---@field TurnRate number #How quickly the drone turns to face its target
---@field MaxSpeed number #The maximum speed of the drone when powered
---@field Speed number #The current maximum speed of the drone
---@field AccelerationDistance number #How far from the target the drone has to be before decelerating
---@field MaxEnergy integer #The maximum amount of energy that the drone can store
---@field EnergyUsage integer #How much energy is used per second when moving
---@field EnergyStorage ResourceContainer #The drone's storage for energy
---@field AtHome boolean #A flag indicating if the drone is at its home
local C = GM.class.getClass("ProjectileComponent"):extends({
    Home = nil,
    Target = nil,
    Arrived = false,
    NextTargetPosUpdateTime = 0,
    LastTargetPos = Vector(0,0,0),
    TargetPos = Vector(0,0,0),
    TargetVelocity = Vector(0,0,0),

    TurnRate = 0.1,
    Speed = 2000,
    MaxSpeed = 2000,
    AccelerationDistance = 1000,

    MaxEnergy = 10000,
    EnergyUsage = 100,
    EnergyStorage = nil,

    AtHome = true
})

local ZeroVector = Vector(0, 0, 0)
local BaseClass = C:getClass()


-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("DroneControlComponent")

function C:init()
    self.LastTargetPos = ZeroVector
    if SERVER then
        self.EnergyStorage = GM:NewStorage("Energy", self.MaxEnergy, CONTAINERMODE_AUTOENLARGE)
    end
    BaseClass.init(self)
end

--#region Internal Callbacks
-- These get overriden by the drone behavior class
-- Do not override these externally!

---Called when the drone sets home as its target
function C:OnReturnToHome()

end

---Called when the target position changes
---@param NewTargetPos vector #The new target position
function C:OnTargetPositionChanged(NewTargetPos)

end

---Called when the drone starts moving towards a target
function C:OnMovingToTarget()
    if self.AtHome then
        self.AtHome = false
    end
end

---Called when the drone arrives at the target
function C:OnArrivedAtTarget()

end

---Called when the drone arrives at home
function C:OnArrivedAtHome()
    self.AtHome = true
end

--#endregion
--#region External Callbacks
-- Callbacks that are meant to be overridden externally

---Called when the drone arrives at the target
function C:OnArrivedAtTargetCallback()

end

---Called when the drone arrives at home
function C:OnArrivedAtHomeCallback()

end

--#endregion
--#region Projectile Events

---Called when an event is broadcasted on the projectile
---@param Event string #The name of the event
---@param Info table #The data associated with the event
function C:OnProjectileEvent(Event, Info)
    if CLIENT then
        if Event == "SetTargetPosition" then
            self.TargetPos = Info.Target
        elseif Event == "SetTargetVelocity" then
            self.TargetVelocity = Info.TargetVelocity
        elseif Event == "SetAccelerationDistance" then
            self.AccelerationDistance = Info.AccelerationDistance or 1000
        elseif Event == "SetSpeed" then
            self.Speed = Info.Speed or self.Speed
        end
    end

    if Event == "Initialized" then
        self.Speed = self.MaxSpeed
    end

    BaseClass.OnProjectileEvent(self, Event, Info)
end

--#endregion
--#region Member Variable Accessors

---Set a new target for the drone
---@param NewTarget DroneTarget #The new target (duh)
function C:SetTarget(NewTarget)
    if IsValid(NewTarget) then
        local TargetPos = NewTarget:GetPos()
        self.LastTargetPos = TargetPos
        self:GetParent():FireNetworkedEvent("SetTargetPosition", {Target = TargetPos})
        self.Target = NewTarget
    end
end

---Get the target of the drone
---@return LogisticsNode
function C:GetTarget()
    return self.Target
end

---Set the home of the drone
---@param NewHome Launcher_Droneport
function C:SetHome(NewHome)
    self.Home = NewHome
end

---Get the home of the drone
---@return Launcher_Droneport
function C:GetHome()
    return self.Home
end

---Get the drone's energy storage
---@return ResourceContainer EnergyStorage
function C:GetEnergyStorage()
    return self.EnergyStorage
end

---Get the maximum amount of energy that the drone can store
---@return integer MaxEnergy
function C:GetMaxEnergy()
    return self.MaxEnergy
end

---Set the acceleration distance of the drone
---@param Distance number
function C:SetAccelerationDistance(Distance)
    self.AccelerationDistance = Distance
    self:GetParent():FireNetworkedEvent("SetAccelerationDistance", {AccelerationDistance = Distance})
end

---Set the current speed for the drone
---@param Speed number
function C:SetSpeed(Speed)
    self.Speed = Speed
    self:GetParent():FireNetworkedEvent("SetSpeed", {Speed = Speed})
end

---Called by the projectile object when it is taking parentship of this component object
---@param NewParent Projectile #The parent projectile
function C:SetParent(NewParent)
    BaseClass.SetParent(self, NewParent)

    -- Set ourself as the targeting component
    if self:GetParent() ~= nil then
        self:GetParent():SetTargetingComponent(self)
    end
end

--#endregion
--#region Misc. Methods

---Drop everything and set the target to the drone's home
function C:SetTargetToHome()
    if IsValid(self.Home) then
        self:SetTarget(self.Home:GetLogisticsNode())
        self:OnReturnToHome()
    else
        -- No home!
        if SERVER then
            local Parent = self:GetParent()
            Parent:Remove()
        end
    end
end

---Set the drone's velocity and rotation
function C:ProcessMovement()
    local Parent = self:GetParent()
    local MovementComponent = Parent:GetMovementComponent()
    local TargetPos = self.TargetPos
    local MyPos = Parent:GetPos()
    local TargetDistance = MyPos:Distance(TargetPos)

    if TargetDistance > 100 then
        if SERVER then
            if self.Arrived then
                self:OnMovingToTarget()
                self.Arrived = false
            end

            -- Consume energy if we're moving
            self:ConsumeEnergy(self.EnergyUsage * FrameTime())
        end
        local Forward = Parent:GetAngles():Forward()
        local DirToTarget = MyPos - TargetPos
        DirToTarget = DirToTarget / DirToTarget:Length()
        local Dot = (2 - (Forward:Dot(DirToTarget) + 1))

        local Velocity

        if Dot > 1.9 then
            Velocity = -DirToTarget * (self.Speed * math.Clamp(TargetDistance / self.AccelerationDistance, 0, 1))
        else
            Velocity = MovementComponent:GetVelocity() * 0.5
        end

        MovementComponent:SetVelocity(Velocity)

        Parent:SetAngles(((-DirToTarget * (FrameTime() * self.TurnRate)) + Forward):Angle())
    else
        MovementComponent:SetVelocity(MovementComponent:GetVelocity() * 0.5)
        --Arrived at the target
        if SERVER and not self.ArrivedAtTarget then
            local Target = self.Target

            self:OnArrivedAtTarget()
            self:OnArrivedAtTargetCallback()

            if Target == self.Home.LogisticsNode then
                self:OnArrivedAtHomeCallback()  -- Call the external callback first since the internal one might delete the parent
                self:OnArrivedAtHome()
            end

            self.Arrived = true
        end
    end
end

if SERVER then
    ---Supply energy to the drone
    ---@param EnergyAmount integer #How much energy to add to the drone
    function C:SupplyEnergy(EnergyAmount)
        self.EnergyStorage:SupplyResource("Energy", EnergyAmount)
    end

    ---Consume energy from the drone
    ---@param EnergyAmount integer #How much energy to remove from the drone
    function C:ConsumeEnergy(EnergyAmount)
        self.EnergyStorage:ConsumeResource("Energy", EnergyAmount)
    end

    ---Get how much energy is currently stored in the drone
    ---@return integer EnergyAmount
    function C:GetEnergy()
        return self.EnergyStorage:GetAmount("Energy")
    end
end

---Called whenever stuff does things
function C:Think()
    local Parent = self:GetParent()

    if SERVER then
        -- Poof out of existence if our home is destroyed
        if not IsValid(self.Home) then Parent:Remove() return end
        -- Network the target position to the client
        local Target = self.Target
        if IsValid(Target) then
            local TargetPos = Target:GetPos()
            if TargetPos ~= self.LastTargetPos then
                self.TargetVelocity = (TargetPos - self.LastTargetPos) / FrameTime()
                self:OnTargetPositionChanged(TargetPos)

                if CurTime() > self.NextTargetPosUpdateTime then
                    Parent:FireNetworkedEvent("SetTargetPosition", {Target = TargetPos})
                    Parent:FireNetworkedEvent("SetTargetVelocity", {TargetVelocity = self.TargetVelocity})
                    self.NextTargetPosUpdateTime = CurTime() + 1
                end

                self.LastTargetPos = TargetPos
            end

            self.TargetPos = TargetPos
        else
            self:SetTargetToHome()
        end

        local EnergyAmount = self:GetEnergy()

        if (EnergyAmount < 100) and (self.Speed > 500) then
            self:SetSpeed(500)
        elseif (EnergyAmount > 100) and (self.Speed == 500) then
            self:SetSpeed(self.MaxSpeed)
        end
    end

    -- Process the drone's movement
    self:ProcessMovement()

    -- Parent got deleted by one of the callbacks. Return early so that the baseclass doesn't generate errors
    if not IsValid(Parent) then return end

    BaseClass.Think(self)
end

--#endregion
--#region Base Class Member Methods

---If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return true
end

---If this returns true then the C:Think function will be called later than normal
---Useful when you need to do something after the projectile has moved.
function C:UsesLateThink()
    return false
end

---If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return true
end

---This should always return the name of the class!
---@return string ComponentClass
function C:GetComponentClass()
    return "DroneControlComponent"
end

---This function is used to send data to the client for replication.
function C:WriteCreationPacket()
    if not SERVER then return end
    BaseClass.WriteCreationPacket(self)

    net.WriteVector(self.TargetPos, false)
    net.WriteFloat(self.TurnRate)
    net.WriteFloat(self.MaxSpeed)
    net.WriteFloat(self.AccelerationDistance)
end

---This function reads the data sent to the client with the WriteCreationPacket function.
function C:ReadCreationPacket()
    if not CLIENT then return end
    BaseClass.ReadCreationPacket(self)

    self.TargetPos = net.ReadVector()
    self.TurnRate = net.ReadFloat()
    self.MaxSpeed = net.ReadFloat()
    self.AccelerationDistance = net.ReadFloat()
end

---Used to package up the server-side data and send it to the client
---@return table Data #Serialized data table
function C:Serialize()
    local Data = BaseClass.Serialize(self)
    Data.TurnRate = self.TurnRate
    Data.MaxSpeed = self.MaxSpeed
    Data.Speed = self.Speed
    Data.AccelerationDistance = self.AccelerationDistance
    Data.MaxEnergy = self.MaxEnergy
    Data.EnergyUsage = self.EnergyUsage

    return Data
end

---Used to read in the packaged server-side data and apply it on the client
---Also used when reading data from the projectile component in the recipe
---@param Data table #The serialized data to unpack
function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)
    self.TurnRate = Data.TurnRate or 0
    self.MaxSpeed = Data.MaxSpeed or 1000
    self.Speed = Data.Speed or 1000
    self.AccelerationDistance = Data.AccelerationDistance or 1
    self.MaxEnergy = Data.MaxEnergy or 10000
    self.EnergyUsage = Data.EnergyUsage or 100
end

--#endregion

GM.class.registerClass("DroneControlComponent", C)