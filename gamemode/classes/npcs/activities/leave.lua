local GM = GM

---@class NPCShip_Leave : NPCShip_Activity_Base #A transport NPC ship
local C = GM.class.getClass("NPCShip_Activity_Base"):extends({

})

local BaseClass = C:getClass()

--#region Thinking

function C:Think()
    self.NPCShip:Remove()
end


--#endregion

GM.class.registerClass("NPCShip_Activity_Leave", C)