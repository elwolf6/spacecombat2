---@alias entity table
if CLIENT then return end


local GM = GM

-- Targeter filter enums
local TARGETER_FILTERID_MINING = 1
local TARGETER_FILTERID_SALVAGE = 2
local TARGETER_FILTERID_COMBAT = 3

-- Ship class enums
local CLASS_DRONE = 1
local CLASS_FIGHTER = 2
local CLASS_FRIGATE = 3
local CLASS_CRUISER = 4
local CLASS_BATTLECRUISER = 5
local CLASS_BATTLESHIP = 6
local CLASS_DREADNAUGHT = 7
local CLASS_TITAN = 8

local ShipSizes = {
    Drone = 1,
    Fighter = 2,
    Frigate = 3,
    Cruiser = 4,
    Battlecruiser = 5,
    Battleship = 6,
    Dreadnaught = 7,
    Titan = 8
}

---@class NPCShip_Activity_Combat : NPCShip_Activity_Base #A transport NPC ship
---@field TargetShip entity #The target ship core that the combat ship is trying to kill
---@field TargetGyropod entity #The target ship's gyropod
---@field TargetFollowDistance number #How far out to follow the target ship
---@field Attacking boolean #A flag indicating that the ship is currently attacking another ship
---@field OrbitTarget boolean #A flag indicating that we should orbit the target
---@field Attackers table #A table of ships that are atttacking us
---@field TargetInEnvironment boolean #A flag indicating that the current target is hiding in an environment
---@field TargetForgivenessCooldown number #The time in seconds that we will forgive the target if they remain in an environment
---@field NoTargetLeaveTime number #The time in seconds that we will leave the map if we have no targets
---@field PreviousTargeterFilterIndex number #The filter index of the targeter upon starting this activity
local C = GM.class.getClass("NPCShip_Activity_Base"):extends({
    TargetShip = nil,
    TargetGyropod = nil,
    TargetFollowDistance = 0,
    Attacking = false,
    OrbitTarget = false,
    Attackers = nil,
    TargetInEnvironment = false,

    TargetForgivenessCooldown = 0,
    NoTargetLeaveTime = 0,

    PreviousTargeterFilterIndex = 0
})

local BaseClass = C:getClass()

---Class initialization
---@param NPCShip NPCShip #The NPC ship to assign this activity to
function C:init(NPCShip)
    BaseClass.init(self, NPCShip)

    self.Attackers = {}
    self.NoTargetLeaveTime = CurTime() + 300
end

--#region Targeting

---Get the index in the targeter's target list of an entity
---@param Entity entity #The entity to find the index of
---@return integer TargetIndex #The index of the target, or 0 if not found
function C:GetTargeterIndex(Entity)
    local Targeter = self.NPCShip:GetTargeter()
    for TargetIndex, Target in ipairs(Targeter:GetTargetList()) do
        if Target == Entity then
            return TargetIndex
        end
    end

    return 0
end

---Set the target ship that we will try to attack
---@param CoreIndex number #The index of the target in the targeter's target list
function C:SetTarget(CoreIndex)
    local Ship = self.NPCShip
    local Targeter = Ship:GetTargeter()
    local Core = Targeter:GetTargetList()[CoreIndex]
    local CoreIsValid = IsValid(Core)
    self.TargetShip = Core

    -- Tell the targeter which entity to target, NULL if 0
    Targeter:TriggerInput("Target Override", CoreIndex)
    Targeter:SetFireGroupFiring("Primary", CoreIsValid)
    Targeter:SetFireGroupFiring("Secondary", CoreIsValid)

    -- Set our flags and variables
    self.Attacking = CoreIsValid
    if CoreIsValid then
        local TargetShipClass = ShipSizes[Core:GetShipClass()]
        self.OrbitTarget = Ship:GetShipClass() < TargetShipClass
        self.TargetFollowDistance = (Core.sigrad * 3) + Ship:GetRadius()

        self.TargetGyropod = Core:GetGyropod()


        --Change the NPCShip's movement behavior for combat
        Ship:SetMaintainPosition(true)
        Ship:SetStoppingDistance(0)

        if self.OrbitTarget then
            Ship:SetTargetDistance(0)
        else
            Ship:SetTargetDistance(self.TargetFollowDistance)
        end
    else
        --Reset our no-target leave timer if we have no target
        self.NoTargetLeaveTime = CurTime() + 300

        --Reset the NPCShip's movement behavior
        Ship:SetMaintainPosition(false)
        Ship:SetStoppingDistance(Ship:GetRadius())
        Ship:SetTargetDistance(1000)

        --Set the NPCShip's navigation variables
        Ship:ClearWaypoints()
        Ship:SetTargetMovePos(nil)
        Ship:SetThrottleOverride(vector_origin)
    end
end

--#endregion
--#region Callback Overrides


function C:OnArrivedAtTarget()
end

function C:OnDepartedFromTarget()
end

function C:OnArrivedAtLastWaypoint()
end


---Called when the NPC Ship AI switches to this activty
---@param OldActivity string #The name of the activity that we just switched from
function C:OnActivityStarted(OldActivity)
    local Targeter = self.NPCShip:GetTargeter()
    self.PreviousTargeterFilterIndex = Targeter.FilterID
    Targeter:SetFilter(TARGETER_FILTERID_COMBAT)
end

---Called when the NPC Ship AI switches away from this activity
---@param NewActivity string #The name of the activity that we are switching to
function C:OnActivityFinished(NewActivity)
    --If we have to switch activities and there's a target ship, stop firing our weapons and stop targeting it
    if IsValid(self.TargetShip) then
        self:SetTarget(0)
    end

    -- Restore the targeter to its previous state
    local Targeter = self.NPCShip:GetTargeter()
    Targeter:SetFilter(self.PreviousTargeterFilterIndex)
end

--#endregion
--#region Target Processing

---Return the latest ship that has attacked us
---@return boolean AttackerFound #A flag indicating that an attacker was found and has been set as our target
function C:AttackAttacker()
    for Attacker, _ in pairs(self.Attackers) do
        if IsValid(Attacker) and Attacker.IsAlive then
            local TargetIndex = self:GetTargeterIndex(Attacker)
            if TargetIndex > 0 then
                self:SetTarget(TargetIndex)
                self.Attackers[Attacker] = nil
                return true
            end
        else
            self.Attackers[Attacker] = nil
        end
    end

    return false
end

---Try to find a target for us to start attacking
function C:FindValidTarget()
    --Don't look for a target if something is attacking us
    if self:AttackAttacker() then return end

    local Ship = self.NPCShip
    local Core = Ship:GetCore()

    local Targeter = Core:GetTargeter()
    if not IsValid(Targeter) then return end
    if Targeter.FilterID ~= TARGETER_FILTERID_COMBAT then
        Targeter:SetFilter(TARGETER_FILTERID_COMBAT)
    end

    local Manager = GM:GetNPCShipManager()
    local MyFaction = self.NPCShip:GetFaction()

    local MostHatedCoreIndex = 0
    local LowestRelation = 0

    local Targets = Targeter:GetTargetList()
    for TargetIndex, Target in ipairs(Targets) do
        if IsValid(Target) and (Target ~= Core) and Target.IsAlive and not (Target:GetOctreeBuilding() or Target.ConstraintsChanged) then
            local ShipFaction = self:GetCoreFaction(Target)
            local Relation = Manager:GetFactionRelation(MyFaction, ShipFaction)
            if Relation < LowestRelation then
                MostHatedCoreIndex = TargetIndex
                LowestRelation = Relation
            end
        end
    end

    if MostHatedCoreIndex > 0 then
        self:SetTarget(MostHatedCoreIndex)
    end
end

---Check if the target is in an environment and handle it
---@return boolean Forgiven #If true, the target was forgiven
function C:ManageTargetInEnvironment()
    if self:GetCoreInEnvironment(self.TargetShip) then
        if not self.TargetInEnvironment then
            self.TargetForgivenessCooldown = CurTime() + 30
            self.TargetInEnvironment = true
            --Stop attacking
            self.Attacking = false
            local Targeter = self.NPCShip:GetTargeter()
            Targeter:SetFireGroupFiring("Primary", false)
            Targeter:SetFireGroupFiring("Secondary", false)
        elseif CurTime() > self.TargetForgivenessCooldown then
            self.TargetInEnvironment = false
            self:SetTarget(0)
            return true
        end
    else
        if self.TargetInEnvironment then
            self.TargetInEnvironment = false
            --Start attacking again
            self.Attacking = true
            local Targeter = self.NPCShip:GetTargeter()
            Targeter:SetFireGroupFiring("Primary", true)
            Targeter:SetFireGroupFiring("Secondary", true)
        end
    end

    return false
end

---Add a new attacker to our list of attackers
---Ignore players attacking us if we are currently attacking something else
---@param Core entity #The core of the ship attacking us
---@param DamageAmount number #How much damage this core has dealt to us within the last second
function C:AddNewAttacker(Core, DamageAmount)
    SC.Error(string.format("%s %s is attacking %s %s", team.GetName(self.NPCShip:GetFaction()), self.NPCShip:GetCore(), team.GetName(self:GetCoreFaction(Core)), Core), 4)
    self.Attackers[Core] = true
end

--#endregion
--#region Thinking

function C:Think()
    local TargetShip = self.TargetShip
    if IsValid(TargetShip) then
        if TargetShip.IsAlive then
            local TargetPos
            if IsValid(self.TargetGyropod) then
                -- Aim the ship in the direction of where the target is traveling
                -- This should help ships with big spinal-mounted beam weapons
                local TargetGyro = self.TargetGyropod
                TargetPos = TargetGyro:GetPos() + (TargetGyro.Velocity * FrameTime())
            else
                TargetPos = TargetShip:GetPos()
            end

            local TargetMovePos
            if self.OrbitTarget then
                local Theta = CurTime() / 2 -- TODO: This should be based on our movement speed so that we don't orbit faster than the gyropod can turn
                TargetMovePos = TargetPos + Vector(math.sin(Theta) * self.TargetFollowDistance, math.cos(Theta) * self.TargetFollowDistance, 0)
            else
                TargetMovePos = TargetPos
            end

            self.NPCShip:SetTargetMovePos(TargetMovePos)
            self.NPCShip:SetTargetAimPos(TargetPos)

        elseif self.Attacking then
            --Target ship is dead. Run away!
            self:SetTarget(0)
        end
    else
        if self.Attacking then
            self:SetTarget(0)
        end
    end
end
---
function C:SlowThink()
    local Ship = self.NPCShip

    if IsValid(self.TargetShip) then
        --The target goes invalid if the core updates or the target enters a planet's environment.
        -- TODO: Fix this when the hook system is implemented
        if Ship:GetTargeter():GetTarget() ~= self.TargetShip then
            self:SetTarget(0)
            return
        end

        if not self.OrbitTarget then
            Ship:SetThrottleOverride(Vector(0, (math.random() * 2) - 1, (math.random() * 2) - 1))
        end
        self:ManageTargetInEnvironment()
    else
        local TargetFound = self:AttackAttacker()

        if not TargetFound then
            if Ship:GetPrimaryActivity() == "Combat" then
                --Wander around and look for targets if we don't have a target
                Ship:GetActivity("Wander"):SlowThink()
                self:FindValidTarget()

                --Leave if we have no targets to fight after 5 minutes
                if CurTime() > self.NoTargetLeaveTime then
                    Ship:SetActivity("Leave")
                end
            else
                Ship:SetActivity(Ship:GetPrimaryActivity())
            end
        end
    end
end
--#endregion

GM.class.registerClass("NPCShip_Activity_Combat", C)