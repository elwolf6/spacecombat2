local GM = GM

-- Ship class enums
local CLASS_DRONE = 1
local CLASS_FIGHTER = 2
local CLASS_FRIGATE = 3
local CLASS_CRUISER = 4
local CLASS_BATTLECRUISER = 5
local CLASS_BATTLESHIP = 6
local CLASS_DREADNAUGHT = 7
local CLASS_TITAN = 8

---@class NPCShip
---@field ID number #The unique ID of this NPC ship
---@field Core entity #The ship core of this ship
---@field Gyropod entity #The gyropod of this ship
---@field Targeter entity #The targeter module for this ship
---@field Refineries table #The refineries/factories/farms on the ship
---@field Valid boolean #Does the ship have the parts it needs to operate
---@field Entities table #The entities that belong to this NPC ship
---@field Waypoints table #Waypoints that the NPC ship will navigate to in order
---@field Age number #How old this ship is, in seconds
---@field HaveWeapons boolean #A flag indicating that this ship has weapons
---@field Radius number #The radius of the ship, in units
---@field Activities table #A table of activity processors for the ship
---@field CurrentActivity NPCShip_Activity #The current activity of the ship to process
---@field CurrentActivityName string #The name of the current activity
---@field PrimaryActivity string #The activity that the ship will prefer to fall back to if given the chance
---@field StoppingDistance number #How far out the ship has to be from its target before slowing down
---@field ThrottleOverride vector #A vector that is added onto the final throttle handed to the gyropod
---@field Arrived boolean #A flag indicating that we have arrived at a target
---@field NearMapWall boolean #A flag indicating that we are in danger near the map wall
---@field CurrentAtmosphere table #The atmosphere that the ship is currently in
---@field TargetMovePosChanged boolean #A flag indicating that the target move pos changed within the last second
---@field MaintainPosition boolean #The ship will attempt to maintain a position at TargetMovePos
---@field TargetMovePos vector #The world position that the ship is currently navigating to
---@field TargetAimPos vector #The world position that the ship will aim at while navigating
---@field TargetDistance number #How far the NPC ship has to be from the target position before triggering `NPCShip:OnArrivedAtTarget`
---@field TargetAimDot number #What the dot of the heading to `NPCShip.TargetAimPos` should be before moving towards `NPCShip.TargetMovePos`
---@field Faction number #The team ID of the faction that this ship belongs to
---@field ShipClass integer #What class this ship is, numerically
---@field Attackers table #A table of cores that have dealt damage to us within the last second
local C = GM.LCS.class({
    ID = 0,
    Core = nil,
    Gyropod = nil,
    Targeter = nil,
    Refineries = nil,
    Valid = false,
    Entities = nil,
    Waypoints = nil,

    Age = 0,
    HaveWeapons = false,
    Radius = 0,
    RadiusMaxs = nil,
    RadiusMins = nil,

    Activities = nil,
    CurrentActivityName = "None",
    CurrentActivity = nil,

    StoppingDistance = 100,
    ThrottleOverride = nil,
    Arrived = false,
    NearMapWall = false,
    CurrentAtmosphere = nil,
    TargetMovePosChanged = false,
    MaintainPosition = false,

    TargetMovePos = nil,
    TargetAimPos = nil,
    TargetDistance = 0,
    TargetAimDot = 0,

    Faction = 0,
    ShipClass = 0,

    Attackers = nil
})

--#region General Class Stuff
function C:init()
    GM:GetNPCShipManager():RegisterShip(self)
    self.Waypoints = {}
    self.Activities = {}
    self.Refineries = {}

    self.Attackers = {}

    self:AddActivity("Wander")
    self:AddActivity("Leave")
    self:SetPrimaryActivity("Wander")
end

---@return boolean #Is this ship initialized, registered to a manager, and not marked for deletion
function C:IsValid()
    return self.ID ~= 0
end

--#endregion
--#region Member Variable Accessors

---Set the ship's unique ID
---@param number ID #The ship's new unique ID
function C:SetID(NewID)
    self.ID = NewID
end

---Get ths ship's unique ID
---@return number ID #The ship's unique ID
function C:GetID()
    return self.ID
end

---Sets the state of the ship's `HaveWeapons` flag
---@param HaveWeapons boolean #True if we have weapons on the ship
function C:SetHasWeapons(HaveWeapons)
    self.HaveWeapons = HaveWeapons
end

---Returns the state of the ship's `HaveWeapons` flag
---@return boolean HaveWeapons
function C:GetHasWeapons()
    return self.HaveWeapons
end

---Sets the radius of the ship
---@param Radius number
function C:SetRadius(Radius)
    self.Radius = Radius
    self.RadiusMaxs = Vector(Radius, Radius, Radius)
    self.RadiusMins = Vector(-Radius, -Radius, -Radius)
end

---Gets the radius of the ship
---@return number Radius
function C:GetRadius()
    return self.Radius
end

---Get the age of this NPC ship
---@return number Age #The age of the ship, in seconds
function C:GetAge()
    return self.Age
end

---Set the ship's core
---@param Core entity #The core to assign to this ship
function C:SetCore(Core)
    if IsValid(Core) then
        self.Core = Core
        GAMEMODE:GetNPCShipManager():AddShipCoreAssociation(self.ID, Core)
    end
end

---Get the ship's core
---@return entity Core #The ship core of this NPC ship
function C:GetCore()
    return self.Core
end

---Set the class of the ship
---@param ClassEnum integer #The class to set the ship to
function C:SetShipClass(ClassEnum)
    self.ShipClass = ClassEnum
end

---Get the class of the ship
---@return integer ClassEnum
function C:GetShipClass()
    return self.ShipClass
end

---Add a new refinery to the ship
---@param Refinery entity #The refinery to add
function C:AddRefinery(Refinery)
    table.insert(self.Refineries, Refinery)
end

---Get the refineries of the ship
---@return table Refineries #The refineries
function C:GetRefineries()
    return self.Refineries
end

function C:SetTargetDistance(TargetDistance)
    self.TargetDistance = TargetDistance
end

function C:SetStoppingDistance(TargetDistance)
    self.StoppingDistance = TargetDistance
end

---Set the ship's faction
---@param Faction number #The faction to apply to this ship
function C:SetFaction(Faction)
    self.Faction = Faction
end

---Get the ship's faction
---@return number Faction #The faction of this ship
function C:GetFaction()
    return self.Faction
end

---Set the ship's gyropod
---@param Gyropod entity #The gyropod to assign to this ship
function C:SetGyropod(Gyropod)
    if IsValid(Gyropod) then
        self.Gyropod = Gyropod
        Gyropod:TriggerInput("UseWireIO", 1)
        Gyropod:TriggerInput("AimAtTarget", 1)
        Gyropod:TriggerInput("RollLock", 1)
    end
end

---Get the ship's gyropod
---@return entity Gyropod #The gyropod of this NPC ship
function C:GetGyropod()
    return self.Gyropod
end

---Assign a targeter to this ship
---@param Targeter entity #The new targeter
function C:SetTargeter(Targeter)
    if IsValid(Targeter) then
        self.Targeter = Targeter
    end
end

---Get the ship's targeter
---@return entity Targeter #The ship's targeter
function C:GetTargeter()
    return self.Targeter
end

---Sets the table of entities associated with this ship
function C:SetEntities(Ents)
    self.Entities = table.Copy(Ents)
end

---Gets the table of entities associated with this ship
---@return table Entities #The table of entities
function C:GetEntities()
    return self.Entities
end

---Sets the state of the MaintainPosition flag
---@param MaintainPosition boolean
function C:SetMaintainPosition(MaintainPosition)
    self.MaintainPosition = MaintainPosition
end


--#endregion
--#region Ship Management

---Remove this ship and all entities connected to it
function C:Remove()
    if IsValid(self) then
        for _, Ent in pairs(self.Entities or {}) do
            if IsValid(Ent) then
                Ent:Remove()
            end
        end

        GM:GetNPCShipManager():UnregisterShip(self)
        self:SetID(0)   --Just in case the manager doesn't know about us
    end
end

---Add a new activity type for the ship
function C:AddActivity(ActivityName)
    local NewActivity = GAMEMODE.class.getClass("NPCShip_Activity_" .. ActivityName):new(self)
    self.Activities[ActivityName] = NewActivity
end

function C:SetActivity(ActivityName)
    if self.Activities[ActivityName] then
        if self.CurrentActivity then
            self.CurrentActivity:OnActivityFinished(ActivityName)   --Let the current activity know that we are switching activities
        end

        --Switch activities
        local PreviousActivityName = self.CurrentActivityName
        self.CurrentActivityName = ActivityName
        self.CurrentActivity = self.Activities[ActivityName]

        self.CurrentActivity:OnActivityStarted(PreviousActivityName) --Let the new activity know that it has been switched to
    end
end

function C:GetCurrentActivityName()
    return self.CurrentActivityName
end

function C:GetCurrentActivity()
    return self.CurrentActivity
end

function C:GetActivity(ActivityName)
    return self.Activities[ActivityName]
end

---Set our primary activity
function C:SetPrimaryActivity(ActivityName)
    self.PrimaryActivity = ActivityName
    self:SetActivity(ActivityName)
end

---Get the primary activity of the ship
---@return string PrimaryActivity #The activity that the ship wants to primarily perform
function C:GetPrimaryActivity()
    return self.PrimaryActivity
end

--#endregion
--#region Entity Compatibility

---Compatibility with entity:GetProtector()
---@return entity Core #The ship core of this NPC ship
function C:GetProtector()
    return self:GetCore()
end

--#endregion
--#region Waypoint Management

---Add a new waypoint to the table of waypoints
---@param Pos vector #The world position of the waypoint
function C:AddWaypoint(Pos)
    table.insert(self.Waypoints, {
        Position = Pos
    })
end

---Clear out all of our waypoints
function C:ClearWaypoints()
    self.Waypoints = {}
end

---Return the table of waypoints
---@return table Waypoints
function C:GetWaypoints()
    return self.Waypoints
end

---Get the next waypoint to travel to
---@return vector WaypointPos #The position of the waypoint. Nil if no more waypoints
function C:GetNextWaypoint()
    local Waypoint = table.remove(self.Waypoints, 1)
    return Waypoint and Waypoint.Position or nil
end

---Generate some random waypoints around the map
---@param Count number #How many waypoints to generate
function C:AddRandomWaypoints(Count)
    local Range = 14000
    local Space = GAMEMODE:GetSpace()
    for I = 1, Count do
        local WaypointAdded = false
        while not WaypointAdded do
            local Pos = Vector(math.random(-Range, Range), math.random(-Range, Range), math.random(-Range, Range))
            if util.IsInWorld(Pos) and (GAMEMODE:GetAtmosphereAtPoint(Pos) == Space) then
                self:AddWaypoint(Pos)
                WaypointAdded = true
            end
        end
    end
end

---Navigate the ship to its next waypoint if it hasn't begun navigating
function C:ManageWaypoints()
    if (not self.TargetMovePos) and (#self.Waypoints > 0) then
        local Pos = self:GetNextWaypoint()
        if Pos then
            self.TargetMovePos = Pos
            self.TargetAimPos = Pos
            self.TargetAimDot = 1.9
            self.Arrived = false
        end
    end
end

--#endregion
--#region Internal Callbacks

---Called when the ship arrives at the target position
function C:OnArrivedAtTarget()
    self.CurrentActivity:OnArrivedAtTarget()

    local Pos = self:GetNextWaypoint()

    if Pos then
        self.TargetMovePos = Pos
        self.TargetAimPos = Pos
        self.TargetAimDot = 0.3
        self.Arrived = false
    else
        self:OnArrivedAtLastWaypoint()
    end
end

---Called when the ship moves away from the target
function C:OnDepartedFromTarget()
    self.CurrentActivity:OnDepartedFromTarget()
end

---Called when the ship arrives at the final waypoint
function C:OnArrivedAtLastWaypoint()
    self.CurrentActivity:OnArrivedAtLastWaypoint()
end

--#endregion
--#region Attack Management

---Called when our ship core takes damage
---@param Damage table #The table of damage types that are being applied to us
---@param Attacker player #The player that is attacking us
---@param Inflictor object #The entity or launcher object that is applying damage
---@param HitData table #How we were hit
function C:ApplyDamage(Damage, Attacker, Inflictor, HitData)
    if IsValid(Inflictor) and Inflictor.GetCore and IsValid(Inflictor:GetCore()) then
        local InflictorCore = Inflictor:GetCore()
        if InflictorCore ~= self.Core then
            for DamageType, DamageAmount in pairs(Damage) do
                self.Attackers[InflictorCore] = (self.Attackers[InflictorCore] or 0) + DamageAmount
            end
        end
    end
end

---Called every second. Tells our current activity about who has attacked us in the last second
function C:ManageAttackers()
    if table.Count(self.Attackers) > 0 then
        --Re-arrange the attacker data into a sortable form
        local AttackerData = {}
        for AttackingCore, DamageAmount in pairs(self.Attackers) do
            table.insert(AttackerData, {Core = AttackingCore, Damage = DamageAmount})
        end
        --Sort the attacker data by descending damage amount
        table.sort(AttackerData, function(A, B)
            return A.Damage > B.Damage
        end)

        --Tell our current activity which ships have attacked us
        self.CurrentActivity:OnReceivedDamageFromShips(AttackerData)

        --Clear the attackers table
        self.Attackers = {}
    end
end

--#endregion
--#region Ship Supplies

-- TODO: Run the ship's actual life support
---Stock up the ship's energy reserves automagically (cheatily)
function C:ManageResources()
    self.Core:SupplyResource("Energy", 100000)
end

--#endregion
--#region Ship Movement Control

---Set the target position that the ship will strafe towards
---@param TargetPos vector #The world position to strafe towards
function C:SetTargetMovePos(TargetPos)
    self.TargetMovePos = TargetPos
    self.Arrived = false
    self.TargetMovePosChanged = true
end

---Set the target position that the ship will aim at
---@param TargetPos vector #The world position to aim at
function C:SetTargetAimPos(TargetPos)
    local Gyro = self.Gyropod
    if TargetPos then
        Gyro:TriggerInput("TargetPos", TargetPos)
    else
        --Make the ship level out and aim directly forwards if the target position is nil
        local GyroForward = Gyro:GetForward()
        Gyro:TriggerInput("TargetPos", Gyro:GetPos() + Vector(GyroForward.x, GyroForward.y, 0))
    end

    self.TargetAimPos = TargetPos
end

---Set the movement throttle override
---@param ThrottleOverride vector #The override amount to add to the gyropod's throttle input
function C:SetThrottleOverride(ThrottleOverride)
    self.ThrottleOverride = ThrottleOverride
end

---Set the target dot value used by the movement controller
---@param Dot number #A dot value between 0 and 2 that the ship must match before applying throttle
function C:SetTargetAimDot(Dot)
    self.TargetAimDot = Dot
end

---Update the gyropod's inputs in order to navigate to `NPCShip.TargetMovePos`
function C:DoMovement()
    local TargetMovePos = self.TargetMovePos
    if not TargetMovePos then return end    --Stop at this point if we don't have a target position

    --Localize the member vairables most commonly used
    local Gyro = self.Gyropod

    --Head back to the center of the map if we are close to flying out of the map
    if self.NearMapWall then
        local Throttle = Gyro:WorldToLocal(vector_origin):GetNormalized()
        Throttle.y = -Throttle.y
        Gyro:TriggerInput("MoveThrottle", Throttle)
        return
    end

    local Space = GM:GetSpace()
    local Atmos = self.CurrentAtmosphere

    if Atmos ~= Space then
        local Throttle = Gyro:WorldToLocal(Atmos:GetCelestial():getEntity():GetPos())
        Throttle:Normalize()
        Throttle.x = -Throttle.x
        Throttle.z = -Throttle.z
        Gyro:TriggerInput("MoveThrottle", Throttle)
        return
    end

    --Pre-fetch commonly used vectors
    local GyroPos = Gyro:GetPos()
    local TargetAimPos = self.TargetAimPos
    local TargetDistance = self.TargetDistance
    local TargetAimDot = self.TargetAimDot

    local Distance = TargetMovePos:Distance(GyroPos) - self.StoppingDistance

    if Distance > TargetDistance then
        if self.Arrived then
            self.Arrived = false
            self:OnDepartedFromTarget()
        end

        local Throttle

            --Do position maintenance movement
        if self.MaintainPosition then
            Throttle = Gyro:WorldToLocal(TargetMovePos + Gyro.Velocity):GetNormalized()
            Throttle.y = -Throttle.y
        else
            local HeadingDiff = (TargetAimPos - GyroPos)
            HeadingDiff:Normalize()

            if (Gyro:GetForward():Dot(HeadingDiff) + 1) > TargetAimDot then
                if Distance > 4000 then
                    Throttle = Gyro:WorldToLocal(TargetMovePos):GetNormalized()
                else
                    Throttle = Gyro:WorldToLocal(TargetMovePos + Gyro.Velocity):GetNormalized()
                end
                Throttle.y = -Throttle.y
            else
                Throttle = vector_origin
            end
        end

        Gyro:TriggerInput("TargetPos", TargetAimPos)
        Gyro:TriggerInput("MoveThrottle", Throttle + (self.ThrottleOverride or vector_origin))
    else
        if not self.Arrived then
            self.Arrived = true
            self:OnArrivedAtTarget()
        end

        --local Throttle = Gyro:WorldToLocal(Gyro.Velocity / FrameTime()):GetNormalized()
        --Throttle.y = -Throttle.y
        Gyro:TriggerInput("MoveThrottle", self.ThrottleOverride or vector_origin)
    end
end

---Check if we are near the map wall and set a flag if we are
function C:ManageMapWallDistance()
    local Gyro = self.Gyropod
    local Pos = Gyro:GetPos() + (Gyro.Velocity * FrameTime())

    local Maxs = Pos + self.RadiusMaxs
    local Mins = Pos + self.RadiusMins

    self.NearMapWall = not (util.IsInWorld(Maxs) and util.IsInWorld(Mins))
end

---Check if the ship is inside of an environment
---TODO: Roll this into ManageObstacleAvoidance
function C:ManageCurrentAtmosphere()
    local Gyro = self.Gyropod
    local GyroPos = Gyro:GetPos()
    local GyroFuturePos = GyroPos + Gyro.Velocity   -- TODO: Use the ship's sigrad to place the check position out in front a ways

    self.CurrentAtmosphere = GM:GetAtmosphereAtPoint(GyroFuturePos)
end

---If the target movement position is a certain distance away from us, construct a series of waypoints to it.
---The waypoints will be placed going around planets
function C:ManageObstacleAvoidance()
    local StepSize = 1000
    local GyroPos = self.Gyropod:GetPos()
    local TargetPos = self.TargetMovePos
    local Radius = self.Radius

    local PrevPos = GyroPos --The previous position that was tested
    local PrevDistance = PrevPos:Distance(TargetPos)

    local LoopEscape = 100
    while (PrevDistance > StepSize) and (LoopEscape > 0) do
        LoopEscape = LoopEscape - 1

        --Calculate the heading from the previous position to the target position
        local Heading = (TargetPos - PrevPos)
        Heading:Normalize()

        --Get the new step position by stepping towards the target
        local Pos = PrevPos + (Heading * math.min(PrevDistance, StepSize))
        --If that position is in a planet, step away from it
        local Atmos = GAMEMODE:GetAtmosphereAtPoint(Pos)
        if Atmos ~= GAMEMODE:GetSpace() then
            local PlanetPos = Atmos:getEntity():GetPos()
            if PlanetPos then
                local PlanetHeading = (PrevPos - PlanetPos)
                PlanetHeading:Normalize()
                local NewHeading = PlanetHeading:Cross(Heading)
                Pos = PrevPos + (NewHeading * StepSize)
            end
        end

        PrevDistance = Pos:Distance(TargetPos)
        PrevPos = Pos
    end
end


--#endregion
--#region Thinking



---The fast think that happens once per game tick
function C:Think()
    --Poof out of existence if some critical part of us goes away
    if (not IsValid(self.Gyropod)) or (not IsValid(self.Core)) then
        self:Remove()
        return
    end

    if self.Valid then
        --TODO: Allow multiple activities to be active at once
        self.CurrentActivity:Think()    --Let the current activity think
        self:ManageMapWallDistance()    --Make sure we don't fly out of the map
        self:DoMovement()   --Poke at the gyropod
    end
end

---The slower think that happens once per second
function C:SlowThink()
    self.Age = self.Age + 1

    local Gyropod = self.Gyropod
    local Core = self.Core

    if not self.Valid then
        local Valid = IsValid(Gyropod) and IsValid(Core)

        if Valid then
            if not Gyropod.On then
                Gyropod:TriggerInput("Active", 1)
            else
                self.Valid = true
            end
        end

        return
    end

    if not self.CurrentActivity then self:SetActivity(self.PrimaryActivity) end

    if self.TargetMovePosChanged then
        --TODO: Make the ships avoid planets
        --self:ManageObstacleAvoidance()
        self.TargetMovePosChanged = false
    end
    self:ManageResources()  --Add resources to the ship
    self:ManageCurrentAtmosphere()  --Check if the ship is inside of an environment
    self:ManageAttackers()
    self.CurrentActivity:SlowThink()    --Let the current activity think
    self:ManageWaypoints()  --Navigate to the next waypoint
end
--#endregion

GM.class.registerClass("NPCShip", C)