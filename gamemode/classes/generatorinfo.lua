--This class describes a generator and how it works
local GM = GM

local C = GM.LCS.class({
    Name = "Base",
    Class = "base_generatorentity",
    Description = "A thing that generates stuff!",
    Category = "Network",
    MaxMultiplier = 20,
    BaseVolume = 1000,
    HasSounds = true,
    DamagingMultiplier = 10,
    HeatPerCycle = 1,
    MaxHeat = 100,
    Production = {},
    Consumption = {},
    DefaultModel = "models/ce_ls3additional/fusion_generator/fusion_generator_medium.mdl",
    ActivationSound = "spacecombat2/modules/generic_start.wav",
    DeactivationSound = "spacecombat2/modules/generic_stop.wav",
    LoopSound = "spacecombat2/modules/generic_runloop.wav",
    Classes = nil,
    ForceModel = false,
    PGUsage = 0,
    CPUUsage = 0,
    HasMultiplier = true,
    HasCycle = true,
    Slot = "None",
    WireInputs = {},
    WireOutputs = {},
    UpdateOutputs = function(generator) end,
    TriggerInput = function(generator, iname, value) end,
    CustomWireIO = false,
    UseDefaultWireIO = true,
    CanGenerate = function(generator) return (not generator:GetBroken()) and IsValid(generator:GetProtector()) and generator:GetFitting().CanRun and generator:CheckResources() end,
    Setup = function(generator) end,
    CalculateMultiplier = function(generator) end,
    CalculateBaseMultiplier = function(generator) return -1 end,
    TurnedOn = function(generator) end,
    TurnedOff = function(generator) end,
    Toggled = function(generator) end
})

function C:init()

end

function C:SetName(name)
    self.Name = name
end

function C:GetName()
    return self.Name
end

function C:SetForceModel(ForceModel)
    self.ForceModel = ForceModel
end

function C:GetForceModel()
    return self.ForceModel
end

function C:SetClass(class)
    self.Class = class
end

function C:GetClass()
    return self.Class
end

function C:SetDescription(desc)
    self.Description = desc
end

function C:GetDescription()
    return self.Description
end

function C:SetCategory(category)
    self.Category = category
end

function C:GetCategory()
    return self.Category
end

function C:SetHasSounds(HasSounds)
    self.HasSounds = HasSounds
end

function C:GetHasSounds()
    return self.HasSounds
end

function C:GetTriggerInputFunction()
    return self.TriggerInput
end

function C:SetTriggerInputFunction(func)
    self.TriggerInput = func
end

function C:GetUpdateOutputsFunction()
    return self.UpdateOutputs
end

function C:SetUpdateOutputsFunction(func)
    self.UpdateOutputs = func
end

function C:GetCanGenerateFunction()
    return self.CanGenerate
end

function C:SetCanGenerateFunction(func)
    self.CanGenerate = func
end

function C:GetSetupFunction()
    return self.Setup
end

function C:SetSetupFunction(func)
    self.Setup = func
end

function C:GetToggledFunction()
    return self.Toggled
end

function C:SetToggledFunction(func)
    self.Toggled = func
end

function C:GetTurnedOffFunction()
    return self.TurnedOff
end

function C:SetTurnedOffFunction(func)
    self.TurnedOff = func
end

function C:GetTurnedOnFunction()
    return self.TurnedOn
end

function C:SetTurnedOnFunction(func)
    self.TurnedOn = func
end

function C:SetBaseVolume(vol)
    self.BaseVolume = vol
end

function C:GetBaseVolume()
    return self.BaseVolume
end

function C:GetCalculateMultiplierFunction()
    return self.CalculateMultiplier
end

function C:SetCalculateMultiplierFunction(func)
    self.CalculateMultiplier = func
end

function C:GetCalculateBaseMultiplierFunction()
    return self.CalculateBaseMultiplier
end

function C:SetCalculateBaseMultiplierFunction(func)
    self.CalculateBaseMultiplier = func
end

function C:SetHasMultiplier(b)
    self.HasMultiplier = b
end

function C:GetHasMultiplier()
    return self.HasMultiplier
end

function C:SetHasCycle(b)
    self.HasCycle = b
end

function C:GetHasCycle()
    return self.HasCycle
end

function C:SetMaxMultiplier(mult)
    self.MaxMultiplier = mult
end

function C:GetMaxMultiplier()
    return self.MaxMultiplier
end

function C:SetDamagingMultiplier(mult)
    self.DamagingMultiplier = mult
end

function C:GetDamagingMultiplier()
    return self.DamagingMultiplier
end

function C:SetHeatPerCycle(Heat)
    self.HeatPerCycle = Heat
end

function C:GetHeatPerCycle()
    return self.HeatPerCycle
end

function C:SetMaxHeat(max)
    self.MaxHeat = max
end

function C:GetMaxHeat()
    return self.MaxHeat
end

function C:AddProduction(res, mul, autoshutdown, onfull, onempty, atmosphere)
    self.Production[res] = {
		mul = mul,
		autoshutdown = autoshutdown == nil and true or autoshutdown,
		onfull = onfull,
		onempty = onempty,
		atmosphere = atmosphere
	}
end

function C:GetProduction()
    return self.Production
end

function C:AddConsumption(res, mul, autoshutdown, onfull, onempty, atmosphere)
    self.Consumption[res] = {
		mul = mul,
		autoshutdown = autoshutdown or false,
		onfull = onfull,
		onempty = onempty,
		atmosphere = atmosphere
	}
end

function C:GetConsumption()
    return self.Consumption
end

function C:SetDefaultModel(model)
    self.DefaultModel = model
end

function C:GetDefaultModel()
    return self.DefaultModel
end

function C:SetActivationSound(sound)
    self.ActivationSound = sound
end

function C:GetActivationSound()
    return self.ActivationSound
end

function C:SetDeactivationSound(sound)
    self.DeactivationSound = sound
end

function C:GetDeactivationSound()
    return self.DeactivationSound
end

function C:SetLoopSound(sound)
    self.LoopSound = sound
end

function C:GetLoopSound()
    return self.LoopSound
end

function C:SetFitting(cpu, pg, slot, classes)
    self.CPUUsage = cpu or 0
    self.PGUsage = pg or 0
    self.Slot = slot or "None"
    self.Classes = classes
end

function C:GetFitting()
    return self.CPUUsage, self.PGUsage, self.Slot, self.Classes
end

function C:HasCustomWireIO()
  return self.CustomWireIO
end

function C:ShouldUseDefaultWireIO()
    return self.UseDefaultWireIO
end

function C:SetWireIO(inputs, outputs, usedefault)
    self.WireInputs = inputs or {}
    self.WireOutputs = outputs or {}
    self.CustomWireIO = true

    if usedefault ~= nil then
        self.UseDefaultWireIO = usedefault
    end
end

function C:GetWireIO()
    return self.WireInputs, self.WireOutputs
end

GM.class.registerClass("GeneratorInfo", C)

--Some gamemode functions to make working with this class easier
local Generators = {}

function GM:RegisterGenerator(Generator)
  Generators[Generator:GetName()] = Generator
end

function GM:NewGeneratorInfo()
  return self.class.getClass("GeneratorInfo"):new()
end

function GM:GetGenerators()
  return Generators
end

function GM:GetGeneratorInfo(name)
  return Generators[name]
end