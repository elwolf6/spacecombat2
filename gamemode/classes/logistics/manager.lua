local GM = GM

include("user_interface.lua")   -- Load up the logistics UI's back-end and front-end

if CLIENT then return end   -- Client doesn't need to go beyond this point
-- Reload the global logistics manager if it exists
hook.Remove("OnReloaded", "SC.LogisticsManager.OnReloaded")
hook.Add("OnReloaded", "SC.LogisticsManager.OnReloaded", function()
    local CurrentManager = GAMEMODE.CurrentLogisticsManager

    if IsValid(CurrentManager) then
        local OldNodes = CurrentManager:GetNodes()
        local OldCouriers = CurrentManager:GetCouriers()

        GAMEMODE.CurrentLogisticsManager = nil

        local NewManager = GM:GetLogisticsManager()

        for _,Node in pairs(OldNodes) do
            NewManager:RegisterNode(Node)
        end

        for _, Courier in pairs(OldCouriers) do
            NewManager:RegisterCourier(Courier)
        end
    end
end)

-- Returns the gamemode's global logistics manager
-- The manager is created if it doesn't exist yet
---@return LogisticsManager
function GM:GetLogisticsManager()
    if not IsValid(GAMEMODE.CurrentLogisticsManager) then
        GAMEMODE.CurrentLogisticsManager = GAMEMODE.class.getClass("LogisticsManager"):new()
    end

    return GAMEMODE.CurrentLogisticsManager
end



--#region Class Definition

-- Define and create the class
---@class LogisticsManager
---@field Nodes LogisticsNode[] #A table of logistics nodes managed by the manager
---@field Couriers LogisticsCourier[] #A table of logistics couriers managed by the manager
---@field CourierVisitations table #A table of nodes visited by couriers. Used for evenly distributing resources.
---@field NextNodeID integer #The next ID to assign to a node
---@field NextCourierID integer #The next ID to assign to a courier
---@field NextThinkTime integer #The next time, in seconds, to make the manager process all nodes/couriers
local C = GM.LCS.class({
    Nodes = nil,
    Couriers = nil,

    CourierVisitations = nil,
    NextNodeID = 1,
    NextCourierID = 1,
    NextThinkTime = 0
})

function C:init()
    self.Nodes = {}
    self.Couriers = {}
    self.CourierVisitations = {}
end

function C:IsValid()
    return true
end


--#region Node Management
function C:GetNextNodeID()
    local I = 1
    while true do
        if not self.Nodes[I] then return I end
        I = I + 1
    end
end

function C:RegisterNode(Node)
    Node:SetID(self.NextNodeID)
    self.Nodes[self.NextNodeID] = Node
    self.NextNodeID = self:GetNextNodeID()
end

function C:UnregisterNode(Node)
    self.NextNodeID = Node:GetID()
    Node:SetID(0)
    self.Nodes[self.NextNodeID] = nil

    -- Remove any entries using this node's ID from the courier visitation table
    for CourierID, Visitation in pairs(self.CourierVisitations) do
        for PriorityLevel, Nodes in pairs(Visitation) do
            Nodes[self.NextNodeID] = nil
        end
    end
end

function C:GetNodes()
    return self.Nodes
end

--Returns a list of nodes that are owned by a player
--TODO: Add nodes that belong to the player's faction, when we get around to that
function C:GetNodesForPlayer(Ply)
    local Nodes = {}
    for NodeID, Node in pairs(self.Nodes) do
        if Node:GetOwner() == Ply then
            Nodes[NodeID] = Node
        end
    end

    return Nodes
end

--Returns a list of sets of nodes that can interconnect with eachother
--TODO: Needs to be extended to support player-to-player connections and connections between different node types
function C:GetNodeGroups()
    local NodeGroups = {}

    for NodeID, Node in pairs(self.Nodes) do
        if IsValid(Node) then   --Don't process tasks for nodes that have embraced the void
            local NodeOwner = Node:GetOwner()
            local GroupKey = tostring(NodeOwner:EntIndex()) .. Node:GetType()
            if not NodeGroups[GroupKey] then
                NodeGroups[GroupKey] = {}
            end

            table.insert(NodeGroups[GroupKey], Node)
        end
    end

    return NodeGroups
end

--#endregion
--#region Courier Management

---Get the next ID that can be assigned to a new courier
---@return integer CourierID
function C:GetNextCourierID()
    -- Don't find the next ID if we already have one cached
    if self.NextCourierID ~= 0 then
        local ID = self.NextCourierID   -- Get the ID
        self.NextCourierID = 0    -- Clear the NextCourierID field
        return ID   -- Return the ID
    end

    -- Find a free ID
    for I = 1, 10000 do -- I think we'd have a problem if we had 10000 couriers...
        if not IsValid(self.Couriers[I]) then
            return I
        end
    end

    return 0
end

---Register a new courier to the manager
---@param Courier LogisticsCourier #The courier to register
function C:RegisterCourier(Courier)
    local ID = self:GetNextCourierID()
    if ID == 0 then return end   -- Bad things happened and we can't get an ID for this courier
    Courier:SetID(ID)
    self.Couriers[ID] = Courier
    self.CourierVisitations[ID] = {}
end

---Unregister a courier and mark it for removal
---@param Courier LogisticsCourier #The courier to unregister
function C:UnregisterCourier(Courier)
    local ID = Courier:GetID()
    SC.Error(string.format("Removing courier %d", ID), 3)
    Courier:SetID(0)    -- Make the courier invalid
    self.Couriers[ID] = nil -- Remove our reference to the courier
    self.CourierVisitations[ID] = nil   -- Remove the visitation table

    self.NextCourierID = ID -- Use this ID for the next courier
end

function C:GetCouriers()
    return self.Couriers
end

--#endregion

--[[
=======================
    Logistics Logic
=======================
]]

--Return with a table of resources that the suppliers in this group can supply
--This should probably get added to a monolithic processing loop
function C:GetSuppliableResourceList(Group)
    local SuppliableResources = {}
    for NodeID, Node in pairs(Group) do
        local Resources = Node:GetAvailableResources()
        for ResName, _ in pairs(Resources) do
            SuppliableResources[ResName] = SuppliableResources[ResName] or {}
            table.insert(SuppliableResources[ResName], Node)
        end
    end

    return SuppliableResources
end

---Look in the group of nodes for the best node that can supply a given resource
---@param Courier LogisticsCourier #The courier that will fulfill this task
---@param Group table #A sequential list of nodes to look through
---@param ResourceName string #The name of the resource
---@param ResourceQuantity integer #The quantity of resource
---@param IgnoredNode LogisticsNode #A node to ignore while searching
---@return LogisticsNode BestSupplier #The best supplier node
function C:FindBestSupplier(Courier, Group, ResourceName, ResourceQuantity, IgnoredNode)
    local CourierPos = Courier:GetPos()
    local BestSupplier = nil
    local BestSupplierPriority = -1
    local BestSupplierDistance = 9999999
    local IgnoredNodeID = IsValid(IgnoredNode) and IgnoredNode:GetID() or 0

    for _, Node in ipairs(Group) do
        if (Node:GetID() ~= IgnoredNodeID) and (Node:GetAvailableResource(ResourceName) > 0) then
            local Supply = Node:GetSupplyResources()[ResourceName]
            local Distance = CourierPos:Distance(Node:GetPos())
            if Supply.Priority > BestSupplierPriority then
                BestSupplier = Node
                BestSupplierPriority = Supply.Priority
                BestSupplierDistance = Distance
            elseif Distance < BestSupplierDistance then
                BestSupplier = Node
                BestSupplierDistance = Distance
            end
        end
    end

    return BestSupplier
end

function C:DispatchTasksToCourier(Courier, Nodes)
    local SuppliableResources = self:GetSuppliableResourceList(Nodes)

    local PriorityDemandTable = {}  -- A table of demands levels, and each node with its demands in it
    local HighestPriority = 0   -- The highest priority demand that we encountered while building the PriorityDemandTable table
    local CourierID = Courier:GetID()
    local CourierVisitation = self.CourierVisitations[CourierID]

    -- Construct the PriorityDemandTable
    for _, Node in pairs(Nodes) do -- Go through every single node in the group
        for ResName,Demand in pairs(Node:GetRequiredResources()) do -- Go through the node's demands
            -- Can we actually supply this demand?
            if SuppliableResources[ResName] then
                -- Pull some data from the demand
                local DemandPriority = Demand.Priority  -- The demand's priority

                local FulfillDemand = true     -- A flag indicating that we need to fulfill this demand

                FulfillDemand = FulfillDemand and (Node:GetRequiredResource(ResName) > 0)

                -- Does this demand need to get fulfilled?
                if FulfillDemand then
                    local NodeID = Node:GetID()
                    -- Update the highest priority
                    if DemandPriority > HighestPriority then HighestPriority = DemandPriority end
                    -- Create some table entries if they do not exist yet
                    if not PriorityDemandTable[DemandPriority] then PriorityDemandTable[DemandPriority] = {} end
                    if not PriorityDemandTable[DemandPriority][NodeID] then PriorityDemandTable[DemandPriority][NodeID] = {} end
                    PriorityDemandTable[DemandPriority][NodeID][ResName] = true

                    -- Add entries to the courier visitation table if it has none yet
                    if not CourierVisitation[DemandPriority] then CourierVisitation[DemandPriority] = {} end
                    if not CourierVisitation[DemandPriority][NodeID] then CourierVisitation[DemandPriority][NodeID] = {} end
                end
            end
        end
    end

    -- We now have a list of nodes that have unmet demands, sorted by priority.
    -- The higher the priority number, the higher the priority of the demand.

    -- Now work throug the list of demands and try to find one that we can fulfill
    for I = HighestPriority,0,-1 do
        local DemandTable = PriorityDemandTable[I]
        -- Are there demands at this level? Don't want to iterate a nil table entry...
        if DemandTable then
            local CourierNodeVisitation = CourierVisitation[I]

            local DoResetCourierVisitation = true

            for NodeID, Resources in pairs(DemandTable) do
                local CourierResourceVisitation = CourierNodeVisitation[NodeID]
                for ResName, _ in pairs(Resources) do
                    if not CourierResourceVisitation[ResName] then
                        DoResetCourierVisitation = false
                        break
                    end
                end

                if not DoResetCourierVisitation then break end
            end

            if DoResetCourierVisitation then
                for NodeID, Resources in pairs(CourierNodeVisitation) do
                    for ResName, _ in pairs(Resources) do
                        Resources[ResName] = false
                    end
                end
            end

            for NodeID, ResourceDemands in pairs(DemandTable) do
                local CourierResourceVisitation = CourierNodeVisitation[NodeID]
                for ResName, _ in pairs(ResourceDemands) do
                    if not CourierResourceVisitation[ResName] then
                        Visitation = true
                        local DemandNode = self.Nodes[NodeID]
                        local QuantityRequired = DemandNode:GetRequiredResource(ResName)

                        -- Find the best supplier for this resource while ignoring the demand node
                        local BestSupplier = self:FindBestSupplier(Courier, SuppliableResources[ResName], ResName, QuantityRequired, DemandNode)

                        local QuantityFromSupply = IsValid(BestSupplier) and BestSupplier:GetAvailableResource(ResName) or 0
                        local QuantityCarryable = Courier:GetMaxCarryableAmount(ResName)

                        local QuantityTransferrable = math.min(QuantityFromSupply, QuantityCarryable, QuantityRequired)

                        if QuantityTransferrable > 0 then
                            CourierResourceVisitation[ResName] = true

                            BestSupplier:AddOutbound(ResName, QuantityTransferrable)
                            DemandNode:AddInbound(ResName, QuantityTransferrable)

                            Courier:AddTask(BestSupplier, DemandNode, ResName, QuantityTransferrable)

                            if not Courier:GetIsAvailable() then return end  --Courier can't accept any more tasks, we're done.
                        end
                    end
                end
            end
        end
    end
end

---Process the table of couriers and dispatch tasks to them
function C:DispatchLogisticsTasks()
    for CourierID, Courier in pairs(self.Couriers) do
        -- TODO: The #Courier:GetTasks() is a quick fix to stop one courier from monopolizing all tasks.
        --       The task dispatcher logic needs to be improved to better balance demanding nodes and their demands.
        --       For example, round-robin between nodes, and then the node demanding resources

        if IsValid(Courier) then
            if Courier:GetIsAvailable() then
                local AvailableNodes = Courier:GetAvailableNodes()
                if #AvailableNodes > 0 then
                    self:DispatchTasksToCourier(Courier, AvailableNodes)
                end
            end

            --print("Energy", Courier:GetParent():GetTargetingComponent():GetEnergy())
        else
            -- Yeet the invalid courier out of existence
            SC.Error(string.format("Removing invalid courier!, ID:%d", CourierID), 5)
            self:UnregisterCourier(Courier)
        end
    end
end

---Called once per tick
function C:Think()
    -- The manager only runs once per second
    if CurTime() > self.NextThinkTime then
        self:DispatchLogisticsTasks()   -- Dispatch tasks to all node groups
        self.NextThinkTime = CurTime() + 1
    end
end

--#region Hooks

-- Set up the think hook
hook.Remove("Think", "SC.LogisticsManager.Think")
hook.Add("Think", "SC.LogisticsManager.Think", function()
    local Manager = GM:GetLogisticsManager()
    if IsValid(Manager) then
        Manager:Think()
    end
end)
--#endregion

GM.class.registerClass("LogisticsManager", C)

--#endregion