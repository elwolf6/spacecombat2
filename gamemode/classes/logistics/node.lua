local GM = GM

---@class LogisticsNode
---@field ID integer #The unique ID of this node in the global logistics node table
---@field Type string #What type this node is. Used when determining which nodes this node can connect to.
---@field Label string #The user-assigned label on this node
---@field Storage table #The resource container/ship core/resource node that this logistics node is attached to
---@field Owner entity #Who owns this node -- TODO: Allow NPC ships to own logistics nodes
---@field Supply table #The table of supply groups for this node
---@field Demand table #The table of demand groups for this node
---@field Inbound table #The table of resources that are in the process of being delivered to this node
---@field Outbound table #The table of resources that are in the process of being taken from this node
---@field Available boolean #Is this node available to be serviced
local C = GM.LCS.class({
    ID = 0,
    Type = "",          --What type this node is
    Label = "Unnamed",         --The user-assigned label on this node

    Storage = nil,

    Owner = nil,       --Who owns this node

    Supply = nil,        --A list of resources supplied by this node
    Demand = nil,        --A list of resources demanded by this node
    Inbound = nil,       --A list of resources that are inbound to this node
    Outbound = nil,      --A list of resources that are outbound from this node

    Available = true,
})

---Class initialization
---@param Type string #What "type" this node is. Used for determining which nodes this node can connect to
function C:init(Type)
    -- Create tables
    self.Supply = {}
    self.Demand = {}
    self.Inbound = {}
    self.Outbound = {}
    -- Set initial variables
    self.Type = Type

    -- Register the node
    GAMEMODE:GetLogisticsManager():RegisterNode(self)
end

--#region Member Variable Access

---Set the unique ID of this node
---@param NewID integer
function C:SetID(NewID)
    self.ID = NewID
end

---Get the unique ID of this node
---@return integer
function C:GetID()
    return self.ID
end

---Set the owner of this node to an entity
---@param NewOwner entity #Probably a player
function C:SetOwner(NewOwner)
    self.Owner = NewOwner
end

---Get the owner of this node
---@return entity
function C:GetOwner()
    return self.Owner
end

---Get the type of this node
---@return string NodeType
function C:GetType()
    return self.Type
end

---Set the label of the node
---@param NewLabel string
function C:SetLabel(NewLabel)
    self.Label = NewLabel or "Unnamed"
end

---Get the label of the node
---@return string Label
function C:GetLabel()
    return self.Label
end

function C:SetIsAvailable(Available)
    self.Available = Available
end

function C:GetIsAvailable()
    return self.Available
end

---Set the parent object of this node
---The parent is probably a launcher
---@param Parent table
function C:SetParent(Parent)
    self.Parent = Parent
end

---Get the parent object of this node
---@return table Parent
function C:GetParent()
    return self.Parent
end

---Set the resource storage that this node is linked to
---@param Storage table #A storage object to link to this node
function C:SetStorage(Storage)
    self.Storage = Storage
end

---Get the resource storage that this node is linked to
---@return table Storage
function C:GetStorage()
    return self.Storage
end



--#endregion
--#region Generic Member Table Acces

---Add/Update an entry to the Supply or Demand tables
---@param Tbl table #The Supply or Demand table to edit
---@param EntryName string #The name of the supply/demand group to edit
---@param ResourceName string #The name of the resource in the group to edit
---@param ResourceQuantity integer #The quantity of the resource
---@param ResourcePriority integer #The priority of the resource
local function AddTableEntry(Tbl, EntryName, ResourceName, ResourceQuantity, ResourcePriority)
    if EntryName and ResourceName then
        if not Tbl[EntryName] then Tbl[EntryName] = {} end
        local Entry = Tbl[EntryName]
        Entry[ResourceName] = {
            Quantity = ResourceQuantity or 0,
            Priority = ResourcePriority or 5
        }
    end
end

---Add a list of entries to the Supply or Demand table
---@param Tbl table #The Supply or Demand table to write the entries into
---@param List table #The list of entries to add to the table
local function AddTableEntries(Tbl, List)
    for _, NewEntry in pairs(List) do
        AddTableEntry(Tbl, NewEntry.EntryName, NewEntry.ResourceName, NewEntry.Quantity, NewEntry.Priority)
    end
end

---Remove an entry from a resource table, or a specific resource from a resource table's entry
---@param Tbl table #The Supply or Demand table
---@param EntryName string #The name of the Supply/Demand group to remove the entry from
---@param ResourceName string #The name of the resource to delete, or delete the entire group if nil
local function RemoveTableEntry(Tbl, EntryName, ResourceName)
    if ResourceName and Tbl[EntryName] then
        Tbl[EntryName][ResourceName] = nil
    else
        Tbl[EntryName] = nil
    end
end

---Formats a resource table into a flat table indexed by resource name
---@param Tbl table #The Supply or Demand table
---@return table FormattedTable
local function FormatTableToResources(Tbl)
    local NewTbl = {}
    for EntryName, Entry in pairs(Tbl) do
        for ResourceName, ResourceEntry in pairs(Entry) do
            if not NewTbl[ResourceName] then
                NewTbl[ResourceName] = {
                    Quantity = ResourceEntry.Quantity,
                    Priority = ResourceEntry.Priority
                }
            else
                local NewEntry = NewTbl[ResourceName]
                if ResourceEntry.Quantity > NewEntry.Quantity then NewEntry.Quantity = ResourceEntry.Quantity end
                if ResourceEntry.Priority > NewEntry.Priority then NewEntry.Priority = ResourceEntry.Priority end
            end
        end
    end

    return NewTbl
end

--#endregion
--#region Demand/Supply Addition/Subtraction

--#region Demand
---Add a demand to the Demand table
---@param DemandName string #The name of the demand group to add the demand to
---@param ResourceName string #The name of the resource to add a demand for
---@param ResourceQuantity integer #The quantity of resource to demand
---@param ResourcePriority integer #The priority of the demand
function C:AddDemand(DemandName, ResourceName, ResourceQuantity, ResourcePriority)
    AddTableEntry(self.Demand, DemandName, ResourceName, ResourceQuantity, ResourcePriority)
end

---Add a list of demands to the demand table
---@param DemandList table
function C:AddDemands(DemandList)
    AddTableEntries(self.Demand, DemandList)
end

---Remove a demand group by name, or an individual resource demand from a demand group
---@param DemandName string #The name of the demand group to remove the demand from, or to remove entirely
---@param ResourceName string #The name of the resource to remove the demand for, or the entire group if nil
function C:RemoveDemand(DemandName, ResourceName)
     --Delete the demand, or the entire demand group
    RemoveTableEntry(self.Demand, DemandName, ResourceName)
end
--#endregion
--#region Supply

---Add a supply to the Supply table
---@param SupplyName string #The name of the supply group to add the supply to
---@param ResourceName string #The name of the resource to add a supply for
---@param ResourceQuantity integer #The quantity of resource to supply
---@param ResourcePriority integer #The priority of the supply
function C:AddSupply(SupplyName, ResourceName, ResourceQuantity, ResourcePriority)
    AddTableEntry(self.Supply, SupplyName, ResourceName, ResourceQuantity, ResourcePriority)
end

---Add a list of supplies to the Supply table
---@param SupplyList table
function C:AddSupplys(SupplyList)
    AddTableEntries(self.Supply, SupplyList)
end

---Remove a supply group by name, or an individual resource supply from a supply group
---@param SupplyName string #The name of the supply group to remove the supply from, or to remove entirely
---@param ResourceName string #The name of the resource to remove the supply for, or the entire group if nil
function C:RemoveSupply(SupplyName, ResourceName)
    --Delete the supply, or the entire supply group
    RemoveTableEntry(self.Supply, SupplyName, ResourceName)
end

--#endregion
--#region Resource Table Information Access

--#region Raw Table Access

---Get the entire Demand table
---@return table DemandTable
function C:GetDemand()
    return self.Demand
end

---Get the entire Supply table
---@return table SupplyTable
function C:GetSupply()
    return self.Supply
end

--#endregion
--#region Formatted Table Access

---Get all resources demanded by this node
---@return table DemandedResources
function C:GetDemandResources()
    return FormatTableToResources(self.Demand)
end

---Get all resources supplied by this node
---@return table SuppliedResources
function C:GetSupplyResources()
    return FormatTableToResources(self.Supply)
end

---Get a table of resources, and their quantities, required to fulfill this node's demands
---@return table RequiredResources
function C:GetRequiredResources()
    if not self.Storage then return {} end

    local RequiredResources = {}
    local DemandResources = self:GetDemandResources()
    for ResName, DemandEntry in pairs(DemandResources) do
        local Quantity = DemandEntry.Quantity
        if Quantity == 0 then Quantity = self.Storage:GetMaxAmount(ResName) end     --If the quantity requested is 0, treat it as "fill to the max"
        local Needed = Quantity - (self:GetAmount(ResName) + self:GetInboundAmount(ResName))
        if Needed > 0 then
            RequiredResources[ResName] = {
                Quantity = math.floor(Needed),
                Priority = DemandEntry.Priority
            }
        end
    end
    return RequiredResources
end

---Get a table of resources, and their quantities, that are available for this node to supply
---@return table AvailableResources
function C:GetAvailableResources()
    local AvailableResources = {}
    local SupplyResources = self:GetSupplyResources()
    for ResName, SupplyEntry in pairs(SupplyResources) do
        local Available = (self:GetAmount(ResName) - SupplyEntry.Quantity) - self:GetOutboundAmount(ResNAme)
        if Available > 0 then
            AvailableResources[ResName] = {
                Quantity = math.floor(Available),
                Priority = SupplyEntry.Priority
            }
        end
    end
    return AvailableResources
end

---Get how much of a resource is required to fulfill this node's demand for it
---@param ResourceName string #The name of the resource to get the quantity for
---@return integer Quantity
function C:GetRequiredResource(ResourceName)
    local DemandEntry = self:GetDemandResources()[ResourceName]
    if not DemandEntry then return 0 end
    local Quantity = DemandEntry.Quantity
    if Quantity == 0 then Quantity = self.Storage:GetMaxAmount(ResourceName) end     --If the quantity requested is 0, treat it as "fill to the max"
    local Needed = Quantity - (self:GetAmount(ResourceName) + self:GetInboundAmount(ResourceName))
    return math.max(math.floor(Needed), 0)
end

---Get how much of a resource is available for this node to supply
---@param ResourceName string #The name of the resoure to get the quantity for
---@return integer Quantity
function C:GetAvailableResource(ResourceName)
    local SupplyEntry = self:GetSupplyResources()[ResourceName]
    if not SupplyEntry then return 0 end
    local Available = (self:GetAmount(ResourceName) - SupplyEntry.Quantity) - self:GetOutboundAmount(ResourceName)
    return math.max(math.floor(Available), 0)
end

--#endregion
--#endregion
--#region Inbound/Outbound Resource Management

---Add onto the amount of resources outbound from this node
---@param ResourceName string
---@param ResourceQuantity integer
function C:AddOutbound(ResourceName, ResourceQuantity)
    self.Outbound[ResourceName] = (self.Outbound[ResourceName] or 0) + ResourceQuantity
end

---Add onto the amount of resources inbound to this node
---@param ResourceName string
---@param ResourceQuantity integer
function C:AddInbound(ResourceName, ResourceQuantity)
    self.Inbound[ResourceName] = (self.Inbound[ResourceName] or 0) + ResourceQuantity
end

---Returns how much of a resource is inbound to this node
---A courier is carrying this resource to this node
---@param ResourceName string
---@return integer Quantity
function C:GetInboundAmount(ResourceName)
    return self.Inbound[ResourceName] or 0
end

---Returns how much of a resource is outbound from this node
---A courier is on its way to collect this much resource
---@param ResourceName string
---@return integer Quantity
function C:GetOutboundAmount(ResourceName)
    return self.Outbound[ResourceName] or 0
end

---Subtract from the amount of a resource inbound to this node
---@param ResourceName string
---@param ResourceAmount integer
function C:SubtractInbound(ResourceName, ResourceAmount)
    self.Inbound[ResourceName] = math.max((self.Inbound[ResourceName] or 0) - ResourceAmount, 0)
end

---Subtract from the amount of a resource outbound from this node
---@param ResourceName string
---@param ResourceAmount integer
function C:SubtractOutbound(ResourceName, ResourceAmount)
    self.Outbound[ResourceName] = math.max((self.Outbound[ResourceName] or 0) - ResourceAmount, 0)
end

--#endregion
--#region ResourceTransfer

-- TODO: Move these to the Courier class
---Functions used to abstract the interface between the outside world and our storage

---Used by a courier when it arrives at a demanding node to supply its requested resource
---@param ResourceName string #The name of the resource to transfer to storage
---@param ResourceAmount integer #The quantity of resource to transfer
---@return boolean SupplySuccessful #Was the storage able to fit the requested quantity of resource
function C:CourierSupplyResource(ResourceName, ResourceAmount)
    if not self.Storage then return false end

    self:SubtractInbound(ResourceName, ResourceAmount)
    return self.Storage:SupplyResource(ResourceName, ResourceAmount)
end

---Used by a courier when it arrives at a supplying node to consume its requested resource
---@param ResourceName string #The name of the resource to transfer to the courier
---@param ResourceAmount integer #The quantity of resource to transfer
---@return boolean ConsumeSuccessful #Was the storage able to supply the request quantity of resource
function C:CourierConsumeResource(ResourceName, ResourceAmount)
    if not self.Storage then return false end

    self:SubtractOutbound(ResourceName, ResourceAmount)
    return self.Storage:ConsumeResource(ResourceName, ResourceAmount)
end

---Supply an amount of a resource to the node's storage
---@param ResourceName string
---@param ResourceAmount integer
---@param Force boolean
---@return boolean SupplySuccessful
function C:SupplyResource(ResourceName, ResourceAmount, Force)
    if not self.Storage then return false end

    return self.Storage:SupplyResource(ResourceName, ResourceAmount, Force)
end

---Consume an amount of a resource from the node's storage
---@param ResourceName string
---@param ResourceAmount integer
---@param Force boolean
---@return boolean ConsumptionSuccessful
function C:ConsumeResource(ResourceName, ResourceAmount, Force)
    if not self.Storage then return false end

    return self.Storage:ConsumeResource(ResourceName, ResourceAmount, Force)
end

---Get how much of a resource is in our storage
---@param ResourceName string
---@return integer Quantity
function C:GetAmount(ResourceName)
    if not self.Storage then return 0 end

    return self.Storage:GetAmount(ResourceName)
end

---Get the maximum amount of a resource that can fit into our storage
---@param ResourceName string
---@return integer Quantity
function C:GetMaxAmount(ResourceName)
    if not self.Storage then return 0 end

    return self.Storage:GetMaxAmount(ResourceName)
end

--#endregion
--#region Entity Methods
---Methods used to emulate entity functionality. -- TODO: These should probably just be reclassified as "generic class methods"

---Get the position of the parent object
---@return vector Position
function C:GetPos()
    return self.Parent:GetPos()
end

---Get the ship core/player/whatever that our parent is protected by
---@return entity Protector
function C:GetProtector()
    return self.Parent:GetProtector()
end

---Is the node valid, and not marked for removal
---@return boolean IsValid
function C:IsValid()
    return (self.ID ~= 0) and IsValid(self.Owner)
end

--#endregion
--#region Node Table Member Management
---Methods used by the courier/logistics manager to determine things about this node.


---Returns true if a player is allowed to modify this node
---Right now just checks if the player is the owner
---@param Ply entity #The player to check
---@return boolean CanModify
function C:IsPlayerAllowedToModify(Ply)
    return self:GetOwner() == Ply
end

--#endregion
--#region Serialization/Deserialization
---Used when saving this node's data for duping/blueprinting

---Serialize the node's fields into a table to be saved
---@return table Data
function C:Serialize()
    local Data = {
        Demand = self:GetDemand(),
        Supply = self:GetSupply(),
        Label = self:GetLabel()
    }

    return Data
end

---Deserialize data and store it into the node's fields
---@param Data table
function C:DeSerialize(Data)
    self:SetLabel(Data.Label)

    if Data.Demand then
        self.Demand = {}
        for GroupName, Group in pairs(Data.Demand) do
            for ResName, Entry in pairs(Group) do
                self:AddDemand(GroupName, ResName, Entry.Quantity, Entry.Priority)
            end
        end

        self.Supply = {}
        for GroupName, Group in pairs(Data.Supply) do
            for ResName, Entry in pairs(Group) do
                self:AddSupply(GroupName, ResName, Entry.Quantity, Entry.Priority)
            end
        end
    end
end
--#endregion

GM.class.registerClass("LogisticsNode", C)
