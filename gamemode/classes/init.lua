AddCSLuaFile()

local SharedIncludeTbl = {
	"class.lua",
    "Celestial.lua",
    "Resource.lua",
    "ResourceContainer.lua",
    "MultiTypeResourceContainer.lua",
	"Environment.lua",
	"Space.lua",
	"HudComponent.lua",
	"HudPanel.lua",
	"HudBarIndicator.lua",
	"TextElement.lua",
	"HudRadialIndicator.lua",
	"PlayerSuit.lua",
    "GeneratorInfo.lua",

    -- Logistics
    "logistics/manager.lua",

    -- Launchers
    "launchers/Launcher.lua",
    "launchers/Launcher_Beam.lua",
    "launchers/launcher_droneport.lua",
    "launchers/LauncherModel.lua",
    "launchers/LauncherType.lua",
    "launchers/LauncherUpgrade.lua",

	-- Projectiles
	"Projectiles/Projectile.lua",
    "Projectiles/ProjectileComponent.lua",
    "Projectiles/ProjectileComponentData.lua",
    "Projectiles/ProjectileRecipe.lua",
    "Projectiles/ProjectileType.lua",

	-- Buff Components
	"Projectiles/Components/Buffs/RemoteBoosterComponent.lua",

	-- Damage Components
	"Projectiles/Components/Damage/DamageComponent.lua",
	"Projectiles/Components/Damage/DoTComponent.lua",
	"Projectiles/Components/Damage/ExplosiveComponent.lua",
	"Projectiles/Components/Damage/KineticDamageComponent.lua",

	-- Misc Components
	"Projectiles/Components/Misc/BeamCollisionComponent.lua",
	"Projectiles/Components/Misc/CollisionComponent.lua",
	"Projectiles/Components/Misc/ExampleComponent.lua",
	"Projectiles/Components/Misc/TargetingComponent.lua",
	"Projectiles/Components/Misc/dronecontrolcomponent.lua",
	"Projectiles/Components/Misc/logisticsdronecontrolcomponent.lua",
	"Projectiles/Components/Misc/TimedEventComponent.lua",
	"Projectiles/Components/Misc/MiningComponent.lua",
	"Projectiles/Components/Misc/SalvagingComponent.lua",

	-- Movement Components
	"Projectiles/Components/Movement/MovementComponent.lua",
	"Projectiles/Components/Movement/LocalizedMovementComponent.lua",
	"Projectiles/Components/Movement/HomingMovementComponent.lua",
	"Projectiles/Components/Movement/ThrusterMovementComponent.lua",

	-- Render Components
	"Projectiles/Components/Rendering/EffectComponent.lua",
	"Projectiles/Components/Rendering/HitEffectComponent.lua",
	"Projectiles/Components/Rendering/ModelComponent.lua",
	"Projectiles/Components/Rendering/TrailComponent.lua",

	-- Effect Components
    "Projectiles/Components/Rendering/Effects/MjolnirEffectComponent.lua",
    "Projectiles/Components/Rendering/Effects/PulseBeamEffectComponent.lua",
	"Projectiles/Components/Rendering/Effects/BeamEffectComponent.lua",
	"Projectiles/Components/Rendering/Effects/TachyonBeamEffectComponent.lua",
	"Projectiles/Components/Rendering/Effects/RailgunEffectComponent.lua",
	"Projectiles/Components/Rendering/Effects/MACEffectComponent.lua",
	"Projectiles/Components/Rendering/Effects/MissileEffectComponent.lua",
	"Projectiles/Components/Rendering/Effects/BerthaEffectComponent.lua",
	"Projectiles/Components/Rendering/Effects/PulseCannonEffectComponent.lua",
	"Projectiles/Components/Rendering/Effects/AntimatterEffectComponent.lua",
	"Projectiles/Components/Rendering/Effects/ParticleBlasterEffectComponent.lua",
	"Projectiles/Components/Rendering/Effects/PlasmaBlasterEffectComponent.lua",
	"Projectiles/Components/Rendering/Effects/PlasmaBeamEffectComponent.lua",

	-- Hit Effect Components
	"Projectiles/Components/Rendering/Effects/MjolnirHitEffectComponent.lua"
}

for _,v in pairs(SharedIncludeTbl) do
	include(string.lower(v))
end

if CLIENT then return end

local ServerIncludeTbl = {
	-- NPC Ships
	"npcs/npcshipmanager.lua",
	"npcs/npcship.lua",
	"npcs/activities/base.lua",
	"npcs/activities/wander.lua",
	"npcs/activities/leave.lua",
	"npcs/activities/mining.lua",
	"npcs/activities/refining.lua",
	"npcs/activities/transport.lua",
	"npcs/activities/combat.lua",

	-- Logistics
	"logistics/node.lua",
	"logistics/courier.lua",

	-- Misc
	"astar.lua"
}

for _,v in pairs(ServerIncludeTbl) do
	include(string.lower(v))
end