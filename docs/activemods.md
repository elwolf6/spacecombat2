For clarity, each mod will run on the following 5-stage system:


Activation: Initial costs taken, buffs applied.

ModTick: Over-time stuff goes here. Runs until ActivationTime is up (or on Continuous mods, until Activate input is off).

Deactivation: Reset buffs, cause things on shutdown, etc. Starts Cooldown and Reset timers.

Reset: Undo any Deactivation debuffs.

Cooldown: Module is silent. OnCooldown hook is called at the END of the timer.




Name: Overcharge Module (Codename: Berserker)

On Activation: +300% Weapon Damage, +300% Weapon Capacitor Cost

On Tick: Ship takes 5% Max Health as THERM damage per second (can be mitigated with resistances)

On Deactivation: -50% Weapon Damage

Activation Period: 10 seconds

Reset Period: 30 seconds

Cooldown: 120 seconds


Module concepts that don't fully adhere to the stages [names big placeholders]:
Fighter Extra Life:
Usable only on fighters, when activated, if the ship takes lethal damage, all HP instead refills to half and the ship gains 5 seconds of invulnerability. However, after this the module is permanently burned out.
Siege Mode:
Dreadnought module. On activation, ship gains damage and resistance bonuses while constantly slowly draining cap. However, the ship cannot move while this is on, and any change in position shuts the module down.





Name: Triage Module





Desc: Divert power from capital ship engine systems to fleet assistance and repair modules. DONT MOVE U DONUT.





On Activation:

Set Max movement speed to 0
Increase Remote repair effectiveness by 450%
Increase Self repair by 100%
Decrease incoming repair by 100%







On Tick: If ship moves, permanently burn out the module, disable all buffs.





On Deactivation: Remove all buffs, enforce no movement for another 10 seconds





Activation Period: 60 seconds





Continuous: true





Cool down: 120 seconds





Class Requirement: Dreadnought / titan.






Name: Shutdown and Repair

Classes: Fighter, Frigate, Cruiser

Slot: Low

On Activation: Max Movement Speed Reduced to 5%, Resistances Set to -80%

On Tick: Repairs 10% of Max Hull/Armor, Consumes 75% of Repaired Health from Capacitor. If there is insufficient cap, module does not heal this tick.

On Deactivation: Nothing

Activation Period: 10 Seconds

Cooldown: 300 seconds





Name: Maneuvering Booster

Classes: Drone, Fighter, Frigate

Slot: High

On Activation: Max Speed Increased by 200%, Max Turn Rate Increased by 200%, Resistances Reduced by 30%

On Tick: Consumes power proportional to the speed moved past the base speed limit

Activation Period: 0 Seconds

Continuous: true

Cooldown: 0 seconds




Name: Blink Drive

Type: Utility

Classes: Drone, Fighter, Frigate, Cruiser, Battlecruiser, Battleship

Slot: MED

On Activation: Figure out the forward direction of the ship, or just inherit from Velocity, or something. Then teleports the ship X units (where X is some distance divided by sigrad) in that direction. Can be fitted with a Vector input, but is still distance-capped.

Cooldown: 60 seconds (maybe base on ship size?)

On that same note:


Name: Emergency Teleport

Type: Utility

Classes: Drone, Fighter, Frigate, Cruiser, Battlecruiser

Slot: MED

On Activation: Teleports the ship to a random, valid location on the map (use a large, cubic hull trace based on boxsize).

Cooldown: 300 seconds