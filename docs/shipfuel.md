The GyroPod used to consume fuel in exchange for a boost to acceleration and top speed, but most of us dodged the system (for more control or different flight models primarily) using ApplyForce/SetVel/SetPos, making the resource basically useless to us. In order to make these Wire functions reach the same limits, we'd need a custom branch of Wiremod to overwrite all these functions.

I still think Ship Fuel can work, but in a more passive than active role. The idea in brief being make the Ship Core consume fuel (or capacitor if fuel is missing) based on `½mv²`.

The best way I think this would work is to define three 'theoretical' speed limits per class, one for when using Ship Fuel, one for when using Capacitor, and one when neither is available.

(Example Speeds (mostly pulled out of my ass) in u/s)

| Class | Ship Fuel | Capacitor Only | Neither |
| :------ | :-----------: |  :-----------------: |  :--------: |
| Drone |  10,000 | 7,000 | 3,000 |
| Fighter | 8,000 | 6,000 | 2,500 |
| Frigate | 6,500 | 5,000 | 1,500 |
| Cruiser | 4,000 | 3,000 | 1,000 |
| Battlecruiser  | 3,000 | 2,000 | 500 |
| Battleship | 2,000 | 1,400 | 300 |
| Dreadnaught | 1,000 | 500 | 100 |
| Titan  | 500 | 250 | 50 |

These speeds would be used in the equations to determine fuel usage, so for example, on a Frigate:
* Provided Ship Fuel is available, between 0 and 6,500 u/s consumption follows a parabolic graph, as defined by `½mv²`
* Beyond 6,500 u/s, fuel usage starts to increase exponentially, quickly making it infeasible to move at these speeds for any extended duration unless your ship is made out of fuel storage
* If no ship fuel is available, the lower speed limit is applied, a stronger exponent is used, and the fuel efficiency is drastically decreased to account for the abundance of capacitor.
* Flying your ship on capacitor still needs to be viable, just that ship fuel needs to be more attractive.
* If you have no fuel and your capacitor is dry, an even harsher limit is applied, but moving under this speed limit is essentially free. Exceeding the limit however, would do proportional damage to structure instead, because clearly the inertial dampeners no longer have power, so clearly excessive speed will tear your ship apart.

Here's an example of consumption against speed for an average frigate

![Graph](http://puu.sh/qtXPg/1678504a76.png)

The equation I used: `(1/FuelPower)*Mass*(Speed)^(2*(1/ClassBonus)) + (ECMult/FuelPower)*((MAX(0,Speed-SpeedLimit))^(ECExp*(1/ClassBonus)))`
* `FuelPower` is a rough measure of how many unit of Kinetic Energy you should get per unit of Fuel, I used 2e9 for Ship Fuel
* `Mass` is mass of the ship, which isn't exposed by the core any more, but I'm sure you have a way to estimate it from the core calculations, I used 15 tonnes
* `ClassBonus` is a value around 1 that can be used to make lighter classes cheaper to power than heavier, I used 1.05 for a frigate, 0.95 would be about fair for a titan
* `ECMult` is a multiplier that decreases the effect of the `FuelPower` when in excess of the speed limit, I used 100
* `ECExp` is the exponent to use for the extra cost, I used 3

_______

Speed would need to be measured as change in position naturally, so you can't circumvent this system with SetPos or holograms, and should ideally be measured from the parent or GyroPod, unless you want your reserves to be taxed for angular momentum as well (though you would need a way to do this that won't unfairly punish those who chose to have the core in the back of the ship, gyro in the front, etc)

[ODS of the equation if you want to play around with numbers](/uploads/96407011330e54565ef29709cb9d95c6/Ship_Fuel_Consumption.ods)

______

I was originally just going to tell Brandon my idea over Steam chat, but he had to go and be offline, so I figured I'd make a post here. Next thing I know I'm flying a titan around with a speedometer debugged and an Excel sheet on the other screen. ¯\\\_(ツ)\_/¯