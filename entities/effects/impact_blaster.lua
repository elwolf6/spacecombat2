--[[*********************************************
	Scalable Blaster Impact Effect
	
	Author: Steeveeo
**********************************************]]--

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Emitter = ParticleEmitter(self.Pos)
	self.EmitterNorm = ParticleEmitter(self.Pos, true)
	self.Scale = data:GetScale()
	
	--Energy Ring
	local dir = self.Normal:Angle():Right()
	local ang = dir:Angle()
	for i=1, 10 do
		ang:RotateAroundAxis(self.Normal, 36)
		local velocity = ang:Forward() * (math.Rand(250, 275) * self.Scale) + (self.Normal * 10)
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4) .. "add", self.Pos)
		p:SetDieTime(math.Rand(0.3, 0.65))
		p:SetVelocity(velocity)
		p:SetAirResistance(150 * (self.Scale))
		p:SetStartLength(math.random(50, 75) * self.Scale)
		p:SetEndLength(math.random(50, 75) * self.Scale)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(50, 75) * self.Scale)
		p:SetEndSize(math.random(50, 75) * self.Scale)
		p:SetColor(math.random(128, 144), math.random(144, 196), 255)
	end
	
	--Normal Splash
	for i=1, 2 do
		local splash = self.EmitterNorm:Add("effects/flares/light-rays_001", self.Pos + self.Normal * 2)
		splash:SetDieTime(math.Rand(0.3, 1))
		splash:SetStartSize(150 * self.Scale)
		splash:SetEndSize(0)
		splash:SetStartAlpha(255)
		splash:SetEndAlpha(0)
		splash:SetColor(220, 240, 255)
		splash:SetRoll(math.random(0, 360))
		splash:SetAngles(self.Normal:Angle())
	end
	
	--Flare
	for i=1, 2 do
		local p = self.Emitter:Add("effects/flares/light-rays_001", self.Pos)
		p:SetDieTime(math.Rand(0.25, 0.5))
		p:SetStartAlpha(255 * self.Scale)
		p:SetEndAlpha(0)
		p:SetStartSize(450 * self.Scale)
		p:SetEndSize(0)
		p:SetColor(220, 220, 255)
	end
	
	--Sparks
	for i=1, 30 do
		local velocity = (self.Normal + (VectorRand() * 0.75)):GetNormalized() * (math.Rand(125, 500) * self.Scale)
		
		local p = self.Emitter:Add("effects/flares/halo-flare_001", self.Pos)
		p:SetDieTime(math.Rand(0.2, 0.65))
		p:SetVelocity(velocity)
		p:SetAirResistance(300)
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(15 * self.Scale)
		p:SetEndSize(0)
		p:SetColor(255, 255, 255)
	end
	
	--Play Sound
	math.randomseed(FrameTime())
	sound.Play("ship_weapons/wpn_plasma_blaster_hit.wav", self.Pos, 75, math.random(32, 75))
	sound.Play("ambient/explosions/exp2.wav", self.Pos, 80, math.random(125, 200))
	
	self.Emitter:Finish()
	self.EmitterNorm:Finish()
end

function EFFECT:Think()
  	return false
end

function EFFECT:Render()
	return false
end
