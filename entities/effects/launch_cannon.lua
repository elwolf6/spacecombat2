/**********************************************
	Scalable Generic Gun Fire Effect
	
	Author: Steeveeo
***********************************************/

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Scale = data:GetScale() or 1
	self.LifeTime = 0.1
	self.Emitter = ParticleEmitter(self.Pos)
	
	//Fire
	for i=1, 3 do
		local velocity = (self.Normal + (VectorRand() * 0.15)):GetNormalized() * math.Clamp(math.Rand(0, 500) * self.Scale, 0, 1500)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(0.05, 0.1) * self.Scale, 0.01, 0.15))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(10, 15) * self.Scale)
		p:SetEndSize(math.random(20, 35) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-1, 1))
		p:SetColor(255, 255, 200)
	end
	
	//Flare
	for i=1, 2 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(0.1, 0.15) * self.Scale, 0.1, 0.25))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.Rand(5, 100) * self.Scale)
		p:SetEndSize(math.Rand(5, 100) * self.Scale)
		p:SetColor(255, 255, 200)
	end
	
end

function EFFECT:Think( )
  	self.LifeTime = self.LifeTime - FrameTime()
	if self.LifeTime <= 0 then
		self.Emitter:Finish()
		return false
	end
	
	return true
end


function EFFECT:Render( )
	return false
end
