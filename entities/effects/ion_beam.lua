--[[*********************************************
	Scalable, Colorable Beam Effect
	
	Author: Steeveeo & Lt.Brandon
**********************************************]]--

local bMats = {}

bMats.Glow23 = GAMEMODE.MaterialFromVMT(
"bluelaser",
[["UnlitTwoTexture"

{
	"$baseTexture"  "effects/bluelaser1"
	"$Texture2" "effects/bluelaser1_smoke"
	"$nocull" "1"	
	"$nodecal" "1"
	"$additive" "1"
	"$no_fullbright" 1
	"$model" "1"
	"$nofog" "1"


	"Proxies"
	{
		"TextureScroll"
		{
			"texturescrollvar" "$texture2transform"
			"texturescrollrate" 1
			"texturescrollangle" 0
		}

		"Sine"
		{
			"sineperiod"	.06
			"sinemin"	".3"
			"sinemax"	".5"
			"resultVar"	"$alpha"
		}
	}
}]]
)
bMats.Glow24 = Material("effects/blueflare1")
bMats.Glow24:SetMaterialInt("$selfillum", 10)
bMats.Glow24:SetMaterialInt("$illumfactor",8)
local lMats = {}
lMats.Glow13 = Material("effects/blueblacklargebeam")
lMats.Glow13:SetMaterialInt("$vertexalpha", 1)
lMats.Glow13:SetMaterialInt("$vertexcolor", 1)
lMats.Glow13:SetMaterialInt("$additive", 1)
lMats.Glow13:SetMaterialInt("$nocull", 1)
lMats.Glow13:SetMaterialInt("$selfillum", 10)
lMats.Glow13:SetMaterialInt("$illumfactor",8)
lMats.Glow23 = GAMEMODE.MaterialFromVMT(
	"sc_blue_beam02",
	[["UnLitGeneric"
	{
		"$basetexture"		"effects/blueblacklargebeam"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
		"$illumfactor" 8
	}]]
)
--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )
	self.Scale 		= data:GetScale()
	self.ColLerp	= data:GetMagnitude()
	self.Entity		= data:GetEntity()
    self.StartOffset= self.Entity:WorldToLocal(data:GetStart())
	self.LocalAngle	= self.Entity:WorldToLocalAngles(data:GetNormal():Angle())
	self.EndOffset 	= self.Entity:GetRange()
	self.Color 		= Color(Lerp(self.ColLerp, 0, 255), Lerp(self.ColLerp, 255, 0), Lerp(self.ColLerp, 0, 255), 255)

	self.CurrentAngle = self.Entity:LocalToWorldAngles(self.LocalAngle):Forward()
	self.StartPos = self.Entity:GetPos() + (self.CurrentAngle * self.StartOffset)
	self.MaxRange = self.EndOffset
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	if not IsValid(self.Entity) then return false end
	
	if self.Dying then
		self.Scale = self.Scale - ((self.Scale * 5) * FrameTime())
	end

    self.CurrentAngle = self.Entity:LocalToWorldAngles(self.LocalAngle):Forward()
	self.StartPos = self.Entity:LocalToWorld(self.StartOffset)
	self.EndPos = self.StartPos + (self.CurrentAngle * self.EndOffset)

	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.EndPos
	tracedata.filter = {self.Entity}
	local tr = util.TraceLine(tracedata)
	
	if tr.Hit then
		self.EndPos = tr.HitPos
		self.LastHitRange = tr.HitPos:Distance(self.StartPos)
	else
		self.LastHitRange = self.Entity:GetRange()
	end
	
	self:SetRenderBoundsWS(self.StartPos, self.EndPos)
	
	if not self.Entity:GetShooting() and not self.Dying then
		timer.Simple(1, function() 
			self.Dead = true
		end)
		
		self.Dying = true
	end
	
	if self.Dead then
		return false
	end
	
	return true
end


--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	local Size = math.random(200,400) * self.Scale
	render.SetMaterial( bMats.Glow24 )
	
	if not self.Dying then
	   	render.DrawSprite(self.StartPos, Size , Size , self.Color) 
	   	render.DrawSprite(self.StartPos, Size/2 , Size/2 , Color(255, 255, 255, 255))
	   	render.DrawSprite(self.StartPos, Size , Size , self.Color) 
	   	render.DrawSprite(self.StartPos, Size/2 , Size/2 , Color(255, 255, 255, 255))
	   	render.DrawSprite(self.StartPos, Size , Size , self.Color) 
	   	render.DrawSprite(self.StartPos, Size/2 , Size/2 , Color(255, 255, 255, 255))
	end

   	local Offset = self.CurrentAngle * (-25 * self.Scale)
   	render.DrawSprite(self.EndPos + Offset, Size*2 , Size*2, self.Color) 
   	render.DrawSprite(self.EndPos + Offset, Size, Size , Color(255, 255, 255, 255))
   	render.DrawSprite(self.EndPos + Offset, Size*2, Size*2, self.Color) 
   	render.DrawSprite(self.EndPos + Offset, Size, Size , Color(255, 255, 255, 255)) 
   	render.DrawSprite(self.EndPos + Offset, Size*2 , Size*2, self.Color) 
   	render.DrawSprite(self.EndPos + Offset, Size, Size , Color(255, 255, 255, 255)) 	
	
	render.SetMaterial( bMats.Glow23 )	
   	render.DrawBeam( self.EndPos, self.StartPos, (100 + (15 * math.sin(CurTime() * (25 + math.Rand(1, 15))))) * self.Scale, 110, 0, Color( 0, 255, 0, 200 ) )
  
	render.SetMaterial( bMats.Glow24 )	
   	render.DrawBeam( self.EndPos, self.StartPos, 100 * self.Scale, 0.5, 0.5, self.Color )	
   	render.DrawBeam( self.EndPos, self.StartPos, 100 * self.Scale, 0.5, 0.5, self.Color )	
   	render.DrawBeam( self.EndPos, self.StartPos, 100 * self.Scale, 0.5, 0.5, self.Color )
   	render.DrawBeam( self.EndPos, self.StartPos, (50 + (25 * math.sin(CurTime() * 45))) * self.Scale, 0.5, 0.5, Color( 255, 255, 255, 255 ) )	
   	render.DrawBeam( self.EndPos, self.StartPos, (50 + (25 * math.cos(CurTime() * 65))) * self.Scale, 0.5, 0.5, Color( 255, 255, 255, 255 ) )	
   	render.DrawBeam( self.EndPos, self.StartPos, 50 * self.Scale, 0.5, 0.5, Color( 255, 255, 255, 255 ) )
   	render.DrawBeam( self.EndPos, self.StartPos, 50 * self.Scale, 0.5, 0.5, Color( 255, 255, 255, 255 ) )
				 
end
