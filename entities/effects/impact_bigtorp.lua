--[[*********************************************
	Huge Torpedo Explosion Effect (mininuke?)
	
	Author: Steeveeo
**********************************************]]--

util.PrecacheSound( "ambient/explosions/explode_1.wav" )
util.PrecacheSound( "ambient/explosions/explode_2.wav" )
util.PrecacheSound( "ambient/explosions/explode_3.wav" )
util.PrecacheSound( "weapons/explode1.wav" )
util.PrecacheSound( "weapons/explode2.wav" )

function EFFECT:Init( data )

	--Effect Settings
	self.Sounds = {
		"ambient/explosions/explode_1.wav",
		"ambient/explosions/explode_2.wav",
		--"ambient/explosions/explode_3.wav",
		"weapons/explode1.wav",
		"weapons/explode2.wav"
	}

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Emitter = ParticleEmitter(self.Pos)
	
	--Smoke Ring
	local dir = self.Normal:Angle():Right()
	local ang = dir:Angle()
	local ringParticles = 35
	for i=1, ringParticles do
		ang:RotateAroundAxis(self.Normal, 360 / ringParticles)
		local velocity = ang:Forward() * math.Rand(1500, 5000) + (self.Normal * 250)
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(7, 10))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(500, 750))
		p:SetEndSize(math.random(500, 750))
		p:SetRollDelta(math.Rand(-0.1, 0.1))
		local grey = math.random(16,128)
		p:SetColor(grey, grey, grey)
	end
	
	--Fire
	for i=1, 15 do
		local velocity = (self.Normal + (VectorRand() * 0.65)):GetNormalized() * math.Rand(500, 2500)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Rand(2, 5))
		p:SetVelocity(velocity)
		p:SetGravity(self.Normal * 300)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(750, 1500))
		p:SetEndSize(math.random(750, 1500))
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(255, 255, 255)
	end
	
	--Smoke Plume
	for i=1, 15 do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * math.Rand(250, 2750)
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(7, 10))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(375, 750))
		p:SetEndSize(math.random(375, 750))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		local grey = math.random(64,196)
		p:SetColor(grey, grey, grey)
	end
	
	--Flare
	for i=1, 5 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(0.5, 3))
		p:SetVelocity(Vector(math.Rand(-1500, 1500), math.Rand(-1500, 1500), math.Rand(-1500, 1500)) + (self.Normal * 1500))
		p:SetGravity(self.Normal * 600)
		p:SetAirResistance(750)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(3500)
		p:SetEndSize(3500)
		p:SetColor(255, 255, 255)
	end
	
	--Play Sound
	math.randomseed(FrameTime())
	sound.Play( self.Sounds[math.random(1,#self.Sounds)], self.Pos, 511)
	
	self.Emitter:Finish()
end

function EFFECT:Think( )
  	return false
end


function EFFECT:Render( )
	return false
end
