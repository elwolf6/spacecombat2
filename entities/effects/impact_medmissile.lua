--[[*********************************************
	Medium Missile Explosion Effect
	
	Author: Steeveeo
**********************************************]]--

util.PrecacheSound( "ambient/explosions/explode_1.wav" )
util.PrecacheSound( "ambient/explosions/explode_3.wav" )
util.PrecacheSound( "ambient/explosions/explode_4.wav" )

function EFFECT:Init( data )

	self.Sounds = {
		"ambient/explosions/explode_1.wav",
		"ambient/explosions/explode_3.wav",
		"ambient/explosions/explode_4.wav"
	}

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Emitter = ParticleEmitter(self.Pos)
	
	--Smoke Ring
	local dir = self.Normal:Angle():Right()
	local ang = dir:Angle()
	local ringParticles = 15
	for i=1, ringParticles do
		ang:RotateAroundAxis(self.Normal, 360 / ringParticles)
		local velocity = ang:Forward() * math.Rand(250, 750) + (self.Normal * 25)
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(2, 3))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(100, 125))
		p:SetEndSize(math.random(100, 125))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		local grey = math.random(16,128)
		p:SetColor(grey, grey, grey)
	end
	
	--[[
	--Fire Ring
	local dir = self.Normal:Angle():Right()
	local ang = dir:Angle()
	local ringParticles = 10
	for i=1, ringParticles do
		ang:RotateAroundAxis(self.Normal, 360 / ringParticles)
		local velocity = ang:Forward() * math.Rand(250, 500) + (self.Normal * 25)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Rand(0.25, 0.75))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(75, 175))
		p:SetEndSize(math.random(75, 175))
		p:SetRollDelta(math.Rand(-5, 5))
		p:SetColor(255, 255, 255)
	end
	]]--
	
	--Fire
	for i=1, 20 do
		local velocity = (self.Normal + (VectorRand() * 0.65)):GetNormalized() * math.Rand(100, 750)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Rand(0.25, 0.75))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(75, 150))
		p:SetEndSize(math.random(75, 150))
		p:SetColor(255, 255, 255)
	end
	
	--Smoke Plume
	for i=1, 10 do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * math.Rand(75, 350)
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(2, 3))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(75, 150))
		p:SetEndSize(math.random(75, 150))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		local grey = math.random(64,196)
		p:SetColor(grey, grey, grey)
	end
	
	--Play Sound
	math.randomseed(FrameTime())
	sound.Play( self.Sounds[math.random(1,3)], self.Pos)
	
	self.Emitter:Finish()
end

function EFFECT:Think( )
	return false
end

function EFFECT:Render( )
	return false
end
