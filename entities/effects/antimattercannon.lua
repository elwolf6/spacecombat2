 local Glow = GAMEMODE.MaterialFromVMT(
	"StaffGlow",
	[["UnLitGeneric"
	{
		"$basetexture"		"sprites/light_glow01"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)

local Shaft = Material("effects/ar2ground2");

--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )
	self.StartPos	= data:GetStart()
	self.Forward	= data:GetNormal()
	self.size		= data:GetScale()
	self.vel		= data:GetMagnitude()
	self.time		= CurTime()	
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	self.EndPos = self.StartPos + (self.Forward * self.vel * (CurTime() - self.time))
	
	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.EndPos
	local tr = util.TraceLine(tracedata)
	
	if tr.Hit then
		self.EndPos = tr.HitPos
		self.Dead = true
	end
	
	self.StartPos = self.EndPos
	
	self:SetRenderBoundsWS( self.StartPos, self.StartPos + (self.Forward * -100 * self.size) )
	
	if self.Dead then
		return false
	end
	
	self.time = CurTime()
	
	return true
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	local r, g, b, size = math.random(155)+100, math.random(155)+100, math.random(155)+100, math.random(150,200)*self.size
	render.SetMaterial(Shaft)
	render.DrawBeam(self.StartPos,self.StartPos + (self.Forward * -1000 * self.size),(50+math.random(25)) * self.size,1,0,Color(62,g,b,150))
	render.DrawBeam(self.StartPos,self.StartPos + (self.Forward * -1000 * self.size),(50+math.random(25)) * self.size,1,0,Color(r-25,0,0,255))
	render.SetMaterial(Glow)
	render.DrawSprite(self.StartPos,size*(math.random(1)+1),size,Color(0,g,b,220))
	render.DrawSprite(self.StartPos,size,size*(math.random(1)+1),Color(r,0,50,255))
	render.DrawSprite(self.StartPos,size*3,size*3,Color(120,g,0,140))
end
