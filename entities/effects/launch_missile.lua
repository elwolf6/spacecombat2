/**********************************************
	Scalable Rocket Fire Effect
	
	Author: Steeveeo
***********************************************/

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Scale = data:GetScale() or 1
	self.Emitter = ParticleEmitter(self.Pos)
	
	//Smoke Exhaust
	for i=1, 15 do
		local velocity = (-self.Normal + (VectorRand() * 0.15)):GetNormalized() * (math.Rand(0, 750) * (self.Scale / 2))
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(1, 3) * self.Scale, 0.5, 5))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(128)
		p:SetEndAlpha(0)
		p:SetStartSize(0)
		p:SetEndSize(math.random(75, 150) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		local grey = math.random(64,196)
		p:SetColor(grey, grey, grey)
	end
	
	//Fire Exhaust
	for i=1, 25 do
		local velocity = (-self.Normal + (VectorRand() * 0.15)):GetNormalized() * (math.Rand(250, 1250) * (self.Scale / 3))
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(0.1, 0.25) * self.Scale, 0.1, 0.45))
		p:SetVelocity(velocity)
		p:SetAirResistance(35)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(0, 0) * self.Scale)
		p:SetEndSize(math.random(25, 45) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-30, 30))
		p:SetColor(255, 255, 200)
	end
	
	
	//Smoke Forward
	for i=1, 10 do
		local velocity = (self.Normal + (VectorRand() * 0.35)):GetNormalized() * (math.Rand(0, 500) * (self.Scale / 3))
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(1, 3) * self.Scale, 0.5, 5))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(128)
		p:SetEndAlpha(0)
		p:SetStartSize(0)
		p:SetEndSize(math.random(50, 75) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		local grey = math.random(64,196)
		p:SetColor(grey, grey, grey)
	end
	
	//Fire Forward
	for i=1, 15 do
		local velocity = (self.Normal + (VectorRand() * 0.35)):GetNormalized() * (math.Rand(0, 500) * (self.Scale / 3))
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(0.2, 0.35) * self.Scale, 0.2, 0.75))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(0, 0) * self.Scale)
		p:SetEndSize(math.random(25, 35) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-5, 5))
		p:SetColor(255, 255, 200)
	end
end

function EFFECT:Think( )
  	self.Emitter:Finish()
	return false
end


function EFFECT:Render( )
	return false
end
