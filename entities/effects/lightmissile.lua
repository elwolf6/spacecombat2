local Shaft = Material("effects/ar2ground2");

--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )
	self.StartPos	= data:GetStart()
	self.Forward	= data:GetNormal()
	self.det		= data:GetScale()
	self.vel		= data:GetMagnitude()
	self.turn		= data:GetRadius()
	self.Ent		= data:GetEntity()
	self.time		= CurTime()
	
	--TODO: Replace this with a missile model
	self.Model		= ClientsideModel("models/slyfo/data_probe.mdl", 7)
	self.emitter = ParticleEmitter( self.StartPos )	
	self.Model:SetModelScale(Vector(1,3,1))
	
	timer.Simple(self.det, function() if IsValid(self) then self.Dead = true end end)
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	if IsValid(self.Ent) then
		self.target = self.Ent:GetTargetPos(Vector(0,0,0))
	end

	local dir = self.StartPos - self.target
    dir = dir / dir:Length()
	local mod = math.Clamp(self.turn * (2 - (self.Forward:Dot(dir) + 1)), 0, self.turn)
	self.Forward = dir * -mod + (1-mod) * self.Forward
	self.EndPos = self.StartPos + (self.Forward * self.vel * (CurTime() - self.time))
	
	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.EndPos
	local tr = util.TraceLine(tracedata)
	
	self.time = CurTime()
	
	if tr.Hit and tr.Entity ~= self.Model then
		self.EndPos = tr.HitPos
		self.Dead = true
	end
	
	self.StartPos = self.EndPos
	self.Model:SetPos(self.StartPos)
	self.Model:SetAngles(self.Forward:Angle())
	self:SetRenderBoundsWS( self.StartPos, self.StartPos + (self.Forward * -250) )
	
	if not IsValid(self.Ent) then self.Dead = true end
	
	if self.Dead or self.Ent:GetDestroyProjectiles() or self.StartPos:Distance(self.target) <= self.Ent:GetDetonateDistance(1) then
		self.Model:Remove()
        self.emitter:Finish()
		return false
	end

    local particle = self.emitter:Add( "particles/flamelet"..math.random(1,5), self.StartPos + (self.Forward * -10))
	if particle then
		particle:SetVelocity(self.Forward * -math.random(50))
		particle:SetLifeTime( 0 )
		particle:SetDieTime( 0.25 )
		particle:SetStartAlpha( math.Rand( 230, 255 ) )
		particle:SetEndAlpha( 0 )
		particle:SetStartSize( 27 )
		particle:SetEndSize( 0 )
		particle:SetRoll( math.Rand(0, 360) )
		particle:SetRollDelta( math.Rand(-10, 10) )
		particle:SetColor( 255 , 255 , 255 ) 
	end

    local smoke = self.emitter:Add( "particles/smokey", self.StartPos )
	if smoke then
		smoke:SetPos(self.StartPos + self.Forward * -10)
		smoke:SetVelocity( self.Forward * -math.random( 50 ) )
		smoke:SetLifeTime( 0 )
		smoke:SetDieTime( 0.5 )
		smoke:SetStartAlpha( math.random( 30 ) + 30 )
		smoke:SetEndAlpha( 0 )
		smoke:SetStartSize(math.random( 10 ) + 15)
		smoke:SetEndSize( math.random( 15 ) + 45 )
		smoke:SetRoll( math.random( 359 ) + 1 )
		smoke:SetRollDelta( math.random( -2, 2 ) )
		smoke:SetColor( 175, 175, 175 )
	end
	
	return true
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	render.SetMaterial(Shaft)
	render.DrawBeam(self.StartPos,self.StartPos + (self.Forward * -250),20,1,0,Color(255,180,180,255))
end
