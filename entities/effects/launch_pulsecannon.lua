--[[*********************************************
	Scalable Pulse Cannon Launch Effect
	
	Author: Steeveeo
**********************************************]]--

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Dir = data:GetNormal() or Vector(0,0,0)
	self.Normal = data:GetStart()
	self.Scale = data:GetScale()
	self.Emitter = ParticleEmitter( self.Pos )
	
	--Ejecta
	for i=1, math.Clamp(5 * (self.Scale/2), 5, 15) do
		local vec = (self.Dir + (VectorRand() * 0.25)):GetNormalized()
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4) .. "add", self.Pos)
		p:SetVelocity(vec * (1500 + math.random(15)))
		p:SetAirResistance(2000)
		p:SetDieTime(math.Rand(0.1,0.25))
		p:SetStartLength((20 + math.random(15)) * self.Scale)
		p:SetEndLength((100 + math.random(75)) * self.Scale)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize((10 + math.random(5)) * self.Scale)
		p:SetEndSize((70 + math.random(40)) * self.Scale)
		p:SetColor(math.random(0,162), math.random(0,162), 255)
	end
	
	--Flare
	for i=1, 2 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(0.1, 0.25))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(100 * self.Scale)
		p:SetEndSize(100 * self.Scale)
		p:SetColor(200, 225, 255)
	end
	
	self.Emitter:Finish()
end

function EFFECT:Think()
	return false
end

function EFFECT:Render()
	return false
end
