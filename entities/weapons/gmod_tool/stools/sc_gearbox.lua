TOOL.Category		= "Mechanical Tools"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Gearbox Spawner"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.Information = {
	"left"
}

TOOL.ClientConVar["type"] = "Default"
TOOL.ClientConVar["finaldrive"] = 1
TOOL.ClientConVar["dualclutch"] = 0
TOOL.ClientConVar["dualdiff"] = 0

if ( CLIENT ) then
    language.Add( "Tool.sc_gearbox.name", "Space Combat 2 Gearbox Creation Tool" )
    language.Add( "Tool.sc_gearbox.desc", "Creates an Gearbox." )
    language.Add( "Tool.sc_gearbox.left", "Create Gearbox" )
    language.Add( "undone_sc_gearbox", "Undone Gearbox" )
end

cleanup.Register( "sc_gearbox" )

function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end

    local ply = self:GetOwner()
    local Settings = {
        ["FinalDrive"] = self:GetClientNumber("finaldrive", 1),
        ["HasDualClutch"] = self:GetClientNumber("dualclutch", 0) == 1,
        ["HasDualDiff"] = self:GetClientNumber("dualdiff", 0) == 1,
    }

    if IsValid(trace.Entity) and trace.Entity.IsGearbox then
        trace.Entity:ApplySettings(Settings)
        ply:ChatPrint("Gearbox updated!")
        return
    end

    local Gearbox = ents.Create("sc_gearbox")
    local Ang = ply:EyeAngles()
    Ang.yaw = Ang.yaw + 180 -- Rotate it 180 degrees in my favour
    Ang.roll = 0
    Ang.pitch = 0

    Gearbox:SetPos(trace.HitPos)

    -- Once the model has been set use it's OBB to move the turret out of the ground
    timer.Simple(0, function()
        if IsValid(Gearbox) then
            Gearbox:SetPos(trace.HitPos - trace.HitNormal * Gearbox:OBBMins().z)
        end
    end)

    Gearbox:SetAngles(Ang)
    Gearbox:Spawn()
    Gearbox:Activate()
    Gearbox:SetupGearbox(ply:GetInfo("sc_gearbox_type"), Settings)
    Gearbox.Owner = ply

    local PhysObj = Gearbox:GetPhysicsObject()
    if not IsValid(PhysObj) then
        SC.Error("[SC2] - ERROR: Gearbox spawned without a valid physics object! What?!\n", 5)
        return
    end
    PhysObj:EnableMotion(false)

    undo.Create("sc_gearbox")
        undo.AddEntity( Gearbox )
        undo.SetPlayer( ply )
    undo.Finish()

    ply:AddCleanup( "sc_gearbox", Gearbox )

    return true
end

local ConVarsDefault = TOOL:BuildConVarList()
function TOOL.BuildCPanel(Panel)
	Panel:ClearControls()
	Panel:SetName("Space Combat 2 - Gearboxes")

    -- Preset control
    Panel:AddControl("ComboBox", {
        MenuButton = 1,
        Folder = "sc_gearbox",
        Options = {
            ["#preset.default"] = ConVarsDefault
        },
        CVars = table.GetKeys(ConVarsDefault)
    })

	Panel:Help("Select an Gearbox Type")

    local Gearboxes = GAMEMODE.Mechanical.Gearboxes
	local SelectedGearbox = LocalPlayer():GetInfo("sc_gearbox_type")
	if not table.HasValue(Gearboxes, SelectedGearbox) then
		SelectedGearbox = next(Gearboxes)
		RunConsoleCommand("sc_gearbox_type", SelectedGearbox)
	end

	local GearboxTypeBox = vgui.Create("DCategoryTree", Panel)
	GearboxTypeBox:SetHeight(300)
    GearboxTypeBox:PopulateFromTableByValue(Gearboxes, "Category", "Name", "Hidden")
    Panel:AddPanel(GearboxTypeBox)

	GearboxTypeBox.OnNodeSelected = function(Parent, Node)
		RunConsoleCommand("sc_gearbox_type", Node.Data)

        local GearboxData = Gearboxes[Node.Data]
        if GearboxData then
            RunConsoleCommand("sc_gearbox_finaldrive", GearboxData.FinalDrive)
        end
    end

    Panel:NumSlider("Final Drive", "sc_gearbox_finaldrive", 0.05, 10)
    Panel:ControlHelp("Final gear ratio applied to all gears of the gearbox.")

    Panel:CheckBox("Dual Clutch", "sc_gearbox_dualclutch")
    Panel:ControlHelp("With a dual clutch the left and right outputs have separate clutches")

    Panel:CheckBox("Dual Diff", "sc_gearbox_dualdiff")
    Panel:ControlHelp("With a dual diff the left and right outputs can spin in different directions")
end
