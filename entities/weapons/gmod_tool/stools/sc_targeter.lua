TOOL.Category = "Ship Tools"
TOOL.Name = "#Targeter Spawner"
TOOL.Command = nil
TOOL.ConfigName = ""
TOOL.Tab = "Space Combat 2"

TOOL.ClientConVar["model"] = "models/maxofs2d/cube_tool.mdl"

if CLIENT then
    language.Add("tool.sc_targeter.name", "Space Combat 2 Targeter")
	language.Add("Tool.sc_targeter.desc", "Customize controls with the tool. READ THE INSTRUCTIONS!")
	language.Add("Tool.sc_targeter.0", "Left-Click to spawn.")
end

if SERVER then
	CreateConVar('sbox_maxsc_targeter', 20)
end
cleanup.Register( "sc_targeter" )

local function ApplySettings(self, Targeter)

end

--Spawn the Targeter
function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
    if CLIENT then return true end

	local ply = self:GetOwner()
    if IsValid(trace.Entity) and trace.Entity:GetClass() == "sc_targeter" and (not trace.Entity.CPPICanTool or trace.Entity:CPPICanTool(ply)) then
        ApplySettings(self, trace.Entity)
		return true
    end

    if not self:GetSWEP():CheckLimit("sc_targeter") then return false end

	local Ang = trace.HitNormal:Angle()
	Ang.pitch = Ang.pitch + 90
	local Model = self:GetClientInfo("model")
    local Targeter = ents.Create("sc_module_targeter")
    Targeter:SetAngles(Ang)
    Targeter:SetPos(trace.HitPos)
    Targeter:SetModel(Model)
    Targeter:Spawn()
    Targeter:SetPlayer(ply)

    ApplySettings(self, Targeter)

	local min = Targeter:OBBMins()
    Targeter:SetPos(trace.HitPos - trace.HitNormal * min.z)

	undo.Create("sc_targeter")
		undo.AddEntity(Targeter)
		undo.SetPlayer(ply)
    undo.Finish()

	ply:AddCleanup("sc_targeter", Targeter)
	return true
end

local DefaultKeys = {}
DefaultKeys["NextTarget"] = KEY_Q
DefaultKeys["PrevTarget"] = BUTTON_CODE_NONE
DefaultKeys["NextFilter"] = KEY_TAB
DefaultKeys["PrevFilter"] = BUTTON_CODE_NONE
DefaultKeys["SelectTarg"] = MOUSE_MIDDLE

local KeyDisplayNames = {}
KeyDisplayNames["NextTarget"] = "Next Target"
KeyDisplayNames["PrevTarget"] = "Previous Target"
KeyDisplayNames["NextFilter"] = "Next Filter"
KeyDisplayNames["PrevFilter"] = "Previous Filter"
KeyDisplayNames["SelectTarg"] = "Select Target"

local Keys = {}
Keys[1] = "NextTarget"
Keys[2] = "PrevTarget"
Keys[3] = "NextFilter"
Keys[4] = "PrevFilter"
Keys[5] = "SelectTarg"

if SERVER then
    util.AddNetworkString("SC.Targeter.UpdateKeys")
    net.Receive("SC.Targeter.UpdateKeys", function(Length, Ply)
        if IsValid(Ply) then
            local NewKeys = net.ReadTable()

            if not Ply.TargeterKeys then
                Ply.TargeterKeys = table.Copy(DefaultKeys)
            end

            for i,k in pairs(NewKeys) do
                if Ply.TargeterKeys[i] ~= nil then
                    Ply.TargeterKeys[i] = k
                end
            end
        end
    end)

    hook.Add("PlayerInitialSpawn", "SetupTargeterDefaultKeys", function(Ply)
        if IsValid(Ply) and not Ply.TargeterKeys then
            Ply.TargeterKeys = table.Copy(DefaultKeys)
        end
    end)
else
    hook.Add("InitPostEntity", "SendTargeterKeysToServer", function()
        local SavedKeys = {}

        for i,k in pairs(DefaultKeys) do
            SavedKeys[i] = cookie.GetNumber("sc_targeter_"..i, k)
        end

        net.Start("SC.Targeter.UpdateKeys")
        net.WriteTable(SavedKeys)
        net.SendToServer()
    end)
end

function TOOL.BuildCPanel(CPanel)
    local Label = vgui.Create("DLabel")
    Label:SetText("Entity Settings")
    Label:SetFont("DermaLarge")
    Label:SizeToContentsY(4)
    CPanel:AddItem(Label)

    CPanel:Help("These settings affect the spawned Targeter entity. Each Targeter can have different settings here. The settings are saved when the Targeter is duplicated.\n")

    Label = vgui.Create("DLabel")
    Label:SetText("Targeter Settings")
    Label:SetFont("DermaDefaultBold")
    Label:SizeToContentsY(2)
    Label:SetContentAlignment(5)
    CPanel:AddItem(Label)

    Label = vgui.Create("DLabel")
    Label:SetText("Player Controls")
    Label:SetFont("DermaLarge")
    Label:SizeToContentsY(4)
    CPanel:AddItem(Label)

    CPanel:Help("These controls will be used for piloting any ship to ensure a seamless piloting experience even if the owner of the ship you're driving uses different controls than you do.\n")

    local function CreateKeyBinder(Key, Default)
        local BinderLabel = vgui.Create("DLabel")
        BinderLabel:SetText(KeyDisplayNames[Key] or "INVALID KEY")
        BinderLabel:SetFont("DermaDefaultBold")
        BinderLabel:SizeToContentsY(2)
        BinderLabel:SetContentAlignment(5)

        local Binder = vgui.Create("DBinder")
        Binder:SetSize(110, 60)
        Binder:SetSelectedNumber(cookie.GetNumber("sc_targeter_"..Key, Default))

        function Binder:OnChange(NewKey)
            cookie.Set("sc_targeter_"..Key, NewKey)

            net.Start("SC.Targeter.UpdateKeys")
            net.WriteTable({[Key]=NewKey})
            net.SendToServer()
        end

        return Binder, BinderLabel
    end


    for i,k in ipairs(Keys) do
        local KeyBinder, BinderLabel = CreateKeyBinder(k, DefaultKeys[k])
        CPanel:AddItem(BinderLabel)
        CPanel:AddItem(KeyBinder)
    end
end