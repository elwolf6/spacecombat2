TOOL.Category = "Mechanical Tools"
TOOL.Name = "#Suspension Spawner"
TOOL.Command = nil
TOOL.ConfigName = ""
TOOL.Tab = "Space Combat 2"

-- Suspension
TOOL.ClientConVar["SuspensionLength"] = 7.5
TOOL.ClientConVar["SpringWeightRating"] = 2200
TOOL.ClientConVar["Stiffness"] = 10
TOOL.ClientConVar["Damping"] = 1.8
TOOL.ClientConVar["Multiplier"] = 1

-- Wheel
TOOL.ClientConVar["WheelModel"] = "models/sprops/trans/wheel_c/wheel45.mdl"
TOOL.ClientConVar["WheelScale"] = 1
TOOL.ClientConVar["WheelRadius"] = 22.5
TOOL.ClientConVar["WheelWidth"] = 8
TOOL.ClientConVar["WheelWeight"] = 25
TOOL.ClientConVar["WheelSlideFrictionCoefficient"] = 0.5
TOOL.ClientConVar["WheelRollingResistanceCoefficient"] = 0.375
TOOL.ClientConVar["MaxSidewaysForce"] = 2.5
TOOL.ClientConVar["MinCompressionGrip"] = 0.95
TOOL.ClientConVar["MaxCompressionGrip"] = 1.15
TOOL.ClientConVar["VehicleWeight"] = 1500
TOOL.ClientConVar["NumberOfWheels"] = 4
TOOL.ClientConVar["PeakTraction"] = 1.05
TOOL.ClientConVar["PeakSlipRatio"] = 6
TOOL.ClientConVar["SlipFalloff"] = 0.6
TOOL.ClientConVar["SlipFalloffRatio"] = 20

-- Steering
TOOL.ClientConVar["HasSteering"] = 1
TOOL.ClientConVar["SteeringAngle"] = 35
TOOL.ClientConVar["SteeringFrictionMultiplier"] = 1.2
TOOL.ClientConVar["SteeringSensitivity"] = 1

-- Brakes
TOOL.ClientConVar["HasBrakes"] = 1
TOOL.ClientConVar["BrakeTorque"] = 1600

if CLIENT then
    language.Add("tool.sc_suspension.name", "Space Combat 2 Suspension")
	language.Add("Tool.sc_suspension.desc", "Customize controls & sensitivity with inputs. READ THE INSTRUCTIONS!")
	language.Add("Tool.sc_suspension.0", "Left-Click to spawn.")
end

if SERVER then
	CreateConVar('sbox_maxsc_suspension', 20)
end
cleanup.Register( "sc_suspension" )

local function ApplySettings(self, Suspension)
    -- Apply Settings
    Suspension.SuspensionLength = self:GetClientNumber("SuspensionLength", 22.5)
    Suspension.SpringWeightRating = self:GetClientNumber("SpringWeightRating", 2200)
    Suspension.Stiffness = self:GetClientNumber("Stiffness", 10)
    Suspension.Damping = self:GetClientNumber("Damping", 1.8)
    Suspension.Multiplier = self:GetClientNumber("Multiplier", 1)

    -- Wheel
    Suspension.WheelModel = self:GetClientInfo("wheelmodel", "models/sprops/trans/wheel_c/wheel45.mdl")
    Suspension.WheelScale = Vector(1, 1, 1) * self:GetClientNumber("wheelscale", 1)
    Suspension.WheelOffset = Vector(0, 0, 0)
    Suspension.WheelRadius = self:GetClientNumber("wheelradius", 22.5)
    Suspension.WheelWidth = self:GetClientNumber("wheelwidth", 8)
    Suspension.WheelWeight = self:GetClientNumber("wheelweight", 25)
    Suspension.WheelSlideFrictionCoefficient = self:GetClientNumber("WheelSlideFrictionCoefficient", 0.5)
    Suspension.WheelRollingResistanceCoefficient = self:GetClientNumber("WheelRollingResistanceCoefficient", 0.375)
    Suspension.MaxSidewaysForce = self:GetClientNumber("MaxSidewaysForce", 2.5)
    Suspension.MinCompressionGrip = self:GetClientNumber("MinCompressionGrip", 0.95)
    Suspension.MaxCompressionGrip = self:GetClientNumber("MaxCompressionGrip", 1.15)
    Suspension.VehicleWeight = self:GetClientNumber("VehicleWeight", 1500)
    Suspension.NumberOfWheels = self:GetClientNumber("NumberOfWheels", 4)
    Suspension.PeakTraction = self:GetClientNumber("PeakTraction", 1.05)
    Suspension.PeakSlipRatio = self:GetClientNumber("PeakSlipRatio", 6)
    Suspension.SlipFalloff = self:GetClientNumber("SlipFalloff", 0.6)
    Suspension.SlipFalloffRatio = self:GetClientNumber("SlipFalloffRatio", 20)

    -- Steering
    Suspension.HasSteering = self:GetClientNumber("HasSteering", 1) == 1
    Suspension.SteeringAngle = self:GetClientNumber("SteeringAngle", 35)
    Suspension.SteeringFrictionMultiplier = self:GetClientNumber("SteeringFrictionMultiplier", 1.2)
    Suspension.SteeringSensitivity = self:GetClientNumber("SteeringSensitivity", 1)

    -- Brakes
    Suspension.HasBrakes = self:GetClientNumber("HasBrakes", 1) == 1
    Suspension.BrakeTorque = self:GetClientNumber("BrakeTorque", 1600) / 0.14

    Suspension:SetupWheel()
    Suspension:WriteCreationPacket()
end

--Spawn the Suspension
function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
    if CLIENT then return true end

	local ply = self:GetOwner()
    if IsValid(trace.Entity) and trace.Entity:GetClass() == "sc_suspension" and (not trace.Entity.CPPICanTool or trace.Entity:CPPICanTool(ply)) then
        ApplySettings(self, trace.Entity)
        ply:ChatPrint("Suspension updated!")
		return true
    end

    if not self:GetSWEP():CheckLimit("sc_suspension") then return false end

	local Ang = trace.HitNormal:Angle()
	Ang.pitch = Ang.pitch + 90
	local Model = self:GetClientInfo("model")
    local Suspension = ents.Create("sc_suspension")
    Suspension:SetAngles(Ang)
    Suspension:SetPos(trace.HitPos)
    Suspension:SetModel(Model)
    Suspension:Spawn()
    Suspension:SetPlayer(ply)

    ApplySettings(self, Suspension)

	local min = Suspension:OBBMins()
    Suspension:SetPos(trace.HitPos - trace.HitNormal * min.z)

	undo.Create("sc_suspension")
		undo.AddEntity(Suspension)
		undo.SetPlayer(ply)
    undo.Finish()

	ply:AddCleanup("sc_suspension", Suspension)
	return true
end

local DefaultKeys = {}
DefaultKeys["Throttle"] = KEY_W -- W
DefaultKeys["Brakes"]    = KEY_S -- S
DefaultKeys["TurnLeft"]    = KEY_A -- A
DefaultKeys["TurnRight"]   = KEY_D -- D

local KeyDisplayNames = {}
KeyDisplayNames["Throttle"] = "Throttle" -- W
KeyDisplayNames["Brakes"]    = "Brakes" -- S
KeyDisplayNames["TurnLeft"]    = "Turn Left" -- A
KeyDisplayNames["TurnRight"]   = "Turn Right" -- D

local Keys = {}
Keys[1]    = "Throttle"
Keys[2]    = "Brakes"
Keys[3]    = "TurnLeft"
Keys[4]    = "TurnRight"

if SERVER then
    util.AddNetworkString("SC.Suspension.UpdateKeys")
    net.Receive("SC.Suspension.UpdateKeys", function(Length, Ply)
        if IsValid(Ply) then
            local NewKeys = net.ReadTable()

            if not Ply.MechanicalKeys then
                Ply.MechanicalKeys = table.Copy(DefaultKeys)
            end

            for i,k in pairs(NewKeys) do
                if Ply.MechanicalKeys[i] ~= nil then
                    Ply.MechanicalKeys[i] = k
                end
            end
        end
    end)

    hook.Add("PlayerInitialSpawn", "SetupSuspensionDefaultKeys", function(Ply)
        if IsValid(Ply) and not Ply.MechanicalKeys then
            Ply.MechanicalKeys = table.Copy(DefaultKeys)
        end
    end)
else
    hook.Add("InitPostEntity", "SendMechanicalKeysToServer", function()
        local SavedKeys = {}

        for i,k in pairs(DefaultKeys) do
            SavedKeys[i] = cookie.GetNumber("sc_suspension_"..i, k)
        end

        net.Start("SC.Suspension.UpdateKeys")
        net.WriteTable(SavedKeys)
        net.SendToServer()
    end)
end

local ConVarsDefault = TOOL:BuildConVarList()
function TOOL.BuildCPanel(CPanel)
    -- Preset control
    CPanel:AddControl("ComboBox", {
        MenuButton = 1,
        Folder = "sc_suspension",
        Options = {
            ["#preset.default"] = ConVarsDefault
        },
        CVars = table.GetKeys(ConVarsDefault)
    })

    local Label = vgui.Create("DLabel")
    Label:SetText("Entity Settings")
    Label:SetFont("DermaLarge")
    Label:SizeToContentsY(4)
    CPanel:AddItem(Label)

    CPanel:Help("These settings affect the spawned Suspension entity. Each Suspension can have different settings here. The settings are saved when the Suspension is duplicated.\n")

    Label = vgui.Create("DLabel")
    Label:SetText("Suspension Settings")
    Label:SetFont("DermaDefaultBold")
    Label:SizeToContentsY(2)
    Label:SetContentAlignment(5)
    CPanel:AddItem(Label)

    CPanel:NumSlider("Suspension Length","sc_suspension_suspensionlength", 4, 50)
    CPanel:ControlHelp("The distance in source units from the start to the end of the suspension.")
    CPanel:NumSlider("Spring Weight Rating","sc_suspension_springweightrating", 1, 50000)
    CPanel:ControlHelp("The amount of weight the suspension is designed to hold in kg.")
    CPanel:NumSlider("Stiffness","sc_suspension_stiffness", 0.5, 20)
    CPanel:ControlHelp("How stiff the suspension is.")
    CPanel:NumSlider("Damping","sc_suspension_damping", 0.1, 20)
    CPanel:ControlHelp("The amoung of damping applied to suspension movements.")
    CPanel:NumSlider("Force Multiplier","sc_suspension_multiplier", 0.1, 2)
    CPanel:ControlHelp("Multiplies the amount of force applied by the suspension.")

    Label = vgui.Create("DLabel")
    Label:SetText("Wheel Settings")
    Label:SetFont("DermaDefaultBold")
    Label:SizeToContentsY(2)
    Label:SetContentAlignment(5)
    CPanel:AddItem(Label)

    local WheelSelect = CPanel:AddControl("PropSelect", {Label = "Wheel Model", ConVar = "sc_suspension_wheelmodel", Height = 5, Models = list.Get("WheelModels")})
    WheelSelect:SetHeight(200)
    CPanel:NumSlider("Wheel Scale","sc_suspension_wheelscale", 0.5, 10)
    CPanel:ControlHelp("The scale of the wheel model.")
    CPanel:NumSlider("Wheel Radius","sc_suspension_wheelradius", 1, 50)
    CPanel:ControlHelp("The radius of the wheel in source units.")
    CPanel:NumSlider("Wheel Width","sc_suspension_wheelwidth", 1, 50)
    CPanel:ControlHelp("The width of the wheel in source units")
    CPanel:NumSlider("Wheel Weight","sc_suspension_wheelweight", 1, 5000)
    CPanel:ControlHelp("The weight of the wheel in kg.")
    CPanel:NumSlider("Slide Friction Coefficient","sc_suspension_wheelslidefrictioncoefficient", 0.1, 10)
    CPanel:ControlHelp("The coefficient of friction that applies to lateral movement. Basically, this controls how much your car can drift.")
    CPanel:NumSlider("Rolling Resistance Coefficient","sc_suspension_wheelrollingresistancecoefficient", 0.1, 10)
    CPanel:ControlHelp("The coefficient of friction that applies to rolling. Basically, this controls how fast your car stops without any external forces.")
    CPanel:NumSlider("Max Sideways Force","sc_suspension_maxsidewaysforce", 0.1, 50)
    CPanel:ControlHelp("The maximum amount of force that will be applied as a result of lateral movement.")
    CPanel:NumSlider("Min Compression Grip","sc_suspension_mincompressiongrip", 0.1, 2)
    CPanel:ControlHelp("A multiplier on tire grip when the suspension is not compressed.")
    CPanel:NumSlider("Max Compression Grip","sc_suspension_maxcompressiongrip", 0.1, 2)
    CPanel:ControlHelp("A multiplier on tire grip when the suspension is fully compressed.")
    CPanel:NumSlider("Vehicle Weight","sc_suspension_vehicleweight", 1, 50000)
    CPanel:ControlHelp("The weight of the car. Set as accurately as possible for the best results!")
    CPanel:NumSlider("Number of Wheels","sc_suspension_numberofwheels", 1, 50)
    CPanel:ControlHelp("The total number of wheels on the car. Set this accurately or it won't work right.")
    CPanel:NumSlider("Peak Traction","sc_suspension_peaktraction", 1, 2)
    CPanel:ControlHelp("The amount of added traction you gain at the peak wheel slip ratio.")
    CPanel:NumSlider("Peak Slip Ratio","sc_suspension_peakslipratio", 0, 20)
    CPanel:ControlHelp("Where on the slip ratio curve do you reach peak traction")
    CPanel:NumSlider("Slip Falloff","sc_suspension_slipfalloff", 0, 1)
    CPanel:ControlHelp("How much does your traction fall off as the slip ratio increases")
    CPanel:NumSlider("Slip Falloff Ratio","sc_suspension_slipfalloffratio", 1, 20)
    CPanel:ControlHelp("Where on the slip ratio curve do you reach the lowest traction")

    Label = vgui.Create("DLabel")
    Label:SetText("Steering Settings")
    Label:SetFont("DermaDefaultBold")
    Label:SizeToContentsY(2)
    Label:SetContentAlignment(5)
    CPanel:AddItem(Label)

    CPanel:CheckBox("Enable Steering?", "sc_suspension_hassteering")
    CPanel:ControlHelp("Should steering be enabled for this wheel?")
    CPanel:NumSlider("Steering Angle","sc_suspension_steeringangle", 1, 90)
    CPanel:ControlHelp("The maximum angle the wheel will turn to steer.")
    CPanel:NumSlider("Steering Friction Multiplier","sc_suspension_steeringfrictionmultiplier", 0, 2)
    CPanel:ControlHelp("A multiplier to the sideways friction of the wheel when it's turning. Helps to control flipping and oversteer.")
    CPanel:NumSlider("Steering Sensitivity","sc_suspension_steeringsensitivity", 0, 5)
    CPanel:ControlHelp("How quickly does the wheel react to the steering input changing")

    Label = vgui.Create("DLabel")
    Label:SetText("Brake Settings")
    Label:SetFont("DermaDefaultBold")
    Label:SizeToContentsY(2)
    Label:SetContentAlignment(5)
    CPanel:AddItem(Label)

    CPanel:CheckBox("Enable Brakes?", "sc_suspension_hasbrakes")
    CPanel:ControlHelp("Should this wheel have brakes installed?")
    CPanel:NumSlider("Brake Torque","sc_suspension_braketorque", 50, 5000)
    CPanel:ControlHelp("Amount of torque applied to stop the car when the brakes are used at full strength.")

    Label = vgui.Create("DLabel")
    Label:SetText("Player Controls")
    Label:SetFont("DermaLarge")
    Label:SizeToContentsY(4)
    CPanel:AddItem(Label)

    CPanel:Help("These controls will be used for driving any car to ensure a seamless driving experience even if the owner of the car you're driving uses different controls than you do.\n")

    local function CreateKeyBinder(Key, Default)
        local BinderLabel = vgui.Create("DLabel")
        BinderLabel:SetText(KeyDisplayNames[Key] or "INVALID KEY")
        BinderLabel:SetFont("DermaDefaultBold")
        BinderLabel:SizeToContentsY(2)
        BinderLabel:SetContentAlignment(5)

        local Binder = vgui.Create("DBinder")
        Binder:SetSize(110, 60)
        Binder:SetSelectedNumber(cookie.GetNumber("sc_suspension_"..Key, Default))

        function Binder:OnChange(NewKey)
            cookie.Set("sc_suspension_"..Key, NewKey)

            net.Start("SC.Suspension.UpdateKeys")
            net.WriteTable({[Key]=NewKey})
            net.SendToServer()
        end

        return Binder, BinderLabel
    end


    for i,k in ipairs(Keys) do
        local KeyBinder, BinderLabel = CreateKeyBinder(k, DefaultKeys[k])
        CPanel:AddItem(BinderLabel)
        CPanel:AddItem(KeyBinder)
    end
end