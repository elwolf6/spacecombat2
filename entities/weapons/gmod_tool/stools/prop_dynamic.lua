
TOOL.Category		= "Construction"
TOOL.Name			= "Prop Dynamicifier"
TOOL.Command		= nil
TOOL.ConfigName		= ""

if ( CLIENT ) then
    language.Add( "Tool.prop_dynamic.name", "Prop Dynamicifier" )
    language.Add( "Tool.prop_dynamic.desc", "Change selected props into prop_dynamic" )
    language.Add( "Tool.prop_dynamic.0", "Primary: Select a prop to change. (Use to select all) Secondary: Apply change. Reload: Clear Targets." )
end

TOOL.enttbl = {}

hook.Remove("PhysgunPickup", "PropDynamicPhysgun")
hook.Add("PhysgunPickup", "PropDynamicPhysgun", function(Ply, Ent)
    -- This will detect any parented object without a valid physics object, but we probably shouldn't be picking those up anyways
    if IsValid(Ent:GetParent()) and not util.IsValidPhysicsObject(Ent, 0) then
        return false
    end
end)

local function SaveTexture( Player, Entity, Data )
	if not SERVER then return end
	if Data.Texture then Entity:SetMaterial(Data.Texture) end
	duplicator.StoreEntityModifier( Entity, "texture", Data )
end
duplicator.RegisterEntityModifier( "texture", SaveTexture )

local function SaveColors( Player, Entity, Data )
	if not SERVER then return end
	if Data.Colors then Entity:SetColor(Data.Colors) end
	duplicator.StoreEntityModifier( Entity, "colors", Data )
end
duplicator.RegisterEntityModifier( "colors", SaveColors )

local function SaveSkin( Player, Entity, Data )
	if not SERVER then return end
	if Data.Skin then Entity:SetSkin(Data.Skin) end
	duplicator.StoreEntityModifier( Entity, "skin", Data )
end
duplicator.RegisterEntityModifier( "skin", SaveSkin )

local function SaveGroup( Player, Entity, Data )
	if not SERVER then return end
	if Data.Group then Entity:SetCollisionGroup(Data.Group or COLLISION_GROUP_NONE) end
	duplicator.StoreEntityModifier( Entity, "group", Data )
end
duplicator.RegisterEntityModifier( "group", SaveGroup )

local function SaveSpawnflag( Player, Entity, Data )
	if not SERVER then return end
	if Data.Spawnflag then Entity:SetKeyValue("spawnflags",Data.Spawnflag) end
	duplicator.StoreEntityModifier( Entity, "spawnflag", Data )
end
duplicator.RegisterEntityModifier( "spawnflag", SaveSpawnflag )

local function SaveCollision( Player, Entity, Data )
	if not SERVER then return end
	if Data.Collision ~= nil then
		local physobj = Entity:GetPhysicsObject()
		if physobj:IsValid() then physobj:EnableCollisions(Data.Collision) end
	end
	duplicator.StoreEntityModifier( Entity, "collision", Data )
end
duplicator.RegisterEntityModifier( "collision", SaveCollision )

function TOOL:LeftClick( trace )

	local ply = self:GetOwner()
	if (CLIENT) then return true end
	if (trace.Entity:IsValid()) and (trace.Entity:IsPlayer()) then return end
	if ( SERVER && !util.IsValidPhysicsObject( trace.Entity, trace.PhysicsBone ) ) then return false end
	if (trace.Entity:IsWorld()) then return false end
	if (trace.Entity:GetClass() != "prop_physics" and trace.Entity:GetClass() != "phys_magnet") then return false end

	local ent = trace.Entity
	if (self:GetOwner():KeyDown(IN_USE)) then
		for k,v in pairs(ents.FindInSphere(trace.HitPos, 2048)) do
			if(v:IsValid() && self:IsPropOwner(ply, v) == true && v:GetClass() == "prop_physics") then
				local eid = v:EntIndex()
				if not (self.enttbl[eid]) then
					local col = Color(0,0,0,0)
					col = v:GetColor()
					self.enttbl[eid] = col
					v:SetColor(Color(255,0,255,128))
				else
					local col = self.enttbl[eid]
					v:SetColor(col)
					self.enttbl[eid] = nil
				end
			end
		end
	else
		local eid = ent:EntIndex()
		if not (self.enttbl[eid]) then
			local col = Color(0,0,0,0)
			col = ent:GetColor()
			self.enttbl[eid] = col
			ent:SetColor(Color(255,0,255,128))
		else
			local col = self.enttbl[eid]
			ent:SetColor(col)
			self.enttbl[eid] = nil
		end
	end


	return true
end

function TOOL:RightClick( trace )

	if (CLIENT) then return true end
	if (table.Count(self.enttbl) < 1) then return end
	if (trace.Entity:IsValid()) and (trace.Entity:IsPlayer()) then return end
	if ( SERVER && !util.IsValidPhysicsObject( trace.Entity, trace.PhysicsBone ) ) then return false end
	if (trace.Entity:IsWorld()) then return false end

	local ent = trace.Entity
	local phys = ent:GetPhysicsObject()
	local cloneTbl = {}
	for k,v in pairs(self.enttbl) do
		local prop = ents.GetByIndex(k)
		local class = ent:GetClass()

		if (prop:IsValid()) then
			phys = prop:GetPhysicsObject()
			if (phys:IsValid()) then
				class = prop:GetClass()
				local owner = prop:GetOwner()
				if (class == "prop_physics" || class == "phys_magnet") then
					local model = prop:GetModel()
					local colors = v or Color(255,255,255,255)
					local group = COLLISION_GROUP_NONE
					local texture = prop:GetMaterial()
					local skin = prop:GetSkin()
					local collision = true
					local spawnflag = 1024

                    local parent
                    local Protector

                    if prop:IsProtected() then
                        Protector = prop:GetProtector()
                        if Protector.GetGyropod and IsValid(Protector:GetGyropod()) then
                            parent = Protector:GetGyropod()
                        end
                    end

					local clone = ents.Create("prop_dynamic_override")
					local ply = self:GetOwner()
					clone:CPPISetOwner(ply)
					clone.Owner = ply
					SaveColors(owner,clone,{ Colors = colors })
					SaveTexture(owner,clone,{ Texture = texture })
					SaveSkin(owner,clone,{ Skin = skin })
					SaveGroup(owner,clone,{ Group = group})
					SaveCollision(owner,clone,{ Collision = collision })
					SaveSpawnflag(owner,clone,{ Spawnflag = spawnflag })
					clone:SetModel(prop:GetModel())
					clone:SetPos(prop:GetPos())
					clone:SetAngles(prop:GetAngles())
					clone:Spawn()
					clone:SetOwner(prop:GetOwner())
					clone:PhysicsInitStatic(SOLID_VPHYSICS)
                    clone:SetUnFreezable(false)
                    clone:SetTransmitWithParent(true)
                    clone.SCAutoParenter = {
                        ["DisableConstraintRemoval"] = true,
                        ["IgnoreSpecialConstraints"] = false,
                        ["IgnoredConstraints"] = {}, -- This is only used if IgnoreSpecialConstraints is true. Fill with constraint types from the ConstraintTypes table
                        ["DisableWeighting"] = true,
                        ["DisableWelding"] = true,
                        ["DisableParenting"] = true
                    }

                    if IsValid(parent) then
                        clone:SetParent(parent)
                        Protector:InvalidateConstraints()
                    end

					table.insert(cloneTbl,clone)
					prop:Remove()
				end
			end
		end
		self.enttbl[k] = nil
	end

	undo.Create("phys_dynamic")
		undo.SetPlayer(self:GetOwner())
		undo.SetCustomUndoText("Undone Dynamicification.")

		for k,v in pairs(cloneTbl) do
			undo.AddEntity(v)
		end

		undo.AddFunction(function(Undoing)
            undo.Create("Props")
			for k,v in pairs(Undoing.Entities) do
				local prop = v
				if prop:IsValid() then
					local class = v:GetClass()
					phys = prop:GetPhysicsObject()
					if (phys:IsValid()) then
						class = prop:GetClass()
						local owner = prop:GetOwner()
						if class == "prop_dynamic_override" or class == "prop_dynamic" then
							local model = prop:GetModel()
							local colors = prop:GetColor()
							local group = COLLISION_GROUP_NONE
							local texture = prop:GetMaterial()
							local skin = prop:GetSkin()
							local collision = true
							local spawnflag = 1024
							local clone = ents.Create("prop_physics")
							local ply = Undoing.Owner

							clone.Owner = ply
							clone:CPPISetOwner(ply)
							SaveColors(owner,clone,{ Colors = colors })
							SaveTexture(owner,clone,{ Texture = texture })
							SaveSkin(owner,clone,{ Skin = skin })
							SaveGroup(owner,clone,{ Group = group})
							SaveCollision(owner,clone,{ Collision = collision })
							SaveSpawnflag(owner,clone,{ Spawnflag = spawnflag })
							clone:SetModel(prop:GetModel())
							clone:SetPos(prop:GetPos())
							clone:SetAngles(prop:GetAngles())
							clone:Spawn()
							clone:SetOwner(prop:GetOwner())
							clone:PhysicsInit(SOLID_VPHYSICS)

							clonephys = clone:GetPhysicsObject()
							clonephys:EnableMotion(false)

                            undo.AddEntity(clone)
						end
					end
				end
			end
            undo.SetPlayer(Undoing.Owner)
            undo.Finish()
		end)

	undo.Finish()

	self.enttbl = {}
	return true
end

function TOOL:SendMessage( message )
		self:GetOwner():PrintMessage( HUD_PRINTCENTER, message )
end

function TOOL:Reload( trace )
	if (CLIENT) then return false end
	if (table.Count(self.enttbl) > 0) then
		for k,v in pairs(self.enttbl) do
			local prop = ents.GetByIndex(k)
			if (prop:IsValid()) then
				prop:SetColor(v)
				self.enttbl[k] = nil
			end
		end
		self.enttbl = {}
	end
	return true
end

function TOOL.BuildCPanel(panel)
	local BindLabel0 = {}
	local BindLabel1 = {}

	BindLabel0.Text =
[[INSTRUCTIONS

Author: Steeveeo

1)  Select all props you wish to change to
    prop_dynamic (hold USE and
    click to select everything in a 2048
    radius).
2)  Right Click to apply change.

]]

	BindLabel1.Text =
[[NOTES:

1)  This change currently cannot be
    reversed. Read on and make sure you
    wish to do this before continuing.
2)  Some things, like color or material, MAY
    not dupe when changed.
3)  These props, due to not being as laggy
    as normal props, do not count against the
    server's prop limit, feel free to use
    this tool generously.]]

	BindLabel0.Description = "Basic Instructions1"
	panel:AddControl("Label", BindLabel0 )

	BindLabel1.Description = "Basic Instructions2"
	panel:AddControl("Label", BindLabel1 )
end

function TOOL:IsPropOwner(ply, ent)
	if ent:CPPIGetOwner() == ply then return true end
	return false
end

local function CreatePropDynamic(pl, Data, ...)
    local sent = scripted_ents.Get(Data.Class)
    if sent then
        Data.Class = sent.ClassName
    end

	local ent = ents.Create("prop_dynamic_override")
	if not IsValid(ent) then return false end

	duplicator.DoGeneric(ent, Data)
	ent:Spawn()
	ent:Activate()
	duplicator.DoGenericPhysics(ent, pl, Data) -- Is deprecated, but is the only way to access duplicator.EntityPhysics.Load (its local)

	ent.Owner = pl

    ent:PhysicsInitStatic(SOLID_VPHYSICS)
    ent:SetUnFreezable(false)
    ent.SCAutoParenter = {
        ["DisableConstraintRemoval"] = true,
        ["IgnoreSpecialConstraints"] = false,
        ["IgnoredConstraints"] = {}, -- This is only used if IgnoreSpecialConstraints is true. Fill with constraint types from the ConstraintTypes table
        ["DisableWeighting"] = true,
        ["DisableWelding"] = true,
        ["DisableParenting"] = true
    }

	return ent
end

duplicator.RegisterEntityClass("prop_dynamic", CreatePropDynamic, "Data")
duplicator.RegisterEntityClass("prop_dynamic_override", CreatePropDynamic, "Data")