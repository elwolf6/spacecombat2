TOOL.Category		= "Basic Tools"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Multi-Entity Linker"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.Information = {
	"left",
	"right",
	{
		name  = "shift_left",
		icon2  = "gui/e.png",
		icon = "gui/lmb.png",

	},
	"reload"
}

if ( CLIENT ) then
    language.Add( "Tool.multi_link.name", "Multi-Link Tool" )
    language.Add( "Tool.multi_link.desc", "Link multiple entities to one node." )
    language.Add( "Tool.multi_link.left", "Selects an entity to link." )
    language.Add( "Tool.multi_link.shift_left", "Selects all entities in a 2048 unit radius." )
    language.Add( "Tool.multi_link.right", "Link all selected entities to this node." )
    language.Add( "Tool.multi_link.reload", "Clear Targets." )
end

TOOL.SelectedDevices = {}

function TOOL:LeftClick( trace )
	local ply = self:GetOwner()
	if not IsValid(trace.Entity)then return false end
	if trace.Entity:IsWorld() then return false end
    if CLIENT then return true end

    local function SelectDevice(Device)
        local CanLink = Device.IsLSEnt or Device.IsMechanical or Device.IsSCTurret or Device.IsSCWeapon
        if CanLink and (not Device.CPPICanTool or Device:CPPICanTool(self:GetOwner())) then
            self.SelectedDevices[Device:EntIndex()] = {Device=Device, OldColor=Device:GetColor()}
            Device:SetColor(Color(0, 0, 255, 255))
            return true
        else
            return false
        end
    end

    if self:GetOwner():KeyDown(IN_USE) then
        local FoundDevices = false
        for _, Device in pairs(ents.FindInSphere(trace.HitPos, 2048)) do
            if SelectDevice(Device) then
                FoundDevices = true
            end
        end

        if FoundDevices then
            self:GetOwner():ChatPrint("[Space Combat 2] - Devices Selected!")
        else
            self:GetOwner():ChatPrint("[Space Combat 2] - No Devices Found!")
        end
    else
        local Selected = self.SelectedDevices[trace.Entity:EntIndex()]
        if Selected then
            if Selected.OldColor then
                trace.Entity:SetColor(Selected.OldColor)
            end

            self.SelectedDevices[trace.Entity:EntIndex()] = nil
            self:GetOwner():ChatPrint("[Space Combat 2] - Device Unselected!")

            return true
        end

        if SelectDevice(trace.Entity) then
            self:GetOwner():ChatPrint("[Space Combat 2] - Device Selected!")
        else
            self:GetOwner():ChatPrint("[Space Combat 2] - You can't link that!")
        end
    end

	return true
end


function TOOL:RightClick( trace )
	if not IsValid(trace.Entity) then return false end
	if trace.Entity:IsWorld() then return false end
	if CLIENT then return true end

    if table.Count(self.SelectedDevices) < 1 then
        self:GetOwner():ChatPrint("[Space Combat 2] - No Devices Selected!")
        return false
    end

    local Linked = {}
    for Index, DeviceInfo in pairs(self.SelectedDevices) do
        local Device = DeviceInfo.Device
        if Device.IsSCTurret or Device.IsSCWeapon then
            if trace.Entity:IsVehicle() then
                if Device:LinkPod(trace.Entity) then
                    table.insert(Linked, Index)
                end
            elseif trace.Entity.IsSCTargeter then
                if Device:LinkTargeter(trace.Entity) then
                    table.insert(Linked, Index)
                end
            end
        elseif Device.IsLSEnt then
            if trace.Entity.IsNode then
                if Device.IsNode then
                    self:GetOwner():ChatPrint("[Space Combat 2] - Cannot link a Ship Core to another Ship Core!")
                else
                    if Device:Link(trace.Entity) then
                        table.insert(Linked, Index)
                    end
                end
            end
        elseif trace.Entity.IsMechanical then
            if not trace.Entity:IsLinked(Device) then
                if trace.Entity:Link(Device) then
                    table.insert(Linked, Index)
                end
            end
        end
    end

    if #Linked > 0 then
        self:GetOwner():ChatPrint(string.format("[Space Combat 2] - %d Devices Linked",  #Linked))

        for _, LinkedIndex in ipairs(Linked) do
            local LinkedDevice = self.SelectedDevices[LinkedIndex]
            if LinkedDevice.OldColor then
                LinkedDevice.Device:SetColor(LinkedDevice.OldColor)
            end

            self.SelectedDevices[LinkedIndex] = nil
        end
    end

    local RemainingCount = table.Count(self.SelectedDevices)
    if RemainingCount > 0 then
        self:GetOwner():ChatPrint(string.format("[Space Combat 2] - %d Devices Couldn't Be Linked",  RemainingCount))
    end

	return true
end

function TOOL:Reload()
    if CLIENT then return false end

    if table.Count(self.SelectedDevices) < 1 then
        self:GetOwner():ChatPrint("[Space Combat 2] - No Devices Selected!")
        return false
    end

    local Keys = table.GetKeys(self.SelectedDevices)
    for _, DeviceIndex in ipairs(Keys) do
        local DeviceInfo = self.SelectedDevices[DeviceIndex]
        if DeviceInfo.OldColor then
            DeviceInfo.Device:SetColor(DeviceInfo.OldColor)
        end

        self.SelectedDevices[DeviceIndex] = nil
    end

	return true
end