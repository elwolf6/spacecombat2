--[[
Space Combat Ship Wrecks - Created by Lt.Brandon and Steeveeo

TODO: Move this file out of plugins, it's not something that should be disabled!
]]
--
--Convars
--Enable the spawning of wreckage
CreateConVar("sc_wreckage_enable", 1, {FCVAR_NOTIFY, FCVAR_ARCHIVE})

--Use old spawning method of spawning a single container of resources
CreateConVar("sc_wreckage_simple", 0, {FCVAR_NOTIFY, FCVAR_ARCHIVE})

--Allow spawning of wreckage debris inside an atmosphere
CreateConVar("sc_wreckage_in_atmo", 0, {FCVAR_NOTIFY, FCVAR_ARCHIVE})

--Set if debris can be ran into
CreateConVar("sc_wreckage_collision_enable", 0, {FCVAR_NOTIFY, FCVAR_ARCHIVE})

--Maximum number of parts that can be spawned as wreckage when a ship explodes
CreateConVar("sc_wreckage_debris_max", 15, {FCVAR_NOTIFY, FCVAR_ARCHIVE})

--Minimum prop size that can be made into debris
CreateConVar("sc_wreckage_size_min", 500, {FCVAR_NOTIFY, FCVAR_ARCHIVE})

--Minimum prop size that can be made into debris
CreateConVar("sc_wreckage_drop_chance", 0.75, {FCVAR_NOTIFY, FCVAR_ARCHIVE})

SC.Salvaging = {}

SC.Salvaging.Drops = {
    "Electronic Circuitry";
    "Scrap Metal";
    "Contaminated Coolant";
    "Radioactive Materials";
    "Nubium Generator Core";
    "Damaged Shield Emitter";
    "Plasma Conduits";
    "Destroyed Consoles";
}

SC.Salvaging.Models = {
    "models/mandrac/energy_cell/large_cell.mdl";
    "models/mandrac/energy_cell/medium_cell.mdl";
    "models/mandrac/ore_container/ore_medium.mdl";
    "models/mandrac/ore_container/ore_large.mdl";
    "models/mandrac/water_storage/water_storage_large.mdl";
    "models/mandrac/water_storage/water_storage_small.mdl";
}

--This function actually spawns a wreck
local function SpawnWreck(core, replace, resources)
    if not IsValid(core) then return NULL end
    -- Make sure the ship core is in a valid location
    if not util.IsInWorld(core:GetPos()) then return NULL end
    local AllowInAtmosphere = GetConVarNumber("sc_wreckage_in_atmo") == 1

    if not AllowInAtmosphere then
        if GAMEMODE:GetAtmosphereAtPoint(core:GetPos()) ~= GAMEMODE:GetSpace() then return NULL end
    end

    -- Make sure the part we're replacing is also in a valid location if we have one
    if IsValid(replace) then
        if not util.IsInWorld(replace:GetPos()) then return NULL end

        if not AllowInAtmosphere then
            if GAMEMODE:GetAtmosphereAtPoint(replace:GetPos()) ~= GAMEMODE:GetSpace() then return NULL end
        end
    end

    --Spawn the wreck, set its position and angles
    local Wreck = ents.Create("sc_wreck")

    if replace ~= nil and IsValid(replace) then
        Wreck:SetPos(replace:GetPos())
        Wreck:SetModel(replace:GetModel())
        Wreck:SetSkin(replace:GetSkin())
        Wreck:SetMaterial(replace:GetMaterial())

        --Randomize angle slightly
        Wreck:SetAngles(replace:GetAngles() + Angle(math.Rand(-15, 15), math.Rand(-15, 15), math.Rand(-15, 15)))

        --Darken color to simulate scorching
        local OldColor = replace:GetColor()
        Wreck:SetColor(Color(OldColor.r / 1.5, OldColor.g / 1.5, OldColor.b / 1.5, 255))
    else
        Wreck:SetPos(core:GetPos())
        Wreck:SetAngles(core:GetAngles())
        Wreck:SetModel(table.Random(SC.Salvaging.Models))
    end

    Wreck:Spawn()
    Wreck:Activate()
    --Toggle physics
    local CanCollide = GetConVarNumber("sc_wreckage_collision_enable")
    local PhysObj = Wreck:GetPhysicsObject()

    if IsValid(PhysObj) then
        if CanCollide == 0 then
            PhysObj:EnableCollisions(false)
        end
    end

    -- Setup the wreck for salvaging
    local StorageVolume = 0 -- The volume of the wreck's storage to create
    local MiningLevel = 1   -- The mining level to set the wreck to
    for Name, Amount in pairs(resources) do
        local Resource = GAMEMODE:GetResourceFromName(Name)

        -- Increase the volume needed
        StorageVolume = StorageVolume + (Amount * Resource:GetSize())

        -- Increase the mining level if needed
        local Rarity = Resource:GetRarity()
        if Rarity > MiningLevel then
            MiningLevel = Rarity
        end
    end

    Wreck:SetMiningLevel(MiningLevel)
    Wreck:CreateStorage("Cargo", StorageVolume, resources)

    return Wreck
end

--Create Wreck function, for all your shipwreck needs!
function SC.CreateWreck(Core)
    --Only run if wreckages are enabled
    if GetConVarNumber("sc_wreckage_enable") == 0 then return end

    --Is this even a valid entity? Can't use normal IsValid because fucking garry, but this should be close enough.
    if Core == nil then return NULL end
    local DropChance = math.Clamp(GetConVarNumber("sc_wreckage_drop_chance"), 0, 1) * 20

    --Which spawning method to use?
    if GetConVarNumber("sc_wreckage_simple") == 1 then
        --What resources should be left over from the explosion?
        local Resources = {}
        local Cargo = Core:GetStorageOfType("Cargo"):GetStored()

        for i, k in pairs(Cargo) do
            local ShouldInclude = math.Rand(1, 20) <= DropChance

            if ShouldInclude then
                local Amount = math.floor(k:GetAmount() * math.Rand(0.2, 0.8))

                if Amount >= 1 then
                    Resources[i] = Amount
                end
            end
        end

        --Base resources
        Resources["Iron"] = (Resources["Iron"] or 0) + math.ceil(math.Rand(0.1, 3) * Core:GetNodeRadius())
        Resources["Tritanium"] = (Resources["Tritanium"] or 0) + math.ceil(math.Rand(0.5, 10) * Core:GetNodeRadius())

        --Add a random assortment of salvaging drops
        for i, k in pairs(SC.Salvaging.Drops) do
            if math.floor(math.random(0, 5)) == 1 then
                Resources[k] = (Resources[k] or 0) + math.ceil(math.random(10, 500) / math.random(1, 10))
            end
        end

        SpawnWreck(Core, nil, Resources)

    --Complex Wreckage Spawning
    else
        local MinSize = GetConVarNumber("sc_wreckage_size_min")
        local MaxBits = GetConVarNumber("sc_wreckage_debris_max")
        MinSize = MinSize * MinSize
        --Tally up all the bits that fit all requirements
        local AvailableBits = {}

        for _, Part in pairs(Core.ConWeldTable) do
            if IsValid(Part) then
                if Part:GetColor().a > 200 then
                    --Check size
                    local Size = Part:OBBMins():DistToSqr(Part:OBBMaxs())

                    if Size >= MinSize then
                        table.insert(AvailableBits, Part)
                    end
                end
            end
        end

        --Select which bits to become actual bits
        local Debris = {}

        for i = 1, math.min(MaxBits, #AvailableBits) do
            local Bit, Key = table.Random(AvailableBits)
            table.insert(Debris, Bit)
            table.remove(AvailableBits, Key)
        end

        --Make the actual bits
        for _, Bit in pairs(Debris) do
            local Resources = {}
            local CargoContainer = Core:GetStorageOfType("Cargo")
            local Cargo = CargoContainer:GetStored()

            for i, k in pairs(Cargo) do
                local ShouldInclude = math.Rand(1, 20) <= DropChance

                if ShouldInclude then
                    local Amount = math.floor(k:GetAmount() * math.Rand(0.2, 0.8) * (1 / MaxBits))

                    if Amount >= 1 then
                        Resources[i] = Amount
                        CargoContainer:ConsumeResource(i, Amount)
                    end
                end
            end

            --Base resources
            Resources["Iron"] = (Resources["Iron"] or 0) + math.ceil(math.Rand(0.1, 3) * Bit:BoundingRadius())
            Resources["Tritanium"] = (Resources["Tritanium"] or 0) + math.ceil(math.Rand(0.5, 10) * Bit:BoundingRadius())

            --Add a random assortment of salvaging drops
            for i, k in pairs(SC.Salvaging.Drops) do
                if math.floor(math.random(0, 5)) == 1 then
                    Resources[k] = (Resources[k] or 0) + math.ceil(((math.random(1, 750) / GAMEMODE:NewResource(k, 0, 0):GetSize()) * math.random(0.5, 2)) / #Debris)
                end
            end

            SpawnWreck(Core, Bit, Resources)
        end
    end
end