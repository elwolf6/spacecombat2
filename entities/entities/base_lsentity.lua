AddCSLuaFile()

DEFINE_BASECLASS("base_scentity")

ENT.PrintName = "Base Life Support Entity"
ENT.Author = "Divran"
ENT.Contact = "arviddivran@gmail.com"
ENT.Purpose = "Base for resource entities"
ENT.Instructions = "node"

ENT.Spawnable = false
ENT.AdminOnly = false

ENT.IsLSEnt = true

local base = scripted_ents.Get("base_scentity")
hook.Add( "InitPostEntity", "base_lsentity_post_entity_init", function()
	base = scripted_ents.Get("base_scentity")
end)

if SERVER then
    util.AddNetworkString("SC.ShipStorageSync")
end

hook.Remove("SC.RegisterCustomSyncFunction", "SC.LS.RegisterCustomSyncFunction")
hook.Add("SC.RegisterCustomSyncFunction", "SC.LS.RegisterCustomSyncFunction", function()
    SC.NWAccessors.RegisterCustomSyncFunction("SC.ShipStorageSync", function(LSEntity, Sending, Accessor, OnFinishedUpdating)
        if Sending then
            -- We have to send something to prevent confusion on the client side
            if not Accessor or not Accessor.value then
                net.WriteUInt(0, 8)
                return
            end

            net.WriteUInt(table.Count(Accessor.value), 8)
            for i,k in pairs(Accessor.value) do
                net.WriteUInt(GAMEMODE:GetStorageTypeID(i), 12)
                k:WriteCreationPacket()
            end
        else
            local TotalResources = net.ReadUInt(8)

            -- If no reosurces were sent then don't bother with the rest
            if TotalResources < 1 then
                OnFinishedUpdating()
                return
            end

            for I = 1, TotalResources do
                local NewStorageID = GAMEMODE:GetStorageTypeFromID(net.ReadUInt(12))
                local NewStorage = GAMEMODE.class.getClass("ResourceContainer"):new()
                NewStorage:SetType(NewStorageID)
                NewStorage:ReadCreationPacket()

                -- Only actually update if the accessor is valid, otherwise we're just reading to prevent the game from breaking
                if Accessor and Accessor.value then
                    Accessor.value[NewStorageID] = NewStorage
                end
            end

            OnFinishedUpdating()
        end
    end)
end)

function ENT:SharedInit()
	base.SharedInit(self)

    SC.NWAccessors.CreateNWAccessor(self, "Node", "entity", NULL)
    SC.NWAccessors.CreateNWAccessor(self, "Links", "table", {})
end

--------------------------------------------------
-- Useless Functions
--------------------------------------------------
function ENT:GetStorage()
    -- This used to be implemented for all lifesupport entities, but due to a performance optimization it has been disabled.
    -- Anything that needs to implement storage must add the following to the SharedInit:
    -- SC.NWAccessors.CreateNWAccessor(self, "Storage", "custom", {}, 1, nil, "SC.ShipStorageSync")
    return {}
end

function ENT:SetStorage()

end

function ENT:GetAmount(name)
    if IsValid(self:GetNode()) then return self:GetNode():GetAmount(name) end
    local Storage = self:GetStorageOfType(GAMEMODE:GetResourceStorageType(name))
    if not Storage then return 0 end
    return Storage:GetAmount(name)
end

function ENT:GetMaxAmount(name)
    if IsValid(self:GetNode()) then return self:GetNode():GetMaxAmount(name) end
    local Storage = self:GetStorageOfType(GAMEMODE:GetResourceStorageType(name))
    if not Storage then return 0 end
    return Storage:GetMaxAmount(name)
end

function ENT:HasResource(resource, amount)
    return self:GetAmount(resource) >= amount
end

function ENT:HasResources(tbl)
    local ret = true
    for i,k in pairs(tbl) do
        ret = self:HasResource(i, k)
        if not ret then return false end
    end

    return ret
end

function ENT:GetStorageOfType( name )
	if IsValid(self:GetNode()) then return self:GetNode():GetStorageOfType( name ) end

	return self:GetStorage(nil, true)[name]
end

-------------------------
-- Deltas
-------------------------
function ENT:CalculateDeltas()
	-- don't calculate deltas if we have a parent node or if we don't have the table
	if IsValid( self:GetNode() ) or not self.resource_deltas or not self:GetStorage(nil, true) then return end

	for type, storage in pairs( self:GetStorage(nil, true) ) do
        for name,res in pairs(storage:GetStored()) do
		    if not self.resource_deltas[name] then self.resource_deltas[name] = {} end
		    local prev = self.resource_deltas[name].previous_value or 0
		    self.resource_deltas[name].delta = res:GetAmount() - prev
		    self.resource_deltas[name].previous_value = res:GetAmount()

            local prevmax = self.resource_deltas[name].previous_maxvalue or 0
		    self.resource_deltas[name].maxdelta = res:GetMaxAmount() - prevmax
		    self.resource_deltas[name].previous_maxvalue = res:GetMaxAmount()
        end
	end
end

function ENT:GetDeltas()
	if IsValid( self:GetNode() ) then return self:GetNode():GetDeltas() end
	if not self.resource_deltas then return {} end

	return self.resource_deltas
end

function ENT:GetDelta( name )
	if IsValid( self:GetNode() ) then return self:GetNode():GetDelta( name ) end
	if not self.resource_deltas then return 0 end

	if self.resource_deltas[name] then
		return self.resource_deltas[name].delta or 0
	else
		return 0
	end
end

if CLIENT then return end

--------------------------------------------------
-- Init & Setup
--------------------------------------------------
function ENT:Initialize()
    self:SharedInit()
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
	self:Setup()
end

function ENT:Setup()
	self.resource_deltas = {}
	self:SetupWirePorts()
	self:UpdateOutputs()
end

--------------------------------------------------
-- Linking
--------------------------------------------------
function ENT:Link( node )
	if not IsValid( node ) and not node.IsNode then return false end
	if node == self then return false end
	if self:IsLinked() and self:GetNode() == node then return false end

	-- Check for infinite loops
	local current_node = node:GetNode()
	while true do
		if not IsValid( current_node ) then break end
		if current_node == self then return false end
		current_node = current_node:GetNode()
	end

	if self:IsLinked() then
		self:Unlink()
	end

	self:SetNode(node)
	self:GetNode():AddLink( self )

	self:OnLinked(node)
	self:GetNode():OnLinked(self)

    return true
end

function ENT:Unlink()
	if not self:IsLinked() then return end

	self:OnUnlinked(self:GetNode())

	if IsValid( self:GetNode() ) then
		self:GetNode():RemoveLink( self )
		self:GetNode():OnUnlinked(self)
	end

	self:SetNode(NULL)
end

function ENT:AddLink( ent )
    local index = ent:EntIndex()
    local tbl = self:GetLinks()
	tbl[index] = ent
    self:SetLinks(tbl)
end

function ENT:RemoveLink( ent )
    local index = ent:EntIndex()
	local tbl = self:GetLinks()
	tbl[index] = nil
    self:SetLinks(tbl)
end

function ENT:UnlinkAll()
    for _,ent in pairs(self:GetLinks()) do
        if IsValid(ent) and ent.Unlink then
            ent:Unlink()
        else
            SC.Error("Entity "..tostring(ent).." was missing Unlink function!", 3)
        end
	end
end

function ENT:IsLinked()
	return IsValid(self:GetNode())
end

--------------------------------------------------
-- Resource Handling
--------------------------------------------------
function ENT:OnLinked( ent )
	local resources = ent:GetStorage(nil, true)
	if resources and next(resource) ~= nil then
		for type,storage in pairs( resources ) do
			storage:MergeStorage(self:GetStorage(nil, true)[type])
		end
	end
end

function ENT:OnUnlinked( ent )
	local resources = ent:GetStorage(nil, true)
	if resources and next(resource) ~= nil then
		for type,storage in pairs( resources ) do
			storage:SplitStorage(self:GetStorage(nil, true)[type])
		end
	end
end

function ENT:SetAmount(name, amount)
    if IsValid(self:GetNode()) then return self:GetNode():SetAmount(name, amount) end
    local Storage = self:GetStorageOfType(GAMEMODE:GetResourceStorageType(name))
    if not Storage then return end
    return Storage:SetAmount(name, amount)
end

function ENT:SetMaxAmount(name, amount)
    if IsValid(self:GetNode()) then return self:GetNode():SetMaxAmount(name, amount) end
    local Storage = self:GetStorageOfType(GAMEMODE:GetResourceStorageType(name))
    if not Storage then return end
    return Storage:SetMaxAmount(name, amount)
end

function ENT:SupplyResource(name, amount)
    if IsValid(self:GetNode()) then return self:GetNode():SupplyResource(name, amount) end
    local Storage = self:GetStorageOfType(GAMEMODE:GetResourceStorageType(name))
    if not Storage then return end
    return Storage:SupplyResource(name, amount)
end

function ENT:SupplyResources(tbl)
    for i,k in pairs(tbl) do
        self:SupplyResource(i, k)
    end
end

function ENT:ConsumeResource(name, amount, force)
    if IsValid(self:GetNode()) then return self:GetNode():ConsumeResource(name, amount, force) end
    local Storage = self:GetStorageOfType(GAMEMODE:GetResourceStorageType(name))
    if not Storage then return false end
    return Storage:ConsumeResource(name, amount, force)
end

function ENT:ConsumeResources(tbl)
    if not self:HasResources(tbl) then return false end

    for i,k in pairs(tbl) do
        self:ConsumeResource(i, k)
    end

    return true
end

--------------------------------------------------
-- Other Hooks
--------------------------------------------------
function ENT:OnRemove()
	self:UnlinkAll()
    self:Unlink()
    if WireLib then
        WireLib.Remove(self)
    end
end

function ENT:UpdateOutputs()

end

function ENT:Think()
	self:UpdateOutputs()
	self:CalculateDeltas()
	self:NextThink( CurTime() + 1 )
	return true
end

function ENT:OnEntityCopyTableFinish(dupedata)
	-- Called by Garry's duplicator, to modify the table that will be saved about an ent

	-- Remove anything with non-string keys, or util.TableToJSON will crash the game
	dupedata.OverlayData_UpdateTime = nil
end