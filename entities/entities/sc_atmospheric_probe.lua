AddCSLuaFile()

DEFINE_BASECLASS("sc_resource_monitor")

ENT.PrintName = "Atmospheric Probe"
ENT.Author = "Steeveeo"
ENT.Contact = "diaspora-community.com"
ENT.Purpose = "Check to see if an atmosphere is deadly BEFORE it kills you!"
ENT.Instructions = "Check to see if an atmosphere is deadly BEFORE it kills you!"

ENT.Spawnable = false
ENT.AdminOnly = false


local function InitGeneratorInfo()
    local Generator = GAMEMODE:NewGeneratorInfo()
    Generator:SetName("Atmospheric Probe")
    Generator:SetClass("sc_atmospheric_probe")
    Generator:SetDescription("Check to see if an atmosphere is deadly BEFORE it kills you!")
    Generator:SetCategory("Utility")
    Generator:SetDefaultModel("models/sbep_community/d12shieldemitter.mdl")

    GAMEMODE:RegisterGenerator(Generator)
end
hook.Remove("InitPostEntity", "sc_atmo_probe_toolinit")
hook.Add("InitPostEntity", "sc_atmo_probe_toolinit", InitGeneratorInfo)
hook.Remove("OnReloaded", "sc_atmo_probe_post_reloaded")
hook.Add("OnReloaded", "sc_atmo_probe_post_reloaded", InitGeneratorInfo)


if CLIENT then

	-------------------------
	-- colors
	-------------------------
	local colors = {
		Oxygen = 				Color(0,255,0,255), -- green
		Water = 				Color(0,0,255,255), -- blue
		["Carbon Dioxide"] = 	Color(255,165,0,255), -- orange
		Hydrogen = 				Color(255,255,0,255), -- yellow
		Nitrogen = 				Color(255,0,255,255), -- purple
	}
	local white = Color(255,255,255,255)
	local black = Color(0,0,0,255)

	local displayModesSizes = {
		function( data )
			local h = (#data-4) * SC2ScreenScale(26) + SC2ScreenScale(64)

			local w = math.ceil(h / (ScrH() - 128))

			return w * 300, math.min(h,ScrH()-128)
		end
	}

	local displayModes = {
		function(data, pos) -- Horizontal Bars
			--Decode
			local planetName = data[2]
			local temperature = data[3]
			local gravity = data[4]
			local pressure = data[5]

			--Position Tracking
			local ypos = 0

			--Planet Name
			draw.DrawText(planetName, "GModWorldtip", pos.center.x, pos.min.y + (pos.edgesize / 2), white, TEXT_ALIGN_CENTER)
			ypos = ypos + SC2ScreenScale(16)

			--Planet Temperature
			local str = string.format("Temperature: \t\t%.2f°K\nGravity: \t\t%.2f G\nPressure: \t\t%.2f Atmosphere", temperature, gravity, pressure)
			draw.DrawText(str, "GModWorldtip_Smaller", pos.min.x + pos.edgesize, pos.min.y + pos.edgesize + ypos, white, TEXT_ALIGN_LEFT)

			ypos = ypos + SC2ScreenScale(56)

			--Resources
			for i = 6, #data do
				local res = data[i]
				local name = res[1]
				local amt = res[2]
				local max = res[3]

				--Resource Label
				draw.DrawText(name, "GModWorldtip_Smaller", pos.min.x + pos.edgesize, pos.min.y + pos.edgesize + ypos, white, TEXT_ALIGN_LEFT)

				--Resource Amount
				local percent = amt / max
				local display_amount = nicenumber.formatDecimal(amt)
				local display_max = nicenumber.formatDecimal(max)
				local display_percent = math.Round(percent * 100)
				str = string.format(
											"%s/%s (%s%%)",
											display_amount,
											display_max,
											display_percent
										)

				draw.DrawText(str, "GModWorldtip_Smaller", pos.max.x - pos.edgesize, pos.min.y + pos.edgesize + ypos, white, TEXT_ALIGN_RIGHT)

				--Resource Bar
				surface.SetDrawColor(colors[name] or white)
				surface.DrawRect(pos.min.x + pos.edgesize, pos.min.y + pos.edgesize + ypos + SC2ScreenScale(18), 300 * percent, 8)

				--Resource Bar Outline
				surface.SetDrawColor(black)
				surface.DrawOutlinedRect(pos.min.x + pos.edgesize, pos.min.y + pos.edgesize + ypos + SC2ScreenScale(18), 300, 8)

				ypos = ypos + SC2ScreenScale(26)
			end
		end
	}

	function ENT:GetWorldTipBodySize()
		local data = self:GetOverlayData()
		if not data or data[1] == -1 or data[1] == -2 then return 40, 32 end

		return displayModesSizes[data[1]+1]( data )
	end

	-------------------------
	-- Draw the stuff
    -------------------------
    local fontname = "GModWorldtip_Smaller"
	function ENT:DrawWorldTipBody( WorldTip, pos )
		local data = self:GetOverlayData()
		if not data or data[1] == -1 then

			local str = "Not linked"
			draw.DrawText( str, fontname, pos.min.x + pos.size.w/2, pos.min.y + pos.edgesize, white, TEXT_ALIGN_CENTER )

			return
		end

		if not data or data[1] == -2 then

			local str = "No resources to display"
			draw.DrawText( str, fontname, pos.min.x + pos.size.w/2, pos.min.y + pos.edgesize, white, TEXT_ALIGN_CENTER )

			return
		end

		local displayMode = data[1] + 1
		displayModes[displayMode]( data, pos )
	end

	return
end


function ENT:Setup()
	self.delta = {}
	self.prev_amounts = {}
	self.DisplayMode = 0
	self:SetUseType( SIMPLE_USE )

    if WireLib ~= nil then
        WireLib.CreateInputs( self, { "Emit" } )
    end
end

function ENT:TriggerInput( name, val )
	if name == "Emit" then
		self:SetNWBool( "emit", val ~= 0 )
	end
end

function ENT:Use()
	self:UpdateOutputs()
end

-------------------------
-- Calculate rates, averages, etc, and sync to client
-------------------------
function ENT:UpdateOutputs()
	if IsValid( self:GetNode() ) then

		--Get Players in Range if Emitting
		if self:GetNWBool("emit", false) then
			for _,ply in pairs(player.GetAll()) do
				local range = ply:GetInfoNum("sc_resourcemonitor_emitrange", 512)
				local distSqr = ply:GetPos():DistToSqr(self:GetPos())

				if distSqr < range * range then
					self:UpdateOverlayData(ply)
				end
			end
		end

		local env = self:GetEnvironment()
		local Containers = env:GetContainers()

		local res = {}
		res[1] = self.DisplayMode
		res[2] = env:GetName()
		res[3] = env:GetTemperature()
		res[4] = env:GetGravity()
		res[5] = env:GetPressure()

        for Type, Container in pairs(Containers) do
            for Name in pairs(Container:GetStored()) do
                res[#res+1] = {
                    Name,
                    Container:GetAmount(Name),
                    Container:GetMaxAmount(Name)
                }
            end
        end

		self:SetOverlayData( res )
	else
		self:SetOverlayData( {-1} )
	end
end

duplicator.RegisterEntityClass("sc_atmospheric_probe", GAMEMODE.MakeEnt, "Data")
