--[[
	Minable Mineral Deposit Ent for SC2 Mining

	Author: Steeveeo + Fuhrball
]]--
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "mining_mineral"

ENT.PrintName = "Mineral Deposit"
ENT.Author = "Steeveeo + Fuhrball"
ENT.Contact = ""
ENT.Purpose = "I am a mineral deposit, and will devour your soul!"
ENT.Instructions = ""
ENT.RenderGroup = RENDERGROUP_BOTH

ENT.Spawnable = false
ENT.AdminSpawnable = false

if CLIENT then
    -- This overrides the one used in the mining_material class with something that works on any mesh, because the
    -- parent models don't have a proper detailed material.
    ENT.MaterialFormat = [[
VertexLitGeneric
{
	$model 					"1"
	$basetexture		 	"spacecombat2\ltbrandon\crystals\diffuse_detail"
	$basetexturetransform	"center .5 .5 scale 7 7 rotate 0 translate 0 0"
	$bumpmap				"spacecombat2\ltbrandon\crystals\normal_detail"
	$bumptransform			"center .5 .5 scale 7 7 rotate 0 translate 0 0"
	$envmap 				env_cubemap
	$envmaptint				"[%.2f %.2f %.2f]"
	$color2					"[%.2f %.2f %.2f]"
	$surfaceprop			"metal"
	$phong 1
	$halflambert 1
}
    ]]

    ENT.CrystalMatNamePrefix = "crystalpmat_"
end

if SERVER then
    --Earthquake Vars
    ENT.Rumbles = {}
    ENT.Rumbles[1] = "ambient/levels/city/citadel_cloudhit1.wav"
    ENT.Rumbles[2] = "ambient/levels/city/citadel_cloudhit2.wav"
    ENT.Rumbles[3] = "ambient/levels/city/citadel_cloudhit3.wav"
    ENT.Rumbles[4] = "ambient/levels/city/citadel_cloudhit4.wav"
    ENT.Rumbles[5] = "ambient/levels/city/citadel_cloudhit5.wav"

    for k,v in pairs(ENT.Rumbles) do
	    util.PrecacheSound(v)
    end

    local Quake_MinDelay = 5
    local Quake_MaxDelay = 30
    local Quake_MinAmp = 0.25
    local Quake_MaxAmp = 5
    local Quake_MinFreq = 25
    local Quake_MaxFreq = 100
    local Quake_MinTime = 2
    local Quake_MaxTime = 6

	ENT.SpawnParticle_Magnitude = 7
	ENT.SpawnQuake_Amp = 12
	ENT.SpawnQuake_Freq = 200
	ENT.SpawnQuake_Time = 10
	ENT.SpawnQuake_Dist = 4000

	ENT.BreachQuake_Amp = 100
	ENT.BreachQuake_Freq = 200
	ENT.BreachQuake_Time = 2
	ENT.BreachQuake_Dist = 1000
	ENT.BreachQuake_DamageRadius = 500

    function ENT:Initialize()
	    self.BaseClass.Initialize(self)

		--If you want to see magic happen, move this back up to the pile of variables above,
		--then print out the damage table in console. Plz explain.
		self.BreachQuake_Damage = {
			RIP = 1500000
		}

	    self.NextQuake = CurTime() + math.Rand(Quake_MinDelay,Quake_MaxDelay)
    end

    function ENT:Think()
	    self.BaseClass.Think(self)

	    --Earthquakes
	    if CurTime() >= self.NextQuake then
		    --Setup
		    local amplitude = math.floor(math.Rand(Quake_MinAmp, Quake_MaxAmp))
		    local frequency = math.floor(math.Rand(Quake_MinFreq, Quake_MaxFreq))
		    local duration = math.floor(math.Rand(Quake_MinTime, Quake_MaxTime))

		    --Make a quake
		    self:DoQuake(amplitude, frequency, duration, 500, 24)

		    --Make some noise
		    self:EmitSound(table.Random(self.Rumbles), 100, 75)

		    self.NextQuake = CurTime() + math.Rand(Quake_MinDelay, Quake_MaxDelay)
	    end

		if not self.IsDecaying then
			self:NextThink(CurTime() + 1)
		end
	    return true
    end

    --Placeholder functions
    function ENT:Touch()
    end

    function ENT:StartTouch()
    end

    function ENT:EndTouch()
    end
end