require("mysqloo")
module("mysql")

function query(DB, Query)
	local errorr = "None"
	local datar = {}

    local q = DB:query( Query )
    function q:onSuccess( data )
		  datar = data
    end

    function q:onError( err, sql )
		  errorr = err
    end

    q:start()
	q:wait()

	return datar, errorr
end

--Async version of the normal query, callback should accept two inputs, success and data(table if success is true, string if false.)
function asyncquery(DB, Query, Callback)
    local q = DB:query(Query)
    function q:onSuccess(data)
		  if Callback then Callback(true, data) end
    end

    function q:onError(err, sql)
		  if Callback then Callback(false, err) end
    end

    q:start()
end