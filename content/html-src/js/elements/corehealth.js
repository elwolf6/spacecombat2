window.addEventListener('load', (event) => {
    const Elements = document.getElementsByClassName('CoreHealthChart')

    for (Element of Elements) {
        var Context = Element.getContext('2d');
        var CoreChart = new Chart(Context, {
            type: 'doughnut',
            data: {
                labels: ['Shield', 'Shield Damage', 'Armor', 'Armor Damage', 'Hull', 'Hull Damage'],
                datasets: [{
                    label: 'Max Health',
                    data: [10, 10, 10, 10, 10, 10],
                    backgroundColor: [
                        'rgba(54, 162, 235, 0.4)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 99, 132, 0.4)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(255, 206, 86, 0.4)',
                        'rgba(255, 206, 86, 0.2)'
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(255, 206, 86, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {

            }
        });

        setInterval(function (TheChart) {
            const CoreHealth = GetCoreHealthData();
            TheChart.data.datasets[0].data[0] = CoreHealth.Shield.Amount;
            TheChart.data.datasets[0].data[1] = CoreHealth.Shield.MaxAmount - CoreHealth.Shield.Amount;
            TheChart.data.datasets[0].data[2] = CoreHealth.Armor.Amount;
            TheChart.data.datasets[0].data[3] = CoreHealth.Armor.MaxAmount - CoreHealth.Armor.Amount;
            TheChart.data.datasets[0].data[4] = CoreHealth.Hull.Amount;
            TheChart.data.datasets[0].data[5] = CoreHealth.Hull.MaxAmount - CoreHealth.Hull.Amount;
            TheChart.update();
        }, 500, CoreChart);
    }
});