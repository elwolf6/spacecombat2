window.addEventListener('load', (event) => {
    const Elements = document.getElementsByClassName('CoreResistanceChart')

    for (Element of Elements) {
        var Context = Element.getContext('2d');
        var CoreChart = new Chart(Context, {
            type: 'radar',
            data: {
                labels: ['EM', 'THERM', 'KIN', 'EXP'],
                datasets: [
                    {
                        data: [70, 50, 90, 70],
                        backgroundColor: 'rgba(54, 162, 235, 0.4)'
                    },
                    {
                        data: [50, 70, 70, 40],
                        backgroundColor: 'rgba(255, 99, 132, 0.4)'
                    },
                    {
                        data: [20, 20, 20, 20],
                        backgroundColor: 'rgba(255, 206, 86, 0.4)'
                    }
                ]
            },
            options: {

            }
        });

        setInterval(function (TheChart) {

            TheChart.update();
        }, 500, CoreChart);
    }
});